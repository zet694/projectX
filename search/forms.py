from django import forms
from catalog.models import SourceQuery
from search.models import SearchQuery
import datetime
from django.utils import timezone


class SimpleSearchForm(forms.Form):
    query_string = forms.CharField(widget=forms.TextInput(attrs={'size': '120'}), )
    date_start = forms.DateField(required=False, label='C',
                                 widget=forms.SelectDateWidget(
                                     years=range(datetime.date.today().year - 5, datetime.date.today().year + 1)
                                 ),
                                 initial=timezone.datetime.today().date() - timezone.timedelta(days=3))
    date_end = forms.DateField(required=False, label='По',
                               widget=forms.SelectDateWidget(
                                   years=range(datetime.date.today().year - 5, datetime.date.today().year + 1)
                               ),
                               initial=timezone.datetime.today().date()
                               )
    sources = forms.ModelMultipleChoiceField(required=False, label='Запросы по источникам', queryset=SourceQuery.objects.all())


class SavedSearchQueryGroupReportForm(forms.Form):
    saved_search_queries = forms.ModelMultipleChoiceField(required=False, label='Сохраненные запросы', queryset=SearchQuery.objects.all(), widget=forms.CheckboxSelectMultiple())

