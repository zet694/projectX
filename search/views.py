import json

from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DetailView
import datetime
from search.models import *
from search.forms import SimpleSearchForm, SavedSearchQueryGroupReportForm
from django.http import HttpResponseRedirect, HttpResponse, Http404
from search.searchers import simple_search
from crowler.models import News
from django import forms
from django.http import Http404
from docxtpl import DocxTemplate
from search.plotters import chart
from crowler.xml_handlers import make_xml_v2_news

# Create your views here.


def search(request):
    return render(request, 'search/search_index.html')


class SearchQueryListView(ListView):
    model = SearchQuery

    # def get_queryset(self):
    #     return SearchQuery.objects.filter(owner=self.request.user)


class SearchQueryCreateView(CreateView):
    model = SearchQuery
    success_url = '/search'
    fields = [
        'name',
        'source_query',
        'query_string',
        'on_monitor',
        'date_start',
        'date_end',
    ]

    def get_form(self):
        form = super(SearchQueryCreateView, self).get_form()
        form.fields['date_start'].widget = forms.SelectDateWidget(
            years=range(datetime.date.today().year - 5, datetime.date.today().year + 1))
        form.fields['date_end'].widget = forms.SelectDateWidget(
            years=range(datetime.date.today().year - 5, datetime.date.today().year + 1))
        return form


class SearchQueryUpdateView(UpdateView):
    model = SearchQuery
    success_url = '/search/close'
    fields = [
        'name',
        'source_query',
        'query_string',
        'on_monitor',
        'date_start',
        'date_end',
    ]
    template_name = 'search/update_form.html'

    def get_form(self):
        form = super(SearchQueryUpdateView, self).get_form()
        form.fields['date_start'].widget = forms.SelectDateWidget(
            years=range(datetime.date.today().year - 5, datetime.date.today().year + 1))
        form.fields['date_end'].widget = forms.SelectDateWidget(
            years=range(datetime.date.today().year - 5, datetime.date.today().year + 1))
        return form


class NewsDetailView(DetailView):
    model = News


def searchquery_delete(request, pk):
    if request.method == 'GET':
        try:
            s = SearchQuery.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


def simple_search_query_form(request):
    if request.method == 'POST':
        form = SimpleSearchForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            r = simple_search(data['query_string'], data['sources'], date_from=data['date_start'],
                              date_to=data['date_end'])

            ids = list(map(lambda x: x['_id'], r.dict['hits']['hits']))
            highlights_title = {key: value for (key, value) in list(
                map(lambda x: [x['_id'], x['highlight']['title']] if x['highlight'].get('title') else [x['_id'], []],
                    r.dict['hits']['hits']))}
            highlights_text = {key: value for (key, value) in list(
                map(lambda x: [x['_id'], x['highlight']['text']] if x['highlight'].get('text') else [x['_id'], []],
                    r.dict['hits']['hits']))}
            news_list = News.objects.filter(id__in=ids)
            return render(request, 'search/news_found_list.html',
                          {'news_list': news_list, 'highlights_title': highlights_title,
                           'highlights_text': highlights_text, 'total': r.total})
    form = SimpleSearchForm()
    form.fields['sources'].queryset = form.fields['sources'].queryset.filter(owner=request.user)
    return render(request, 'search/simple_search_query_form.html', {'form': form})


class NewsListView(ListView):
    model = News

    def get_queryset(self):
        pass


def saved_search_all_news(request, pk):
    k = int(pk)
    if request.method == 'GET':
        sq = SearchQuery.objects.get(id=k)
        r_all = sq.get_founded_news_all()
        total = len(r_all)
        ids = set(map(lambda x: x['id'], r_all))
        view_ids = set(sq.viewed_news.values_list('id', flat=True))
        del_ids = set(sq.deleted_news.values_list('id', flat=True))
        app_ids = set(sq.approved_news.values_list('id', flat=True))

        highlights_title = {key: value for (key, value) in
                            list(map(lambda x: [x['id'], x['highlight']['title']], r_all))}
        highlights_text = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['text']], r_all))}

        news_list = News.objects.filter(id__in=ids).values('source__name', 'id', 'url', 'title', 'text', 'date_post')
        return render(request, 'search/news_found_list_marks.html',
                      {'news_list': news_list, 'highlights_title': highlights_title, 'highlights_text': highlights_text,
                       'total': total, 'query': sq,
                       'view_ids': view_ids,
                       'del_ids': del_ids,
                       'app_ids': app_ids,
                       })


def saved_search_in_news(request, pk):
    k = int(pk)
    if request.method == 'GET':
        sq = SearchQuery.objects.get(id=k)
        # todo переделать запрос что бы брал только нужное только фходящие
        r = sq.get_founded_news_all()

        all_ids = set(map(lambda x: int(x['id']), r))
        view_ids = set(sq.viewed_news.values_list('id', flat=True))
        del_ids = set(sq.deleted_news.values_list('id', flat=True))
        app_ids = set(sq.approved_news.values_list('id', flat=True))
        ids = list((all_ids.union(view_ids) - del_ids - app_ids))

        r = search_by_id(ids, sq)
        total = len(r)

        ids = list(map(lambda x: x['id'], r))

        highlights_title = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['title']], r))}
        highlights_text = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['text']], r))}

        news_list = News.objects.filter(id__in=ids).values('source__name', 'id', 'url', 'title', 'text', 'date_post')
        return render(request, 'search/news_found_list_marks.html',
                      {'news_list': news_list, 'highlights_title': highlights_title, 'highlights_text': highlights_text,
                       'total': total, 'query': sq,
                       'view_ids': view_ids,
                       'del_ids': del_ids,
                       'app_ids': app_ids,
                       })


def saved_search_deleted_news(request, pk):
    k = int(pk)
    if request.method == 'GET':
        sq = SearchQuery.objects.get(id=k)
        r = sq.get_deleted_news_all()

        view_ids = set(sq.viewed_news.values_list('id', flat=True))
        del_ids = set(sq.deleted_news.values_list('id', flat=True))
        app_ids = set(sq.approved_news.values_list('id', flat=True))

        total = len(r)

        highlights_title = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['title']], r))}
        highlights_text = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['text']], r))}

        news_list = News.objects.filter(id__in=sq.deleted_news.all()).values('source__name', 'id', 'url', 'title', 'text', 'date_post')

        return render(request, 'search/news_found_list_marks.html',
                      {'news_list': news_list, 'highlights_title': highlights_title, 'highlights_text': highlights_text,
                       'total': total, 'query': sq,
                       'view_ids': view_ids,
                       'del_ids': del_ids,
                       'app_ids': app_ids,
                       })


def saved_search_approved_news(request, pk):
    k = int(pk)
    if request.method == 'GET':
        sq = SearchQuery.objects.get(id=k)
        r = sq.get_founded_news_all()
        total = len(r)

        view_ids = set(sq.viewed_news.values_list('id', flat=True))
        del_ids = set(sq.deleted_news.values_list('id', flat=True))
        app_ids = set(sq.approved_news.values_list('id', flat=True))

        highlights_title = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['title']], r))}
        highlights_text = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['text']], r))}

        news_list = News.objects.filter(id__in=sq.approved_news.all()).values('source__name', 'id', 'url', 'title', 'text', 'date_post')

        return render(request, 'search/news_found_list_marks.html',
                      {'news_list': news_list, 'highlights_title': highlights_title, 'highlights_text': highlights_text,
                       'total': total, 'query': sq,
                       'view_ids': view_ids,
                       'del_ids': del_ids,
                       'app_ids': app_ids,
                       })


def saved_search_viewed_news(request, pk):
    k = int(pk)
    if request.method == 'GET':
        sq = SearchQuery.objects.get(id=k)

        r = sq.get_viewed_news_all()

        total = len(r)

        view_ids = set(sq.viewed_news.values_list('id', flat=True))
        del_ids = set(sq.deleted_news.values_list('id', flat=True))
        app_ids = set(sq.approved_news.values_list('id', flat=True))

        highlights_title = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['title']], r))}
        highlights_text = {key: value for (key, value) in list(map(lambda x: [x['id'], x['highlight']['text']], r))}

        news_list = News.objects.filter(id__in=sq.viewed_news.all()).values('source__name', 'id', 'url', 'title', 'text', 'date_post')
        return render(request, 'search/news_found_list_marks.html',
                      {'news_list': news_list, 'highlights_title': highlights_title, 'highlights_text': highlights_text,
                       'total': total, 'query': sq,
                       'view_ids': view_ids,
                       'del_ids': del_ids,
                       'app_ids': app_ids,
                       })


def saved_search_mark_read_one(request, ss_id, news_id):
    sq = SearchQuery.objects.get(id=int(ss_id))
    if sq.viewed_mark_news(news_id):
        return HttpResponse('ss_id: {} <br> news_id: {} <br> OK'.format(ss_id, news_id))
    else:
        return HttpResponse('ss_id: {} <br> news_id: {} <br>'.format(ss_id, news_id))


def saved_search_del_news_one(request, ss_id, news_id):
    sq = SearchQuery.objects.get(id=int(ss_id))
    if sq.delete_news(news_id):
        return HttpResponse('ss_id: {} <br> news_id: {} <br> OK'.format(ss_id, news_id))
    else:
        return HttpResponse('ss_id: {} <br> news_id: {} <br>'.format(ss_id, news_id))


def saved_search_approve_news_one(request, ss_id, news_id):
    sq = SearchQuery.objects.get(id=int(ss_id))
    if sq.approve_news(news_id):
        return HttpResponse('ss_id: {} <br> news_id: {} <br> OK'.format(ss_id, news_id))
    else:
        return HttpResponse('ss_id: {} <br> news_id: {} <br>'.format(ss_id, news_id))


def export_doc_simple(request, pk):
    k = int(pk)
    if request.method == 'GET':
        sc = SearchQuery.objects.get(id=k)
        # todo approved, date moscow
        news = sc.approved_news.all()
        doc = DocxTemplate("search/reports/report_simple.docx")
        context = {'query': sc, 'news': news}
        doc.render(context)
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        response['Content-Disposition'] = 'attachment; filename=report_simple.docx'
        doc.save(response)
        return response
    return HttpResponse('Что то пошло не так')


def export_html_simple(request, pk):
    k = int(pk)
    if request.method == 'GET':
        sc = SearchQuery.objects.get(id=k)
        # todo approved, date moscow
        news = sc.approved_news.order_by('-date_post').values_list('date_post', flat=True)
        img = chart(news)
        html = render(request, 'search/reports/export_htm_simple.html', {'source_query': sc, 'chart': img})

        response = HttpResponse(html, content_type='text/html')
        response['Content-Disposition'] = 'attachment; filename=export_html_simple.html'
        return response
    return HttpResponse('Что то пошло не так')


def export_html_simple_all(request, pk):
    k = int(pk)
    if request.method == 'GET':
        sc = SearchQuery.objects.get(id=k)
        # todo approved, date moscow
        news = sc.get_all_news_set().values_list('date_post', flat=True)
        img = chart(news)
        html = render(request, 'search/reports/export_htm_simple_all.html', {'source_query': sc, 'chart': img})

        response = HttpResponse(html, content_type='text/html')
        response['Content-Disposition'] = 'attachment; filename=export_html_simple.html'
        return response
    return HttpResponse('Что то пошло не так')


def export_html_simple_group(request):
    if request.method == 'POST':
        # todo approved, date moscow
        form = SavedSearchQueryGroupReportForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            sc = data['saved_search_queries']
        html = render(request, 'search/reports/export_htm_simple_group.html', {'sc': sc})

        response = HttpResponse(html, content_type='text/html')
        response['Content-Disposition'] = 'attachment; filename=export_html_simple_group.html'
        return response
    form = SavedSearchQueryGroupReportForm()
    return render(request, 'search/saved_search_query_group_report_form.html', {'form': form})


# Helpers
# Закрыватель вкладок, не знаю как сделать лучше
def close(request):
    return HttpResponse("<script type='text/javascript'>window.close();</script>")


def xml_news(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        n_ids = data['news']
        news = News.objects.filter(id__in=n_ids)
        xml = make_xml_v2_news(news)
        # xml = ''
        response = HttpResponse(xml, content_type='text/xml')
        response['Content-Disposition'] = 'attachment; filename=report.xml'
        return response