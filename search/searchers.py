from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, A
from elasticsearch_dsl.query import Match, MultiMatch, QueryString, Q, MatchAll
from django.conf import settings
from collections import namedtuple
from catalog.models import SourceFull
from django.utils import timezone


# client = Elasticsearch(
#     [settings.ES_SERVER + '/' + settings.ES_DEFAULT_INDEX + '/' + settings.ES_DEFAULT_DOC_TYPE + '/', ],
#     http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
#     port=9200,
#
# )


# s = s.highlight_options(order='score')


# m = Match(using=client)
# mm = MultiMatch(using=client)


def simple_search(query_string, source_queries=[], date_from=False, date_to=False):
    client = Elasticsearch(
        [settings.ES_SERVER + '/' + settings.ES_DEFAULT_INDEX + '/' + settings.ES_DEFAULT_DOC_TYPE + '/', ],
        http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
        port=9200,
    )
    Response = namedtuple('Response', ['dict', 'total'])

    s = Search().using(client)
    qs = QueryString(query=query_string, fields=['text', 'title'])

    if source_queries:
        sf = set()
        for i in source_queries:
            for j in i.sources_full.all():
                if j:
                    sf.add(j.name)
        filters = list(map(lambda x: Q('match', source=x), sf))
        s = s.query(Q('bool', should=filters))

    if date_from:
        if date_to:
            s = s.filter('range', **{'post_date': {'gte': date_from, 'lte': date_to, 'time_zone': '+03:00'}})
        s = s.filter('range', **{'post_date': {'gte': date_from, 'lte': 'now', 'time_zone': '+03:00'}})

    s = s.query(qs)
    s = s.highlight('title', fragment_size=50, pre_tags=["", ], post_tags=["", ])
    s = s.highlight('text', fragment_size=50, pre_tags=["", ], post_tags=["", ])

    response = s[0:1000].execute(ignore_cache=True)
    total = response.hits.total
    print(total)
    r = Response(dict=response.to_dict(), total=total)
    return r


def count_query(query_string, source_queries=False, date_from=False, date_to=False):
    client = Elasticsearch(
        [settings.ES_SERVER + '/' + settings.ES_DEFAULT_INDEX + '/' + settings.ES_DEFAULT_DOC_TYPE + '/', ],
        http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
        port=9200,
    )
    s = Search().using(client).source(fields=['', ])
    qs = QueryString(query=query_string, fields=['text', 'title'])

    if source_queries:
        sf = set()
        for j in source_queries.sources_full.all():
            if j:
                sf.add(j.name)
        filters = list(map(lambda x: Q('match', source=x), sf))
        s = s.query(Q('bool', should=filters))

    if date_from:
        if date_to:
            s = s.filter('range', **{'post_date': {'gte': date_from, 'lte': date_to, 'time_zone': '+03:00'}})
        s = s.filter('range', **{'post_date': {'gte': date_from, 'lte': 'now', 'time_zone': '+03:00'}})

    s = s.query(qs)

    return s.count()


def count_all():
    client = Elasticsearch(
        [settings.ES_SERVER, ],
        http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
        port=9200,
    )
    s = Search().using(client).source(fields=['', ])
    q = MatchAll('')
    s = s.query(q)
    return s.count()


def count_today():
    client = Elasticsearch(
        [settings.ES_SERVER, ],
        http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
        port=9200,
    )
    s = Search().using(client).source(fields=['', ])
    q = MatchAll('')
    s = s.query(q)
    s = s.filter('range', **{'post_date': {'gt': 'now-1d/d', 'lte': 'now/d', 'time_zone': '+03:00'}})

    return s.count()


def count_yesterday():
    client = Elasticsearch(
        [settings.ES_SERVER, ],
        http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
        port=9200,
    )
    s = Search().using(client).source(fields=['', ])
    q = MatchAll('')
    s = s.query(q)
    s = s.filter('range', **{'post_date': {'gte': 'now-1d/d', 'lt': 'now/d', 'time_zone': '+03:00'}})
    return s.count()


def count_source_news(source):
    pass


def last_news_date(source):
    pass


def found_query(query_string, source_queries=False, date_from=False, date_to=False, only_id=False):
    client = Elasticsearch(
        [settings.ES_SERVER, ],
        http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
        port=9200,
    )

    if only_id:
        s = Search(using=client, index='news', doc_type='news').source(fields=['id', ])
    else:
        # todo .source(fields=['id', ]) попробовать ято бы возвращал только id b хайлайты
        s = Search(using=client, index='news', doc_type='news').source(fields=['id', ])

    s = s.params(size=8000)
    qs = QueryString(query=query_string, fields=['text', 'title'])

    if source_queries:
        sf = set()
        for j in source_queries.sources_full.all():
            if j:
                sf.add(j.name)
        filters = list(map(lambda x: Q('match', source=x), sf))
        s = s.query(Q('bool', should=filters))

    if date_from:
        if date_to:
            s = s.filter('range', **{'post_date': {'gte': date_from, 'lte': date_to, 'time_zone': '+03:00'}})
        s = s.filter('range', **{'post_date': {'gte': date_from, 'lte': 'now', 'time_zone': '+03:00'}})

    s = s.query(qs)

    if not only_id:
        s = s.highlight('title', fragment_size=50, pre_tags=["", ], post_tags=["", ])
        s = s.highlight('text', fragment_size=50, pre_tags=["", ], post_tags=["", ])

    # r = dict(map(lambda x: ))
    def title_l(x):
        try:
            return x.meta.highlight.title
        except Exception as e:
            return []

    def text_l(x):
        try:
            return x.meta.highlight.text
        except Exception as e:
            return []

    return list(map(lambda x: dict(id=x.id, highlight=dict(title=title_l(x), text=text_l(x))), s.scan()))


def get_highlights(query_string, news):

    client = Elasticsearch(
        [settings.ES_SERVER, ],
        http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
        port=9200,
    )

    s = Search(using=client, index='news', doc_type='news').source(fields=['id', ]).query('match', id=news.id)
    s = s.params(size=8000)
    qs = QueryString(query=query_string, fields=['text', 'title'])
    s = s.highlight('title', fragment_size=100, pre_tags=["<strong>", ], post_tags=["</strong>", ])
    s = s.highlight('text', fragment_size=100, pre_tags=["<strong>", ], post_tags=["</strong>", ])
    s = s.query(qs)

    resp = s.execute()

    return resp[0].meta.highlight


# todo переделать на scan()
def search_by_id(ids, saved_search):
    client = Elasticsearch(
        [settings.ES_SERVER, ],
        http_auth=(settings.NGINX_USER, settings.NGINX_PASS),
        port=9200,
    )

    if len(ids) > 1000:
        l = []
        for i in range(0, len(ids)+1, 1000):
            s = Search().using(client)
            s = s.params(size=8000)
            qs = QueryString(query=saved_search.query_string, fields=['text', 'title'])

            q_id = Q('bool',
                     must=[Q('terms', id=ids[i:i+1000])],
                     should=[qs])
            s = s.query(q_id)
            s = s.highlight('title', fragment_size=50, pre_tags=["", ], post_tags=["", ])
            s = s.highlight('text', fragment_size=50, pre_tags=["", ], post_tags=["", ])

            def title_l(x):
                try:
                    return x.meta.highlight.title
                except Exception as e:
                    return []

            def text_l(x):
                try:
                    return x.meta.highlight.text
                except Exception as e:
                    return []

            l.extend(list(map(lambda x: dict(id=x.id, highlight=dict(title=title_l(x), text=text_l(x))), s.scan())))
        print('len {}'.format(str(len(l))))
        return l
    else:
        s = Search().using(client)
        s = s.params(size=8000)
        qs = QueryString(query=saved_search.query_string, fields=['text', 'title'])
        q_id = Q('bool',
                 must=[Q('terms', id=ids)],
                 should=[qs])
        s = s.query(q_id)
        s = s.highlight('title', fragment_size=50, pre_tags=["", ], post_tags=["", ])
        s = s.highlight('text', fragment_size=50, pre_tags=["", ], post_tags=["", ])

        def title_l(x):
            try:
                return x.meta.highlight.title
            except Exception as e:
                return []

        def text_l(x):
            try:
                return x.meta.highlight.text
            except Exception as e:
                return []
        l = list(map(lambda x: dict(id=x.id, highlight=dict(title=title_l(x), text=text_l(x))), s.scan()))
        return l
