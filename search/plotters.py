from matplotlib import rc
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from search.models import SearchQuery
from django.utils import timezone
from collections import OrderedDict
from io import BytesIO
import base64


def chart(news):
    # sq = SearchQuery.objects.get(id=sq_id)

    font = {'family': 'Droid Sans', 'weight': 'normal'}
    rc('font', **font)

    hours = timezone.timedelta(hours=3)

    dots = OrderedDict()
    for i in news:
        d = i + hours
        s = timezone.datetime.strftime(d, '%Y-%m')
        if s not in dots:
            dots.update([(s, dict(x=d, y=1))])
        else:
            dots[s]['y'] += 1

    x = []
    y = []
    xticks = []
    for k, v in dots.items():

        xticks.append(k)
        x.append(v['x'])
        y.append(v['y'])

    pyplot.figure(1, figsize=(12, 5))
    pyplot.plot(x, y)
    pyplot.xticks(x, xticks, rotation=90, fontsize=8)

    data = BytesIO()
    pyplot.savefig(data, format='png')
    r = base64.b64encode(data.getvalue())
    data.close()
    pyplot.close()
    return r
