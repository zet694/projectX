from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.admin.views.decorators import staff_member_required

from search.views import *

urlpatterns = [

                  url(r'^$', search, name='search'),

                  # ListViews
                  url(r'^searchquery_list/$', staff_member_required(SearchQueryListView.as_view()),
                      name='searchquery_list'),

                  # CreateViews
                  url(r'^searchquery_form/$', staff_member_required(SearchQueryCreateView.as_view()),
                      name='searchquery_form'),

                  # DetailViews
                  url(r'^news_detail/(?P<pk>[0-9]+)$', staff_member_required(NewsDetailView.as_view()),
                      name='news_detail'),

                  # Update
                  url(r'^searchquery_update/(?P<pk>[0-9]+)/$', staff_member_required(SearchQueryUpdateView.as_view()),
                      name='searchquery_update'),

                  # Delete
                  url(r'^searchquery_delete/(?P<pk>[0-9]+)/$', staff_member_required(searchquery_delete),
                      name='searchquery_delete'),

                  # Helpers
                  url(r'^simple_search_query_form/$', staff_member_required(simple_search_query_form),
                      name='simple_search_query_form'),
                  # показать все найденные новости по сохраненному запросу
                  url(r'^saved_search_all_news/(?P<pk>[0-9]+)/$', staff_member_required(saved_search_all_news),
                      name='saved_search_all_news'),
                  # показать все не обработанные новости inbox
                  url(r'^saved_search_in_news/(?P<pk>[0-9]+)/$', staff_member_required(saved_search_in_news),
                      name='saved_search_in_news'),
                  # показать все просмотренные новости
                  url(r'^saved_search_viewed_news/(?P<pk>[0-9]+)/$', staff_member_required(saved_search_viewed_news),
                      name='saved_search_viewed_news'),
                  # показать все удаленные новости
                  url(r'^saved_search_deleted_news/(?P<pk>[0-9]+)/$', staff_member_required(saved_search_deleted_news),
                      name='saved_search_deleted_news'),
                  # показать все Одобренные новости
                  url(r'^saved_search_approved_news/(?P<pk>[0-9]+)/$',
                      staff_member_required(saved_search_approved_news),
                      name='saved_search_approved_news'),

                  # todo После переноса действий над объектами из вьюшки в модели , посмотреть можно ли сюда запихать
                  # эти методы без перехода во вьюшку'

                  # Пометить новость как прочитанную
                  url(r'^saved_search_mark_read_one/(?P<ss_id>[0-9]+)/(?P<news_id>[0-9]+)/$',
                      staff_member_required(saved_search_mark_read_one),
                      name='saved_search_mark_read_one'),
                  # Переместить новость в карзину
                  url(r'^saved_search_del_news_one/(?P<ss_id>[0-9]+)/(?P<news_id>[0-9]+)/$',
                      staff_member_required(saved_search_del_news_one),
                      name='saved_search_del_news_one'),
                  # Пометить новость как одобренную
                  url(r'^saved_search_approve_news_one/(?P<ss_id>[0-9]+)/(?P<news_id>[0-9]+)/$',
                      staff_member_required(saved_search_approve_news_one),
                      name='saved_search_approve_news_one'),

                  # простой отчет DOCX
                  url(r'^export_doc_simple/(?P<pk>[0-9]+)/$', staff_member_required(export_doc_simple),
                      name='export_doc_simple'),
                  # простой отчет HTML
                  url(r'^export_html_simple/(?P<pk>[0-9]+)/$', staff_member_required(export_html_simple),
                      name='export_html_simple'),
                  # простой отчет HTML для категории ALL
                  url(r'^export_html_simple_all/(?P<pk>[0-9]+)/$', staff_member_required(export_html_simple_all),
                      name='export_html_simple_all'),
                  # простой отчет HTML групповой
                  url(r'^export_html_simple_group/$', staff_member_required(export_html_simple_group),
                      name='export_html_simple_group'),

                  # url(r'^news_found_list/$', staff_member_required(news_found_list),
                  #     name='news_found_list'),
                  url(r'^close$', close, name='close'),
                  url(r'^make_xml/$', xml_news),


              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
