from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from search.searchers import count_query, found_query, search_by_id, get_highlights
from crowler.models import News
from catalog.models import SourceQuery


# Create your models here.


class SearchQuery(models.Model):
    name = models.CharField(max_length=150, verbose_name='Наименование')
    owner = models.ForeignKey(User, verbose_name='Владелец', blank=True, null=True, default=None)
    source_query = models.ForeignKey(SourceQuery, blank=True, verbose_name='Сохраненный запрос по источникам',
                                     default=None, null=True)
    query_string = models.TextField(verbose_name='Строка запроса', blank=True)
    on_monitor = models.BooleanField(verbose_name='На мониторинге', default=False)
    date_start = models.DateTimeField(verbose_name='Дата начала мониторинга', default=timezone.datetime.now, blank=True)
    date_end = models.DateTimeField(verbose_name='Дата окончания мониторинга', blank=True, null=True)
    approved_news = models.ManyToManyField(News, blank=True, verbose_name='Сохраненные новости', editable=False, )
    deleted_news = models.ManyToManyField(News, blank=True, verbose_name='Удаленные новости', editable=False,
                                          related_name='search_query_deleted_news')
    viewed_news = models.ManyToManyField(News, blank=True, verbose_name='Просмотренные новости', editable=False,
                                         related_name='search_query_viewed_news')

    class Meta:
        verbose_name = 'Поисковый запрос'
        verbose_name_plural = 'Поисковые запросы  по источникам'

    def __str__(self):
        return self.name

    def get_count_all(self):
        c = count_query(self.query_string,
                        source_queries=self.source_query,
                        date_from=self.date_start,
                        date_to=self.date_end
                        )
        return c

    def get_founded_news_all(self):
        return found_query(self.query_string,
                           source_queries=self.source_query,
                           date_from=self.date_start,
                           date_to=self.date_end)

    # Список id всех найденых нвостей
    def get_founded_news_all_id_only(self):
        return found_query(self.query_string,
                           source_queries=self.source_query,
                           date_from=self.date_start,
                           date_to=self.date_end, only_id=True)

    # QuerySet новостей
    def get_all_news_set(self):
        es_all = self.get_founded_news_all_id_only()
        ids = set(map(lambda x: x['id'], es_all))
        return News.objects.all().filter(id__in=ids).order_by('-date_post')

    # QuerySet новостей + все хайлайты по текущему запросу
    def get_all_news_set_highlight(self):
        es_all = self.get_founded_news_all()
        ids = set(map(lambda x: x['id'], es_all))
        highlights_title = {key: value for (key, value) in
                            list(map(lambda x: [x['id'], x['highlight']['title']], es_all))}
        highlights_text = {key: value for (key, value) in
                           list(map(lambda x: [x['id'], x['highlight']['text']], es_all))}
        news_list = News.objects.all().filter(id__in=ids).order_by('-date_post')
        return {'news_list': news_list, 'highlights_title': highlights_title, 'highlights_text': highlights_text}

    def get_count_inbox_news(self):
        r = self.get_founded_news_all()
        all_ids = set(map(lambda x: int(x['id']), r))
        view_ids = set(self.viewed_news.values_list('id', flat=True))
        del_ids = set(self.deleted_news.values_list('id', flat=True))
        app_ids = set(self.approved_news.values_list('id', flat=True))
        c = len((all_ids - view_ids - del_ids - app_ids))
        return c

    def get_inbox_news_all(self):
        # todo вот это
        pass

    def get_approved_news_all(self):
        return search_by_id(list(self.approved_news.all().values_list('id', flat=True)), self)

    def get_viewed_news_all(self):
        return search_by_id(list(self.viewed_news.all().values_list('id', flat=True)), self)

    def get_deleted_news_all(self):
        return search_by_id(list(self.deleted_news.all().values_list('id', flat=True)), self)

    # todo почистить от ненужного
    # Операции с состоянием новостей
    def delete_news(self, news_id):
        try:
            if isinstance(news_id, list):
                news_id = set(map(lambda x: int(x), news_id))
                news = News.objects.filter(id__in=news_id)
            else:
                news = News.objects.get(id=int(news_id))
            self.approved_news.remove(news)
            self.deleted_news.add(news)
            return True
        except Exception as e:
            print(e)
            return False

    def approve_news(self, news_id):
        try:
            if isinstance(news_id, list):
                news_id = set(map(lambda x: int(x), news_id))
                news = News.objects.filter(id__in=news_id)
            else:
                news = News.objects.get(id=int(news_id))
            self.viewed_news.remove(news)
            self.deleted_news.remove(news)
            self.approved_news.add(news)
            return True
        except Exception as e:
            print(e)
            return False

    def viewed_mark_news(self, news_id):
        try:
            if isinstance(news_id, list):
                news_id = set(map(lambda x: int(x), news_id))
                news = News.objects.filter(id__in=news_id)
            else:
                news = News.objects.get(id=int(news_id))
            self.viewed_news.add(news)
            return True
        except Exception as e:
            print(e)
            return False

    # todo перенести функционал из вьюшки
    def get_count_approved(self):
        # return int and ids
        pass

    def undo_news(self, ids):
        pass

    def on_monit(self):
        pass

    def off_monit(self):
        pass

    def unload(self):
        pass

    def get_highlights(self, news):

        return get_highlights(self.query_string, news)
