from django.conf.urls import url, include
from rest_framework import routers
from api import views
from rest_framework.urlpatterns import format_suffix_patterns

router = routers.DefaultRouter()
# router.register(r'(?P<pr_id>[0-9]+)/news', views.ProjectNewsViewSet.as_view())
router.register(r'sourcefull', views.SourceFullViewSet)
router.register(r'smitype', views.SmiTypeViewSet)
# router.register(r'^project/status/create$', views.StatusCreate)
# router.register(r'sourcefull-detail', views.SourceFullDetail)

urlpatterns = [
    # url(r'^', include(router.urls)),
    # Новости

    url(r'^news/(?P<pk>[0-9]+)/$', views.ProjectNewsMinDetail.as_view()),
    url(r'^news/detail/(?P<pk>[0-9]+)/$', views.ProjectNewsDetail.as_view()),
    url(r'^news/detail/analytics/num/(?P<pk>[0-9]+)/$', views.ParamNumPNews.as_view()),
    url(r'^news/detail/analytics/list/(?P<pk>[0-9]+)/$', views.ParamListPNews.as_view()),
    # url(r'^news/detail/analytics/bool/(?P<pk>[0-9]+)/$', views.ProjectNewsDetail.as_view()),
    url(r'^news/update/(?P<pk>[0-9]+)/$', views.ProjectNewsUpdate.as_view()),
    url(r'^news/min/update/$', views.ProjectNewsMinUpdate.as_view()),
    url(r'^news/ptr/copy/$', views.ProjectNewsPTRCopy.as_view()),
    url(r'^news/ptr/move/$', views.ProjectNewsPTRMove.as_view()),
    url(r'^news/delete/$', views.ProjectNewsDelete.as_view()),
    url(r'^news/create/$', views.ProjectNewsCreate.as_view()),
    url(r'^news/add/param/num/$', views.ProjectNewsAddParamNum.as_view()),
    url(r'^news/add/param/bool/$', views.ProjectNewsAddParamBool.as_view()),
    url(r'^news/add/param/list/$', views.ProjectNewsAddParamList.as_view()),
    url(r'^news/highlight/(?P<n_pk>[0-9]+)/(?P<q_pk>[0-9]+)/$', views.NewsHighlight.as_view()),


    # Статусы
    url(r'^project/(?P<pr_id>[0-9]+)/statuses/$', views.StatusList.as_view()),
    url(r'^project/status/create/$', views.StatusCreate.as_view()),
    url(r'^project/status/update/$', views.StatusUpdate.as_view()),
    url(r'^project/status/delete/$', views.StatusDelete.as_view()),
    # Тональность
    url(r'^project/(?P<pr_id>[0-9]+)/tonalities/$', views.TonalityList.as_view()),
    url(r'^project/tonality/create/$', views.TonalityCreate.as_view()),
    url(r'^project/tonality/update/$', views.TonalityUpdate.as_view()),
    url(r'^project/tonality/delete/$', views.TonalityDelete.as_view()),
    # url(r'^project/tonality/delete/$', views.TonalityDelete.as_view()),

    # Источники
    url(r'^source/all/$', views.SourceFullList.as_view()),
    # Проекты
    url(r'^project/all/$', views.ProjectList.as_view()),
    url(r'^project/(?P<pr_id>[0-9]+)/news/$', views.ProjectNewsList.as_view()),
    url(r'^project/news/add/$', views.News_PTR.as_view()),
    # Темы
    url(r'^project/(?P<pr_id>[0-9]+)/theme/all/$', views.ThemeList.as_view()),
    # Рубрики
    url(r'^theme/(?P<t_id>[0-9]+)/rubric/all/$', views.RubricList.as_view()),
    # Аналитики
    # Числовые
    url(r'^project/(?P<pr_id>[0-9]+)/analytics/num/$', views.AnalyticNumList.as_view()),
    # Булевы
    url(r'^project/(?P<pr_id>[0-9]+)/analytics/bool/$', views.AnalyticBoolList.as_view()),
    url(r'^analytic/bool/(?P<a_id>[0-9]+)/params/$', views.ParamBoolList.as_view()),
    # Списочные
    url(r'^project/(?P<pr_id>[0-9]+)/analytics/list/$', views.AnalyticListList.as_view()),
    url(r'^analytic/list/(?P<a_id>[0-9]+)/params/$', views.ParamListList.as_view()),

    # Запросы

    url(r'^query/news/all/(?P<id>[0-9]+)/$', views.NewsList.as_view(), {'type': 'all'}),
    url(r'^query/news/in/(?P<id>[0-9]+)/$', views.NewsList.as_view(), {'type': 'in'}),
    url(r'^query/news/view/(?P<id>[0-9]+)/$', views.NewsList.as_view(), {'type': 'view'}),
    url(r'^query/news/del/(?P<id>[0-9]+)/$', views.NewsList.as_view(), {'type': 'del'}),
    url(r'^query/news/app/(?P<id>[0-9]+)/$', views.NewsList.as_view(), {'type': 'app'}),
    url(r'^query/news/modify/in/(?P<id>[0-9]+)/$', views.NewsModify.as_view(), {'type': 'in'}),
    url(r'^query/news/modify/view/(?P<id>[0-9]+)/$', views.NewsModify.as_view(), {'type': 'view'}),
    url(r'^query/news/modify/del/(?P<id>[0-9]+)/$', views.NewsModify.as_view(), {'type': 'del'}),
    url(r'^query/news/modify/app/(?P<id>[0-9]+)/$', views.NewsModify.as_view(), {'type': 'app'}),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
