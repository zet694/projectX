from django.shortcuts import render
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from projects.models import ProjectNews
from catalog.models import SourceFull
from search.models import SearchQuery
from rest_framework import generics, mixins
from api.serializers import *
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from django.utils import timezone
import json
from crowler.models import News
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom import minidom
from django.http import HttpResponse


# Create your views here.

# Новости Проекта
class ProjectNewsCreate(generics.CreateAPIView):
    serializer_class = ProjectNewsSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def create(self, request, *args, **kwargs):
        try:
            date_post = timezone.datetime.strptime(request.data.get('date'), '%d-%m-%Y %H:%M GMT%z')
        except ValueError as e:
            print(e)
            date_post = timezone.localtime(timezone.now())
        try:
            project = Project.objects.get(id=request.data.get('project'))
            if request.data.get('title'):
                title = request.data.get('title')
                p_news = ProjectNews.objects.create(title=title)
                project.news.add(p_news)
            else:
                return Response({}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

        try:
            source = SourceFull.objects.get(id=request.data.get('source'))
            p_news.source = source
        except Exception as e:
            print(e)
            source = False

        try:
            theme = Theme.objects.get(id=request.data.get('theme'))
            p_news.theme_set.add(theme)
        except Exception as e:
            print(e)
            theme = False

        try:
            rubric = Rubric.objects.get(id=request.data.get('rubric'))
            p_news.rubric_set.add(rubric)
        except Exception as e:
            print(e)
            rubric = False

        try:
            stat = Status.objects.get(id=request.data.get('status'))
            p_news.status = stat
        except Exception as e:
            print(e)
            stat = False

        try:
            tonality = Tonality.objects.get(id=request.data.get('tonality'))
            p_news.tonality = tonality
        except Exception as e:
            print(e)
            tonality = False

        url = request.data.get('url')
        annotation = request.data.get('annotation')
        text = request.data.get('text')
        note = request.data.get('note')
        p_news.url = url
        p_news.annotation = annotation
        p_news.text = text
        p_news.note = note
        p_news.date_post = date_post
        nums = json.loads(request.data.get('nums'))
        lists = json.loads(request.data.get('lists'))
        bools = json.loads(request.data.get('bools'))

        p_news.analytics_add(nums, lists, bools)
        # p_news.save()
        p_news.refresh_from_db()
        serializer = self.serializer_class(p_news, data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ProjectNewsUpdate(generics.UpdateAPIView):
    serializer_class = ProjectNewsMinDetailsSer
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def get_object(self):
        o = ProjectNews.objects.get(pk=self.request.data.get('news'))
        return o

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        now('title')
        if request.data.get("title"):
            instance.title = request.data.get("title")
        now('annotation')
        if request.data.get("annotation"):
            instance.annotation = request.data.get("annotation")
        now('text')
        if request.data.get("text"):
            instance.text = request.data.get("text")
        now('project')
        if request.data.get("project") != instance.project_set.first().id:
            p = Project.objects.get(id=request.data.get("project"))
            instance.project_set.remove(instance.project_set.first())
            instance.project_set.add(p)
        now('theme')
        if request.data.get("theme"):
            t = Theme.objects.get(id=request.data.get("theme"))
            instance.theme_set.clear()
            instance.theme_set.add(t)
        now('rubric')
        if request.data.get("rubric"):
            r = Rubric.objects.get(id=request.data.get("rubric"))
            instance.rubric_set.clear()
            instance.rubric_set.add(r)
        now('source')
        if request.data.get("source"):
            so = SourceFull.objects.get(id=request.data.get("source"))
            instance.source = so
        now('date')
        if request.data.get("date"):
            try:
                date_post = timezone.datetime.strptime(request.data.get('date'), '%d-%m-%Y %H:%M GMT%z')
                instance.date_post = date_post
            except ValueError as e:
                print(e)
        now('status')

        if request.data.get("status"):
            s = Status.objects.get(id=int(request.data.get("status")))
            instance.status = s
        else:
            instance.status = None
        now('tonality')
        if request.data.get("tonality"):
            t = Tonality.objects.get(id=request.data.get("tonality"))
            instance.tonality = t
        else:
            instance.tonality = None

        now('save')
        instance.save()

        nums = json.loads(request.data.get('nums'))
        lists = json.loads(request.data.get('lists'))
        bools = json.loads(request.data.get('bools'))

        instance.analytics_remove()

        instance.analytics_add(nums, lists, bools)

        instance.save()

        serializer = ProjectNewsSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)


class ProjectNewsPTRCopy(generics.UpdateAPIView):
    serializer_class = ProjectNewsSerializer(many=True)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def get_object(self):
        o = ProjectNews.objects.filter(id__in=self.request.data.getlist('news[]'))
        return o

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # cur_p = Project.objects.get(id=request.data.get("cur_project"))

        if not request.data.get("project") and not request.data.get("rubric") and not request.data.get("theme"):
            serializer = ProjectNewsSerializer(instance)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        d = instance.values(
            'origin_id',
            'url',
            'title',
            'text',
            'date_post',
            'date_add',
            'source_id',
        )

        new = list(map(lambda x: ProjectNews(**x), d))
        for i in new:
            i.save()
        if request.data.get("project"):
            try:
                p = Project.objects.get(id=request.data.get("project"))
                p.news.add(*new)
                p.save()
            except ValueError as e:
                print(e)

        if request.data.get("rubric"):
            try:
                r = Rubric.objects.get(id=request.data.get("rubric"))
                r.news.add(*new)
                r.save()
            except ValueError as e:
                print(e)
        if request.data.get("theme"):
            try:
                t = Theme.objects.get(id=request.data.get("theme"))
                t.news.add(*new)
                t.save()
            except ValueError as e:
                print(e)
        # instance.save()

        serializer = ProjectNewsSerializer(new, many=True, data=request.data)
        serializer.is_valid(raise_exception=True)
        # self.perform_update(serializer)

        return Response(serializer.data)


class ProjectNewsPTRMove(generics.UpdateAPIView):
    serializer_class = ProjectNewsSerializer(many=True)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def get_object(self):
        o = ProjectNews.objects.filter(id__in=self.request.data.getlist('news[]'))
        return o

    def update(self, request, *args, **kwargs):
        news = self.get_object()
        cur_p = Project.objects.get(id=request.data.get("cur_project"))

        if not request.data.get("project") and not request.data.get("rubric") and not request.data.get("theme"):
            serializer = ProjectNewsSerializer(news)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        d = news.values(
            'origin_id',
            'url',
            'title',
            'text',
            'date_post',
            'date_add',
            'source_id',
        )

        new = list(map(lambda x: ProjectNews(**x), d))
        for i in new:
            i.save()

        if request.data.get("project"):
            try:
                p = Project.objects.get(id=request.data.get("project"))

                p.news.add(*new)
                p.save()
                news.delete()
            except ValueError as e:
                print(e)

        if request.data.get("rubric"):
            try:
                r = Rubric.objects.get(id=request.data.get("rubric"))
                r.news.add(*new)
                r.save()
            except ValueError as e:
                print(e)
        if request.data.get("theme"):
            try:
                t = Theme.objects.get(id=request.data.get("theme"))
                t.news.add(*new)
                t.save()
            except ValueError as e:
                print(e)

        new = news.prefetch_related(
            'paramnum_set',
            'paramlist_set',
            'theme_set',
            'rubric_set',
            'p_news_false',
            'p_news_true'
        ).select_related(
            'source',
            'source__location',
            'source__smi_type',
            'status',
            'tonality',
        )

        serializer = ProjectNewsSerializer(new, many=True, data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data)


class ProjectNewsAddParamNum(generics.UpdateAPIView):
    serializer_class = ProjectNewsSerializer(many=True)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def get_object(self):
        o = ProjectNews.objects.filter(id__in=self.request.data.getlist('news[]'))
        return o

    def update(self, request, *args, **kwargs):
        news = self.get_object()
        param = request.data.get("num_param")
        analytic = AnalyticNum.objects.get(id=request.data.get("num_analytic"))
        for n in news:
            analytic.param.add(ParamNum.objects.create(num=param, p_news=n))

        analytic.projectnews_set.add(*news)
        analytic.save()

        news = news.prefetch_related(
            'paramnum_set',
            'paramlist_set',
            'theme_set',
            'rubric_set',
            'p_news_false',
            'p_news_true'
        ).select_related(
            'source',
            'source__location',
            'source__smi_type',
            'status',
            'tonality',
        )

        serializer = ProjectNewsSerializer(news, many=True, data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data)


class ProjectNewsAddParamBool(generics.UpdateAPIView):
    serializer_class = ProjectNewsSerializer(many=True)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def get_object(self):
        o = ProjectNews.objects.filter(id__in=self.request.data.getlist('news[]'))
        return o

    def update(self, request, *args, **kwargs):
        news = self.get_object()
        analytic = AnalyticBool.objects.get(id=request.data.get("bool_analytic"))
        param = int(request.data.get("bool_param"))
        if param == 0:
            analytic.p_news_true.remove(*news)
            analytic.p_news_false.add(*news)
        elif param == 1:
            analytic.p_news_false.remove(*news)
            analytic.p_news_true.add(*news)

        analytic.projectnews_set.add(*news)

        news = news.prefetch_related(
            'paramnum_set',
            'paramlist_set',
            'theme_set',
            'rubric_set',
            'p_news_false',
            'p_news_true'
        ).select_related(
            'source',
            'source__location',
            'source__smi_type',
            'status',
            'tonality',
        )

        serializer = ProjectNewsSerializer(news, many=True, data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data)


class ProjectNewsAddParamList(generics.UpdateAPIView):
    serializer_class = ProjectNewsSerializer(many=True)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def get_object(self):
        o = ProjectNews.objects.filter(id__in=self.request.data.getlist('news[]'))
        return o

    def update(self, request, *args, **kwargs):
        news = self.get_object()
        analytic = AnalyticList.objects.get(id=request.data.get("list_analytic"))
        param = ParamList.objects.get(id=request.data.get("list_param"))

        param.p_news.add(*news)
        analytic.projectnews_set.add(*news)
        news = news.prefetch_related(
            'paramnum_set',
            'paramlist_set',
            'theme_set',
            'rubric_set',
            'p_news_false',
            'p_news_true'
        ).select_related(
            'source',
            'source__location',
            'source__smi_type',
            'status',
            'tonality',
        )

        serializer = ProjectNewsSerializer(news, many=True, data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data)


class ProjectNewsMinDetail(generics.RetrieveAPIView):
    serializer_class = ProjectNewsMinDetailsSer

    def retrieve(self, request, pk=None):
        news = get_object_or_404(ProjectNews, pk=pk)
        serializer = ProjectNewsMinDetailsSer(news)
        return Response(serializer.data)


class ProjectNewsDetail(generics.RetrieveAPIView):
    serializer_class = ProjectNewsSerializer

    def retrieve(self, request, pk=None):
        news = get_object_or_404(ProjectNews, pk=pk)
        serializer = self.serializer_class(news)
        return Response(serializer.data)


class ProjectNewsList(generics.ListAPIView):
    serializer_class = ProjectNewsSerializer

    def get_queryset(self):
        pr_id = self.kwargs['pr_id']

        return Project.objects.get(id=pr_id).news.all().prefetch_related(
            'paramnum_set',
            'paramlist_set',
            'theme_set',
            'rubric_set',
            'p_news_false',
            'p_news_true'
        ).select_related(
            'source',
            'source__location',
            'source__smi_type',
            'status',
            'tonality',
        )

    def get(self, request, *args, **kwargs):
        news = self.get_queryset()
        serializer = self.serializer_class(news, many=True)
        data = serializer.data

        return Response(data)


class ProjectNewsMinUpdate(generics.UpdateAPIView):
    serializer_class = ProjectNewsMinDetailsSer
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def get_object(self):
        o = ProjectNews.objects.get(pk=self.request.data.get('news'))
        return o

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.data.get("title"):
            instance.title = request.data.get("title")
        if request.data.get("annotation"):
            instance.annotation = request.data.get("annotation")
        if request.data.get("text"):
            instance.text = request.data.get("text")
        if request.data.get("id_status"):
            s = Status.objects.get(id=int(request.data.get("id_status")))
            instance.status = s
        if request.data.get("id_tonality"):
            t = Tonality.objects.get(id=request.data.get("id_tonality"))
            instance.tonality = t
        # instance.save()

        serializer = ProjectNewsSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)


class ProjectNewsDelete(generics.DestroyAPIView):
    serializer_class = ProjectNewsMinDetailsSer
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = ProjectNews

    def get_queryset(self):
        n_ids = self.request.data.getlist('news[]')
        o = ProjectNews.objects.filter(id__in=n_ids)
        return o

    def destroy(self, request, *args, **kwargs):
        news = self.get_queryset()
        news.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

        # def delete(self, request,  *args, **kwargs):
        #     object = self.get_object()
        #     object.delete()
        #     return Response(status=status.HTTP_204_NO_CONTENT)


class NewsHighlight(generics.RetrieveAPIView):
    serializer_class = QueryNewsHighlightSer

    def get_object(self):
        return get_object_or_404(News, pk=self.kwargs.get('n_pk'))

    def retrieve(self, request, *args, **kwargs):
        news = self.get_object()
        serializer = self.serializer_class(news, data=request.data, context={'q_pk': self.kwargs.get('q_pk')})
        serializer.is_valid()
        return Response(serializer.data)


# Статусы
class StatusList(generics.ListAPIView):
    serializer_class = StatusSerializer

    def get_queryset(self):
        pr_id = self.kwargs['pr_id']
        return Project.objects.get(id=pr_id).statuses.all()


class StatusCreate(generics.CreateAPIView):
    serializer_class = StatusSerializer

    class Meta:
        model = Status

    def create(self, request, *args, **kwargs):
        serializer = StatusSerializer(data=request.data)
        if serializer.is_valid():
            pr_id = request.data.get('project')
            if pr_id:
                s = serializer.save()
                p = Project.objects.get(id=pr_id)
                p.statuses.add(s)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StatusUpdate(generics.UpdateAPIView):
    serializer_class = StatusSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = Status

    def get_object(self):
        o = Status.objects.get(pk=self.request.data.get('status'))
        return o

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.name = request.data.get("name")
        instance.color = request.data.get("color")

        if not request.data.get("bold"):
            instance.bold = False
        else:
            instance.bold = True

        if not request.data.get("editable"):
            instance.editabe = False
        else:
            instance.editabe = True

        instance.save()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)


class StatusDelete(generics.DestroyAPIView):
    serializer_class = StatusSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = Status

    def get_object(self):
        o = Status.objects.get(pk=self.request.data.get('status'))
        return o


# Тональность
class TonalityList(generics.ListAPIView):
    serializer_class = TonalitySer

    def get_queryset(self):
        pr_id = self.kwargs['pr_id']
        return Project.objects.get(id=pr_id).tonalities.all()


class TonalityCreate(generics.CreateAPIView):
    serializer_class = TonalitySer

    class Meta:
        model = Tonality

    def create(self, request, *args, **kwargs):
        serializer = TonalitySer(data=request.data)
        if serializer.is_valid():
            pr_id = request.data.get('project')
            if pr_id:
                s = serializer.save()
                p = Project.objects.get(id=pr_id)
                p.tonalities.add(s)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TonalityUpdate(generics.UpdateAPIView):
    serializer_class = TonalitySer
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = Tonality

    def get_object(self):
        o = Tonality.objects.get(pk=self.request.data.get('tonality'))
        return o

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.name = request.data.get("name")
        instance.color = request.data.get("color")
        instance.save()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)


class TonalityDelete(generics.DestroyAPIView):
    serializer_class = TonalitySer
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = Tonality

    def get_object(self):
        o = Tonality.objects.get(pk=self.request.data.get('tonality'))
        return o


class SourceFullViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SourceFull.objects.all()
    serializer_class = SourceFullSerializer


class SmiTypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SmiType.objects.all()
    serializer_class = SmiTypeSerializer


# Источники

class SourceFullList(generics.ListAPIView):
    serializer_class = SourceFullSerializer

    def get_queryset(self):
        return SourceFull.objects.all()


# Проекты
class ProjectList(generics.ListAPIView):
    serializer_class = ProjectSer

    def get_queryset(self):
        user = self.request.user
        return user.project_set.all()


class News_PTR(generics.ListCreateAPIView):
    serealizer_class = QueryNewsSer

    def get_queryset(self):
        return News.objects.filter(id__in=self.request.data.getlist('news[]'))

    def create(self, request, *args, **kwargs):
        news = self.get_queryset()
        project = request.data.get('project')
        theme = request.data.get('theme')
        rubric = request.data.get('rubric')
        query = request.data.get('query')

        s = SearchQuery.objects.get(id=query)

        if project:
            p = Project.objects.get(id=project)
            p_news = p.make_p_news(news)
            p.news.add(*p_news)
            s.deleted_news.remove(*news)
            s.viewed_news.remove(*news)
            s.approved_news.add(*news)

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if theme and theme != '---':
            t = Theme.objects.get(id=theme)
            t.news.add(*p_news)
        if rubric and rubric != '---':
            r = Rubric.objects.get(id=rubric)
            r.news.add(*p_news)

        serializer = QueryNewsSer(news, many=True, data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


# Темы
class ThemeList(generics.ListAPIView):
    serializer_class = ThemeMinSer

    def get_queryset(self):
        return Project.objects.get(id=self.kwargs['pr_id']).themes.all()


# Рубрики
class RubricList(generics.ListAPIView):
    serializer_class = RubricMinSer

    def get_queryset(self):
        return Theme.objects.get(id=self.kwargs['t_id']).rubrics.all()


# Аналитики
# Числовые
class AnalyticNumList(generics.ListAPIView):
    serializer_class = AnalyticNumSerializer

    def get_queryset(self):
        return Project.objects.get(id=self.kwargs['pr_id']).num_analytics.all()


class ParamNumPNews(generics.ListAPIView):
    serializer_class = PNewsParamNumSerializer

    def get_queryset(self):
        return ProjectNews.objects.get(id=self.kwargs['pk']).paramnum_set.all()


# Булевы
class AnalyticBoolList(generics.ListAPIView):
    serializer_class = AnalyticBoolSer

    def get_queryset(self):
        return Project.objects.get(id=self.kwargs['pr_id']).bool_analytics.all()


class ParamBoolList(generics.RetrieveAPIView):
    serializer_class = AnalyticBoolSer

    def retrieve(self, request, a_id=None):
        an = get_object_or_404(AnalyticBool, pk=a_id)
        serializer = AnalyticBoolSer(an)
        return Response(serializer.data)

        # def get_queryset(self):
        #     return AnalyticBool.objects.filter(id=self.kwargs['a_id'])


# Списочные
class AnalyticListList(generics.ListAPIView):
    serializer_class = AnalyticListSerializerMin

    def get_queryset(self):
        return Project.objects.get(id=self.kwargs['pr_id']).list_analytics.all()


class ParamListList(generics.ListAPIView):
    serializer_class = ParamListSer

    def get_queryset(self):
        return AnalyticList.objects.get(id=self.kwargs['a_id']).params.all()


class ParamListPNews(generics.ListAPIView):
    serializer_class = ParamListSer

    def get_queryset(self):
        return ProjectNews.objects.get(id=self.kwargs['pk']).paramlist_set.all()


# Запросы


# Новости
class NewsList(generics.ListAPIView):
    serializer_class = QueryNewsSer

    def get_queryset(self):
        q = SearchQuery.objects.get(id=self.kwargs['id'])
        if self.kwargs['type'] == 'all':
            n_ids = q.get_founded_news_all_id_only()
            n_ids = list(map(lambda x: x['id'], n_ids))
            return News.objects.filter(id__in=n_ids)

        elif self.kwargs['type'] == 'in':
            all_ids = set(list(map(lambda x: x['id'], q.get_founded_news_all_id_only())))
            view_ids = set(q.viewed_news.values_list('id', flat=True))
            del_ids = set(q.deleted_news.values_list('id', flat=True))
            app_ids = set(q.approved_news.values_list('id', flat=True))
            n_ids = list((((all_ids - view_ids) - del_ids) - app_ids))

            return News.objects.filter(id__in=n_ids)

        elif self.kwargs['type'] == 'view':
            return q.viewed_news.all()

        elif self.kwargs['type'] == 'del':
            return q.deleted_news.all()

        elif self.kwargs['type'] == 'app':
            return q.approved_news.all()


class NewsModify(generics.UpdateAPIView):
    serializer_class = QueryNewsSer(many=True)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    class Meta:
        model = News

    def get_queryset(self):
        return News.objects.filter(id__in=json.loads(self.request.data.get('news')))

    def update(self, request, *args, **kwargs):
        news = self.get_queryset()
        serializer = QueryNewsSer(news, many=True, data=request.data,
                                  context={'type': self.kwargs['type'], 'id': self.kwargs['id']})
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)
