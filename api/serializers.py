from rest_framework import serializers
from projects.models import *
from catalog.models import *
from search.models import SearchQuery
from django.shortcuts import get_object_or_404
import json


class SourceFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = SourceFull
        fields = ('id', 'name', 'smi_type')


class ProjectSer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'name')


class SmiTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SmiType
        fields = '__all__'


class AnalyticNumSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalyticNum
        fields = ('id', 'name')


class PNewsParamNumSerializer(serializers.ModelSerializer):
    analyticnum_set = AnalyticNumSerializer(many=True, read_only=True)

    class Meta:
        model = ParamNum
        fields = ('analyticnum_set', 'id', 'num')
        # depth = 1


class AnalyticListSerializerMin(serializers.ModelSerializer):
    class Meta:
        model = AnalyticList
        fields = ('id', 'name')


class ParamListSer(serializers.ModelSerializer):
    class Meta:
        model = ParamList
        fields = ('id', 'name')


class AnalyticListSer(serializers.ModelSerializer):
    params = ParamListSer(many=True, read_only=True)

    class Meta:
        model = AnalyticList
        fields = ('id', 'name', 'params')


class PNewsParamListSerializer(serializers.ModelSerializer):
    analyticlist_set = AnalyticListSerializerMin(many=True, read_only=True)

    class Meta:
        model = ParamList
        fields = ('analyticlist_set', 'id', 'name')


class PNewsAnalyticBoolTrueSer(serializers.ModelSerializer):
    class Meta:
        model = AnalyticBool
        fields = ('id', 'name', 'name_positive')


class PNewsAnalyticBoolFalseSer(serializers.ModelSerializer):
    class Meta:
        model = AnalyticBool
        fields = ('id', 'name', 'name_negative')


class AnalyticBoolSer(serializers.ModelSerializer):
    class Meta:
        model = AnalyticBool
        fields = ('id', 'name', 'name_negative', 'name_positive')


class PNewsLocationSer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('id', 'name')


class PNewsSmiTypeSer(serializers.ModelSerializer):
    class Meta:
        model = SmiType
        fields = ('id', 'name')


class PNewsSourceFullSerializer(serializers.ModelSerializer):
    location = PNewsLocationSer(read_only=True)
    smi_type = PNewsSmiTypeSer(read_only=True)

    class Meta:
        model = SourceFull
        fields = ('id', 'url', 'name', 'smi_type', 'location')
        # depth = 1


class ThemeMinSer(serializers.ModelSerializer):
    class Meta:
        model = Theme
        fields = ('id', 'name')


class RubricMinSer(serializers.ModelSerializer):
    class Meta:
        model = Rubric
        fields = ('id', 'name')


class ProjectNewsSerializer(serializers.ModelSerializer):
    paramlist_set = PNewsParamListSerializer(many=True, read_only=True)
    paramnum_set = PNewsParamNumSerializer(many=True, read_only=True)
    theme_set = ThemeMinSer(many=True, read_only=True)
    rubric_set = RubricMinSer(many=True, read_only=True)
    source = PNewsSourceFullSerializer(read_only=True)
    p_news_false = PNewsAnalyticBoolFalseSer(many=True, read_only=True)
    p_news_true = PNewsAnalyticBoolTrueSer(many=True, read_only=True)
    project = ProjectSer(source='project_set.first', read_only=True)

    class Meta:
        model = ProjectNews
        fields = (
            'id',
            'title',
            'date_post',
            'date_add',
            'date_journal',
            'source',
            'status',
            'tonality',
            'theme_set',
            'rubric_set',
            'p_news_false',
            'p_news_true',
            'paramlist_set',
            'paramnum_set',
            'note',
            # 'project_set',
            'project')
        # fields = '__all__'
        depth = 1


class MinProjectSer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id',)


class StatusSerializer(serializers.ModelSerializer):
    project_set = MinProjectSer(many=True, read_only=True)

    class Meta:
        model = Status
        fields = ('id', 'name', 'color', 'bold', 'editable', 'project_set')


class TonalitySer(serializers.ModelSerializer):
    project_set = MinProjectSer(many=True, read_only=True)

    class Meta:
        model = Tonality
        fields = ('id', 'name', 'color', 'project_set')


class ProjectNewsMinDetailsSer(serializers.ModelSerializer):
    class Meta:
        model = ProjectNews
        fields = (
            'id', 'title', 'date_post', 'text', 'status', 'tonality', 'note')
        depth = 1


class NewsSer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ('id', 'title', 'text', 'date_post', 'date_add', 'source')


class Source(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ('id', 'name')


class QueryNewsMoveSer(serializers.ListSerializer):
    def update(self, instance, validated_data):
        news = instance

        q_id = self.context.get('id')
        modify_type = self.context.get('type')
        query = SearchQuery.objects.get(id=q_id)

        if modify_type == 'view':
            query.deleted_news.remove(*news)
            query.approved_news.remove(*news)
            query.viewed_news.add(*news)
        elif modify_type == 'in':
            query.deleted_news.remove(*news)
            query.approved_news.remove(*news)
            query.viewed_news.remove(*news)
        elif modify_type == 'del':
            query.approved_news.remove(*news)
            query.viewed_news.remove(*news)
            query.deleted_news.add(*news)
        elif modify_type == 'app':
            query.deleted_news.remove(*news)
            query.viewed_news.remove(*news)
            query.approved_news.add(*news)

        query.save()
        return news


class QueryNewsSer(serializers.ModelSerializer):
    source = Source(read_only=True)

    class Meta:
        list_serializer_class = QueryNewsMoveSer
        model = News
        fields = ('id', 'title', 'text', 'url', 'date_post', 'date_add', 'source')
        depth = 1


class QueryNewsHighlightSer(serializers.ModelSerializer):
    highlights = serializers.SerializerMethodField('get_highlight')

    def get_highlight(self, news):
        q = get_object_or_404(SearchQuery, pk=self.context.get('q_pk'))
        h = q.get_highlights(news)
        d = {'text_high': ''}

        try:
            d.update(text=list(h['text']))
        except KeyError as e:
            print(e)
            d.update(text=None)
        try:
            d.update(title=list(h['title']))
        except KeyError as e:
            print(e)
            d.update(title=None)
        s = news.text
        if d['text']:
            for i in d['text']:
                h = i.replace('<strong>', '').replace('</strong>', '')
                t_l = s.split(h)
                s = '<highlight>{}</highlight>'.format(i).join(t_l)
                d['text_high'] = s
        else:
            d['text_high'] = s
        return d

    class Meta:
        model = News
        fields = ('id', 'title', 'highlights')
