#!/usr/bin/env bash

DJANGO_SETTINGS_MODULE='smimonitor.dev_settings' celery -A smimonitor worker --loglevel=info -n $1@%h -Q $2
