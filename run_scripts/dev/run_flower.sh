#!/usr/bin/env bash

DJANGO_SETTINGS_MODULE='smimonitor.dev_settings' flower -A smimonitor --port=5555 --broker_api=localhost:15672/api/