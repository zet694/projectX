from django.contrib import admin
from .models import SmiType, CoverageLevel, Industry, Language, SourceFull, Location, LocationType, GeoGroup, SourceQuery
# Register your models here.


admin.site.register(SmiType)
admin.site.register(CoverageLevel)
admin.site.register(Industry)
admin.site.register(Language)
admin.site.register(SourceFull)
admin.site.register(Location)
admin.site.register(LocationType)
admin.site.register(GeoGroup)
admin.site.register(SourceQuery)
