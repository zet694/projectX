from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.admin.views.decorators import staff_member_required
from catalog.views import *

urlpatterns = [

                  url(r'^$', catalog, name='catalog'),
                  # ListViews
                  url(r'^sourcefull_list/$', staff_member_required(SourceFullListView.as_view()),
                      name='sourcefull_list'),

                  url(r'^geogroup_list/$', staff_member_required(GeoGroupListView.as_view()), name='geogroup_list'),
                  url(r'^smitype_list/$', staff_member_required(SmiTypeListView.as_view()), name='smitype_list'),
                  url(r'^locationtype_list/$', staff_member_required(LocationTypeListView.as_view()),
                      name='locationtype_list'),
                  url(r'^industry_list/$', staff_member_required(IndustryListView.as_view()), name='industry_list'),
                  url(r'^coveragelevel_list/$', staff_member_required(CoverageLevelListView.as_view()),
                      name='coveragelevel_list'),
                  url(r'^language_list/$', staff_member_required(LanguageListView.as_view()), name='language_list'),
                  url(r'^location_list/$', staff_member_required(LocationListView.as_view()), name='location_list'),
                  url(r'^sourcequery_list/$', staff_member_required(SourceQueryListView.as_view()),
                      name='sourcequery_list'),

                  # CreateViews
                  url(r'^sourcefull_form/$', staff_member_required(SourceFullCreateView.as_view()),
                      name='sourcefull_form'),
                  url(r'^geogroup_form/$', staff_member_required(GeoGroupCreateView.as_view()),
                      name='geogroup_form'),
                  url(r'^smitype_form/$', staff_member_required(SmiTypeCreateView.as_view()),
                      name='smitype_form'),
                  url(r'^locationtype_form/$', staff_member_required(LocationTypeCreateView.as_view()),
                      name='locationtype_form'),
                  url(r'^industry_form/$', staff_member_required(IndustryCreateView.as_view()),
                      name='industry_form'),
                  url(r'^coveragelevel_form/$', staff_member_required(CoverageLevelCreateView.as_view()),
                      name='coveragelevel_form'),
                  url(r'^language_form/$', staff_member_required(LanguageCreateView.as_view()),
                      name='language_form'),
                  url(r'^location_form/$', staff_member_required(LocationCreateView.as_view()),
                      name='location_form'),

                  # Delete
                  url(r'^sourcefull_delete/(?P<pk>[0-9]+)/$', staff_member_required(sourcefull_delete),
                      name='sourcefull_delete'),
                  url(r'^geogroup_delete/(?P<pk>[0-9]+)/$', staff_member_required(geogroup_delete),
                      name='geogroup_delete'),
                  url(r'^smitype_delete/(?P<pk>[0-9]+)/$', staff_member_required(smitype_delete),
                      name='smitype_delete'),
                  url(r'^locationtype_delete/(?P<pk>[0-9]+)/$', staff_member_required(locationtype_delete),
                      name='locationtype_delete'),
                  url(r'^industry_delete/(?P<pk>[0-9]+)/$', staff_member_required(industry_delete),
                      name='industry_delete'),
                  url(r'^coveragelevel_delete/(?P<pk>[0-9]+)/$', staff_member_required(coveragelevel_delete),
                      name='coveragelevel_delete'),
                  url(r'^language_delete/(?P<pk>[0-9]+)/$', staff_member_required(language_delete),
                      name='language_delete'),
                  url(r'^location_delete/(?P<pk>[0-9]+)/$', staff_member_required(location_delete),
                      name='location_delete'),
                  url(r'^sourcequery_delete/(?P<pk>[0-9]+)/$', staff_member_required(sourcequery_delete),
                      name='sourcequery_delete'),

                  # Update
                  url(r'^sourcefull_update_form/(?P<pk>[0-9]+)/$',
                      staff_member_required(SourceFullUpdateView.as_view()), name='sourcefull_update_form'),
                  url(r'^geogroup_update_form/(?P<pk>[0-9]+)/$',
                      staff_member_required(GeoGroupUpdateView.as_view()), name='geogroup_update_form'),
                  url(r'^smitype_update_form/(?P<pk>[0-9]+)/$',
                      staff_member_required(SmiTypeUpdateView.as_view()), name='smitype_update_form'),
                  url(r'^locationtype_update_form/(?P<pk>[0-9]+)/$',
                      staff_member_required(LocationTypeUpdateView.as_view()), name='locationtype_update_form'),
                  url(r'^industry_update_form/(?P<pk>[0-9]+)/$',
                      staff_member_required(IndustryUpdateView.as_view()), name='industry_update_form'),
                  url(r'^coveragelevel_update_form/(?P<pk>[0-9]+)/$',
                      staff_member_required(CoverageLevelUpdateView.as_view()), name='coveragelevel_update_form'),
                  url(r'^language_update_form/(?P<pk>[0-9]+)/$',
                      staff_member_required(LanguageUpdateView.as_view()), name='language_update_form'),
                  url(r'^location_update_form/(?P<pk>[0-9]+)/$',
                      staff_member_required(LocationUpdateView.as_view()), name='location_update_form'),

                  url(r'catalog_search/', catalog_search, name='catalog_search'),
                  url(r'close$', close, name='close'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
