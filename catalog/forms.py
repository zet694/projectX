from django import forms
from catalog.models import *


class SearchSourcesForm(forms.Form):
    name = forms.CharField(required=False, label='Название запроса', max_length=150)
    geo_groups = forms.ModelMultipleChoiceField(required=False, label='ГЕО Группы', queryset=GeoGroup.objects.all(), widget=forms.CheckboxSelectMultiple())
    smi_types = forms.ModelMultipleChoiceField(required=False, label='Типы СМИ', queryset=SmiType.objects.all(), widget=forms.CheckboxSelectMultiple())
    location_types = forms.ModelMultipleChoiceField(required=False, label='Типы локаций', queryset=LocationType.objects.all(), widget=forms.CheckboxSelectMultiple())
    industries = forms.ModelMultipleChoiceField(required=False, label='Отрасли', queryset=Industry.objects.all(), widget=forms.CheckboxSelectMultiple())
    coverage_levels = forms.ModelMultipleChoiceField(required=False, label='Уровни покрытия', queryset=CoverageLevel.objects.all(), widget=forms.CheckboxSelectMultiple())
    languages = forms.ModelMultipleChoiceField(required=False, label='Языки', queryset=Language.objects.all(), widget=forms.CheckboxSelectMultiple())
    locations = forms.ModelMultipleChoiceField(required=False, label='Локации', queryset=Location.objects.all(), widget=forms.CheckboxSelectMultiple())
    sources_full = forms.ModelMultipleChoiceField(required=False, label='Источники', queryset=SourceFull.objects.all(), widget=forms.CheckboxSelectMultiple())
    save_mode = forms.BooleanField(required=False, label='Сохранить?')
    #
    # def clean(self):
    #     cleaned_data = super(SearchSourcesForm, self).clean()
    #     save_mode = cleaned_data.get("save_mode")
    #
    #     if save_mode:
    #         raise forms.ValidationError("Нужно ввести название запроса")
    #     return cleaned_data