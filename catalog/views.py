from django.shortcuts import render
from django.http import Http404
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import ListView, CreateView, UpdateView
from catalog.models import *
from catalog.forms import SearchSourcesForm


# Create your views here.


def catalog(request):
    return render(request, 'catalog/catalog_index.html')


# SolrceFull
class SourceFullListView(ListView):
    model = SourceFull


class SourceFullCreateView(CreateView):
    model = SourceFull
    fields = [
        'name',
        'url',
        'smi_type',
        'lang',
        'coverage_levels',
        'industry',
        'condition',
        'under_control',
        'location',
        'contacts'
    ]
    success_url = '/catalog'


class SourceFullUpdateView(UpdateView):
    model = SourceFull
    template_name = 'catalog/update_form.html'
    success_url = '/catalog/close'
    fields = [
        'name',
        'url',
        'smi_type',
        'lang',
        'coverage_levels',
        'industry',
        'condition',
        'under_control',
        'location',
        'contacts'
    ]


def sourcefull_delete(request, pk):
    if request.method == 'GET':
        try:
            s = SourceFull.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# GeoGroup
class GeoGroupListView(ListView):
    model = GeoGroup


class GeoGroupCreateView(CreateView):
    model = GeoGroup
    fields = ['name', 'description', 'locations']
    success_url = '/catalog'


class GeoGroupUpdateView(UpdateView):
    model = GeoGroup
    template_name = 'catalog/update_form.html'
    success_url = '/catalog/close'
    fields = ['name', 'description', 'locations']


def geogroup_delete(request, pk):
    if request.method == 'GET':
        try:
            s = GeoGroup.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# SmiType
class SmiTypeListView(ListView):
    model = SmiType


class SmiTypeCreateView(CreateView):
    model = SmiType
    fields = ['name', ]
    success_url = '/catalog'


class SmiTypeUpdateView(UpdateView):
    model = SmiType
    template_name = 'catalog/update_form.html'
    success_url = '/catalog/close'
    fields = ['name', ]


def smitype_delete(request, pk):
    if request.method == 'GET':
        try:
            s = SmiType.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# LocationType
class LocationTypeListView(ListView):
    model = LocationType


class LocationTypeCreateView(CreateView):
    model = LocationType
    fields = ['name', ]
    success_url = '/catalog'


class LocationTypeUpdateView(UpdateView):
    model = LocationType
    template_name = 'catalog/update_form.html'
    success_url = '/catalog/close'
    fields = ['name', ]


def locationtype_delete(request, pk):
    if request.method == 'GET':
        try:
            s = LocationType.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# Industry
class IndustryListView(ListView):
    model = Industry


class IndustryCreateView(CreateView):
    model = Industry
    fields = ['name', 'description']
    success_url = '/catalog'


class IndustryUpdateView(UpdateView):
    model = Industry
    template_name = 'catalog/update_form.html'
    success_url = '/catalog/close'
    fields = ['name', 'description']


def industry_delete(request, pk):
    if request.method == 'GET':
        try:
            s = Industry.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# CoverageLevel
class CoverageLevelListView(ListView):
    model = CoverageLevel


class CoverageLevelCreateView(CreateView):
    model = CoverageLevel
    fields = ['name', 'father_level', 'source_full']
    success_url = '/catalog'


class CoverageLevelUpdateView(UpdateView):
    model = CoverageLevel
    template_name = 'catalog/update_form.html'
    success_url = '/catalog/close'
    fields = ['name', 'father_level', 'source_full']


def coveragelevel_delete(request, pk):
    if request.method == 'GET':
        try:
            s = CoverageLevel.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# Language
class LanguageListView(ListView):
    model = Language


class LanguageCreateView(CreateView):
    model = Language
    fields = ['name', ]
    success_url = '/catalog'


class LanguageUpdateView(UpdateView):
    model = Language
    template_name = 'catalog/update_form.html'
    success_url = '/catalog/close'
    fields = ['name', ]


def language_delete(request, pk):
    if request.method == 'GET':
        try:
            s = Language.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# Location
class LocationListView(ListView):
    model = Location


class LocationCreateView(CreateView):
    model = Location
    fields = ['father_location', 'location_type', 'geo_group', 'name', 'full_name']
    success_url = '/catalog'


class LocationUpdateView(UpdateView):
    model = Location
    template_name = 'catalog/update_form.html'
    success_url = '/catalog/close'
    fields = ['father_location', 'location_type', 'geo_group', 'name', 'full_name']


def location_delete(request, pk):
    if request.method == 'GET':
        try:
            s = Location.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# Search
def catalog_search(request):
    if request.method == 'POST':
        form = SearchSourcesForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            q = SourceQuery()
            q.save()
            q.owner = request.user
            q.smi_types = data['smi_types']
            q.name = data['name']
            q.geo_groups = data['geo_groups']
            q.location_types = data['location_types']
            q.industries = data['industries']
            q.coverage_levels = data['coverage_levels']
            q.languages = data['languages']
            q.sources_full = data['sources_full']
            q.save()
            sources = q.sources_sercher()
            if data['save_mode']:
                if q.name:
                    msg = 'Запрос сохранен под именем <strong>"{}"</strong>'.format(q.name)
                    return render(request, 'catalog/search_sources_result.html',
                                  {'object_list': sources, 'message': msg})
                else:
                    msg = 'Для сохранения запроса введите имя запроса'
                    return render(request, 'catalog/search_sources_result.html', {'message': msg})

            q.delete()
            msg = 'Результаты вашего запроса. Для сохранения поставьте галочку "Сохранить" и введите "Название запроса"'
            return render(request, 'catalog/search_sources_result.html', {'object_list': sources, 'message': msg})
        else:
            print('tr')
            data = form.cleaned_data
            print(data)
    form = SearchSourcesForm()
    return render(request, 'catalog/search_sources.html', {'form': form})


class SourceQueryListView(ListView):
    model = SourceQuery

    def get_queryset(self):
        return SourceQuery.objects.filter(owner=self.request.user)


def sourcequery_delete(request, pk):
    if request.method == 'GET':
        try:
            s = SourceQuery.objects.get(id=pk)
            s.delete()
            return HttpResponse('OK')
        except:
            raise Http404('Ошибка удаления')


# Helpers
# Закрыватель вкладок, не знаю как сделать лучше
def close(request):
    return HttpResponse("<script type='text/javascript'>window.close();</script>")
