from django.db import models
from crowler.models import Source, News
from django.contrib.auth.models import User


# Create your models here.


class SmiType(models.Model):
    name = models.CharField(max_length=150, verbose_name='Наименование')

    class Meta:
        verbose_name = 'Тип СМИ'
        verbose_name_plural = 'Типы СМИ'

    def __str__(self):
        return self.name


class CoverageLevel(models.Model):
    name = models.CharField(max_length=150, verbose_name='Наименование')
    father_level = models.ForeignKey(to='CoverageLevel', verbose_name='Отцовский уровень', blank=True, null=True)
    source_full = models.ManyToManyField('SourceFull', blank=True, verbose_name='Источник')

    class Meta:
        verbose_name = 'Уровень покрытия'
        verbose_name_plural = 'Уровни покрытия'

    def __str__(self):
        return self.name


class Industry(models.Model):
    name = models.CharField(max_length=150, verbose_name='Наименование')
    description = models.TextField(verbose_name='Описание', blank=True)

    class Meta:
        verbose_name = 'Отрасль'
        verbose_name_plural = 'Отрасли'

    def __str__(self):
        return self.name


class Language(models.Model):
    name = models.CharField(max_length=150, verbose_name='Наименование')

    class Meta:
        verbose_name = 'Язык'
        verbose_name_plural = 'Языки'

    def __str__(self):
        return self.name


class GeoGroup(models.Model):
    name = models.CharField(max_length=150, verbose_name='Наименование')
    description = models.TextField(verbose_name='Описание', blank=True)
    locations = models.ManyToManyField('Location', blank=True)

    class Meta:
        verbose_name = 'Гео группа'
        verbose_name_plural = 'Гео группы'

    def __str__(self):
        return self.name


class LocationType(models.Model):
    name = models.CharField(max_length=150, verbose_name='Наименование')

    class Meta:
        verbose_name = 'Тип локации'
        verbose_name_plural = 'Типы локации'

    def __str__(self):
        return self.name


class Location(models.Model):
    father_location = models.ForeignKey(to='Location', verbose_name='Отцовская локация', default=None, blank=True,
                                        null=True)
    location_type = models.ForeignKey(to=LocationType, verbose_name='Тип локации', default=None)
    name = models.CharField(max_length=150, verbose_name='Наименование', default='Наименование локации')
    full_name = models.CharField(max_length=400, verbose_name='Полное наименование', blank=True, default=None,
                                 null=True)
    geo_group = models.ManyToManyField(to=GeoGroup, through=GeoGroup.locations.through, verbose_name='ГЕО группа',
                                       blank=True, default=None)

    class Meta:
        verbose_name = 'Локация'
        verbose_name_plural = 'Локации'

    def __str__(self):
        return self.name


class SourceFull(Source):
    condition = models.BooleanField(verbose_name='Активный/Не Активный', default=False)
    under_control = models.BooleanField(verbose_name='На мониторинге', default=False)
    smi_type = models.ForeignKey(to=SmiType, verbose_name='Тип СМИ')
    coverage_levels = models.ManyToManyField(to=CoverageLevel, through=CoverageLevel.source_full.through,
                                             verbose_name='Уровни покрытия', blank=True)
    industry = models.ManyToManyField(to=Industry, verbose_name='Отрасль', blank=True)
    location = models.ForeignKey(to=Location, verbose_name='Локация', blank=True, default=None, null=True)
    contacts = models.TextField(verbose_name='Контакты', blank=True, default='', null=True)
    lang = models.ForeignKey(to=Language, verbose_name='Язык', blank=True, null=True)

    class Meta:
        verbose_name = 'Источник с полным описанием'
        verbose_name_plural = 'Источники с полным описанием'

    def __str__(self):
        return self.name


class SourceQuery(models.Model):
    owner = models.ForeignKey(User, verbose_name='Владелец', blank=True, null=True, default=None)
    name = models.CharField(verbose_name='Название запроса', max_length=150)
    geo_groups = models.ManyToManyField(GeoGroup, blank=True, verbose_name='Гео группы')
    smi_types = models.ManyToManyField(SmiType, blank=True, verbose_name='Типы сми')
    location_types = models.ManyToManyField(LocationType, blank=True, verbose_name='Типы локаций')
    industries = models.ManyToManyField(Industry, blank=True, verbose_name='Индустрии')
    coverage_levels = models.ManyToManyField(CoverageLevel, blank=True, verbose_name='Уровни покрытия')
    languages = models.ManyToManyField(Language, blank=True, verbose_name='Языки')
    locations = models.ManyToManyField(Location, blank=True, verbose_name='Локации')
    sources_full = models.ManyToManyField(SourceFull, blank=True, verbose_name='Источники')

    class Meta:
        verbose_name = 'Запос по источникам'
        verbose_name_plural = 'Запросы по источникам'

    def sources_sercher(self):
        s = []
        s += SourceFull.objects.filter(smi_type__in=self.smi_types.all())
        s += SourceFull.objects.filter(lang__in=self.languages.all())
        s += SourceFull.objects.filter(location__geo_group__in=self.geo_groups.all())
        s += SourceFull.objects.filter(location__location_type__in=self.location_types.all())
        s += SourceFull.objects.filter(location__in=self.locations.all())
        s += SourceFull.objects.filter(industry__in=self.industries.all())
        s += SourceFull.objects.filter(coverage_levels__in=self.coverage_levels.all())
        s += SourceFull.objects.filter(id__in=self.sources_full.all())
        return s

    def __str__(self):
        return self.name