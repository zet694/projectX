from django.shortcuts import render
from .models import News, Source
from django.utils import timezone
from .xml_handlers import make_xml
from django.http import HttpResponse, Http404
from django.shortcuts import Http404, HttpResponse
from .spiders import selector
from .forms import GetNewsForm, GetXMLForm
from django.db.models import Q
from search.searchers import count_all, count_today, count_yesterday


# Create your views here.


def crowler(request):
    tz = timezone.get_current_timezone()
    d = timezone.timedelta(days=1)
    today = timezone.datetime.now(tz=tz).replace(hour=0, minute=0, second=0, microsecond=0)
    yesterday = today - d
    tomorrow = today + d
    user = request.user
    hour = timezone.timedelta(hours=1)

    # news_not_unloaded = News.objects.all().exclude(unloaded=user).count()
    news_not_unloaded = None
    sources = Source.objects.all().count()
    # news_today = News.objects.all().filter(date_post__range=[today, tomorrow]).count()
    # news_yesterday = News.objects.all().filter(date_post__range=[yesterday, today]).count()
    # news_all = News.objects.all().count()
    # todo переделать выборку последних десяти новостей
    news_last = News.objects.all().order_by('-date_post')[:10]
    # news_last = News.objects.all().filter(date_post__range=[today - hour * 12, tomorrow]).order_by('-date_post')[:10]
    # news_last = News.objects.all()
    # news_last = news_last.reverse()[:10]

    get_news_form = GetNewsForm()
    get_xml_form = GetXMLForm()
    return render(request, 'crowler/dashboard/dashboard_index.html', {
        'news_not_unloaded': news_not_unloaded,
        'sources': sources,
        'news_today': count_today(),
        'news_yesterday': count_yesterday(),
        'news_all': count_all(),
        'news_last': news_last,
        'get_news_form': get_news_form,
        'get_xml_form': get_xml_form,
    })


def news_stats(request):
    sources = Source.objects.all()
    return render(request, 'crowler/news/news_index.html', {
        'sources': sources,
    }
                  )


def hand_scan(request):
    # return HttpResponse('Функция ручного сканирования временно откючена')
    if request.method == 'POST':
        form = GetNewsForm(request.POST)
        if form.is_valid():
            start = form.cleaned_data['date_start']
            end = form.cleaned_data['date_end']
            date_range = end - start
            print('date_start: {} / date_end: {} / range {}'.format(start, end, date_range.days))
            source = form.cleaned_data['source']
            spider = selector(source)
            spider(from_date=start, date_range=date_range.days, source_code=int(source.code))
            return HttpResponse('Задание поставлено в очередь')
    else:
        raise Http404('Wrong Method mr Bot')


def xml_news_not_unloaded(request):
    user = request.user
    news_not_unloaded = News.objects.exclude(unloaded=user)
    xml = make_xml(news_not_unloaded, user)
    response = HttpResponse(xml, content_type='text/xml')
    response['Content-Disposition'] = 'attachment; filename=unloaded.xml'
    return response


def xml_news_today(request):
    tz = timezone.get_current_timezone()
    d = timezone.timedelta(days=1)
    today = timezone.datetime.now(tz=tz)
    tomorrow = timezone.datetime.now(tz=tz) + d
    news_today = News.objects.filter(date_post__range=[today.date(), tomorrow.date()])
    user = request.user

    xml = make_xml(news_today, user)
    response = HttpResponse(xml, content_type='text/xml')
    response['Content-Disposition'] = 'attachment; filename=today.xml'
    return response


def xml_news_yesterday(request):
    tz = timezone.get_current_timezone()
    d = timezone.timedelta(days=1)
    today = timezone.datetime.now(tz=tz).replace(hour=0, minute=0, second=0, microsecond=0)
    yesterday = today - d
    news_yesterday = News.objects.filter(date_post__range=[yesterday, today])
    user = request.user

    xml = make_xml(news_yesterday, user)
    response = HttpResponse(xml, content_type='text/xml')
    response['Content-Disposition'] = 'attachment; filename=yesterday.xml'
    return response


def xml_news_all(request):
    news_all = News.objects.all()
    user = request.user
    xml = make_xml(news_all, user)
    response = HttpResponse(xml, content_type='text/xml')
    response['Content-Disposition'] = 'attachment; filename=all.xml'
    return response


def xml_manual(request):
    if request.method == 'POST':
        form = GetXMLForm(request.POST)
        time = timezone.datetime.strptime('00:00:00 +0300', '%H:%M:%S %z')
        user = request.user

        if form.is_valid():
            date_start = timezone.datetime.combine(form.cleaned_data['date_start'], time.time()).replace(tzinfo=time.tzinfo)
            date_end = timezone.datetime.combine(form.cleaned_data['date_end'], time.time()).replace(tzinfo=time.tzinfo)

            if request.POST.get('only_unloaded'):
                if request.POST.get('start_param'):
                    if request.POST.get('end_param'):
                        news = News.objects.filter(date_post__range=[date_start, date_end]).exclude(
                            unloaded=user)
                    else:
                        news = News.objects.filter(date_post__gte=date_start).exclude(unloaded=user)
                else:
                    if request.POST.get('end_param'):
                        news = News.objects.filter(date_post__lte=date_end).exclude(unloaded=user)
                    else:
                        news = News.objects.exclude(unloaded=user)
            else:
                if request.POST.get('start_param'):
                    if request.POST.get('end_param'):
                        news = News.objects.filter(date_post__range=[date_start, date_end])
                    else:
                        news = News.objects.filter(date_post__gte=date_start)
                else:
                    if request.POST.get('end_param'):
                        news = News.objects.filter(date_post__lte=date_end)
                    else:
                        news = News.objects.all()
            xml = make_xml(news, user)
            response = HttpResponse(xml, content_type='text/xml')
            response['Content-Disposition'] = 'attachment; filename=news_range.xml'
            return response
        else:
            return Http404()
            # return HttpResponse('done')


def clean_up_base(request):
    n = News.objects.all().filter(Q(title=None) | Q(date_post=None))
    t = News.objects.all().filter(title=None)
    d = News.objects.all().filter(date_post=None)
    print(len(t), len(d))
    s = ''
    for i in n:
        s += '<a href="{}">{}</a> <br>'.format(i.url, i.url)
    return HttpResponse(s)
