from crowler.models import News
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from django.contrib.auth.models import User
from django.utils import timezone
from xml.dom import minidom
from smimonitor.celery import app
# from smimonitor.dev_celery import app


@app.task()
def news_unloaded_handler(ids, user_id):
    news = News.objects.filter(id__in=ids)
    user = User.objects.get(id=user_id)
    user.news_set.add(*news)


def make_xml(news_from_base, user):
    root = Element('root')
    time = timezone.datetime.strptime('00:00:00 +0300', '%H:%M:%S %z')
    ids = []
    for i in news_from_base:
        news = SubElement(root, 'news', id=str(i.id))

        url = SubElement(news, 'url')
        url.text = i.url
        source = SubElement(news, 'source')
        source.text = i.source.name
        title = SubElement(news, 'title')
        title.text = i.title
        text = SubElement(news, 'text')
        text.text = i.text
        post_date = SubElement(news, 'post_date')
        try:
            post_date.text = timezone.datetime.strftime(i.date_post + time.utcoffset(), '%Y-%m-%dT%H:%M')
        except TypeError as e:
            print(e)
            print(i.id, i.url)

        ids.append(i.id)

    news_unloaded_handler.delay(ids, user.id)

    s = minidom.parseString(tostring(root, 'utf-8'))
    return s.toprettyxml(indent='  ')


def make_xml_v2_news(news):
    digest = Element('digest', attrib={'from': '', 'to': ''})
    time = timezone.datetime.strptime('00:00:00 +0300', '%H:%M:%S %z')
    ids = []
    messages = SubElement(digest, 'messages')
    for i in news:

        message = SubElement(messages, 'message', attrib={
            'title': i.title,
            'mmCategory': i.source.sourcefull.smi_type.name,
            'mmName': i.source.name})
        context = SubElement(message, 'context')
        context.text = i.text
        publish_dates = SubElement(message, 'publishDates')
        publish_date = SubElement(publish_dates, 'publishDate')

        try:
            publish_date.text = timezone.datetime.strftime(i.date_post + time.utcoffset(), '%Y-%m-%dT%H:%M')
        except TypeError as e:
            print(e)
            print(i.id, i.url)

        ids.append(i.id)

    s = minidom.parseString(tostring(digest, 'utf-8'))
    return s.toprettyxml(indent='  ')


def make_xml_v2_project_news(news):
    digest = Element('digest', attrib={'from': '', 'to': ''})
    time = timezone.datetime.strptime('00:00:00 +0300', '%H:%M:%S %z')
    ids = []
    messages = SubElement(digest, 'messages')
    for i in news:

        message = SubElement(messages, 'message', attrib={
            'title': i.title,
            'mmCategory': i.source.smi_type.name,
            'mmName': i.source.name})
        context = SubElement(message, 'context')
        context.text = i.text
        publish_dates = SubElement(message, 'publishDates')
        publish_date = SubElement(publish_dates, 'publishDate')

        try:
            publish_date.text = timezone.datetime.strftime(i.date_post + time.utcoffset(), '%Y-%m-%dT%H:%M')
        except TypeError as e:
            print(e)
            print(i.id, i.url)

        ids.append(i.id)

    s = minidom.parseString(tostring(digest, 'utf-8'))
    return s.toprettyxml(indent='  ')