from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from collections import Counter


# Create your models here.


class Source(models.Model):
    name = models.CharField(verbose_name='Название источника', max_length=150)
    url = models.URLField(verbose_name='Начальная ссылка')
    code = models.DecimalField(verbose_name='Числовой код', max_digits=4, decimal_places=0, blank=True, unique=True, null=True)

    def last_news(self):
        return self.news_set.latest(field_name='date_post')

    def time_without_news(self):
        return timezone.datetime.now() - self.last_news()

    def chart_data(self):
        # todo сделать с параметрами, что бы отдавала данные по заданным датам
        time = timezone.datetime.strptime('00:00:00 +0300', '%H:%M:%S %z')
        day = timezone.timedelta(days=1)
        now = timezone.datetime.now() - day * 10
        now = timezone.datetime.combine(now, time.time()).replace(tzinfo=time.tzinfo)
        n = list(self.news_set.all().filter(date_post__gte=now.date()).values_list('date_post', flat=True))
        # n = self.news_set.all().filter(date_post__gte=now.date())
        # days = n.dates('date_post', 'day')
        # charts = []
        n = tuple(map(lambda x: timezone.datetime.combine(x, time.time()).replace(tzinfo=time.tzinfo).strftime('%Y-%m-%d'), n))
        c = Counter()
        for i in n:
            c[i] += 1
        # for i in days:
        #     d = timezone.datetime.combine(i, time.time()).replace(tzinfo=time.tzinfo)
        #     c = n.filter(date_post__range=[d, d + day]).values_list('id', flat=True)
        #     charts.append({'day': d, 'count': len(c)})
        return dict(c)

    class Meta:
        verbose_name = 'Источник'
        verbose_name_plural = 'Источники'

    def __str__(self):
        return self.name


class News(models.Model):
    url = models.URLField(unique=True)
    title = models.TextField(blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    date_post = models.DateTimeField(db_index=True, blank=True, null=True)
    date_add = models.DateTimeField(auto_now_add=True)
    source = models.ForeignKey(Source, verbose_name='Источник', blank=True, null=True, default=None)
    unloaded = models.ManyToManyField(User, editable=False, blank=True, default=None)

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    def __str__(self):
        s = 'Нет текста новости'
        try:
            s = self.title[:20] + '...'
        except TypeError as e:
            print(e)
        return s

