from django import forms
from .models import Source


class GetNewsForm(forms.Form):
    date_start = forms.DateField(label='Начало периода', widget=forms.SelectDateWidget(years=('2015', '2016', '2017')))
    date_end = forms.DateField(label='Конец периода', widget=forms.SelectDateWidget(years=('2015', '2016', '2017')))
    source = forms.ModelChoiceField(Source.objects.all())


class GetXMLForm(forms.Form):
    only_unloaded = forms.BooleanField(required=False)
    start_param = forms.BooleanField(required=False)
    end_param = forms.BooleanField(required=False)
    date_start = forms.DateField(label='Начало периода', widget=forms.SelectDateWidget(years=('2015', '2016', '2017')))
    date_end = forms.DateField(label='Конец периода', widget=forms.SelectDateWidget(years=('2015', '2016', '2017')))
