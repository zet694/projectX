from django import template
from projects.models import ProjectNews

register = template.Library()


@register.simple_tag
def get_analytics(value):
    p = ProjectNews.objects.get(id=value)
    s = ''
    tpl = '''
    <div id="a_n_{id}">
        ({abr}){name}(
        <val id="val_{id}_pn_{pn_id}">{val}</val>
        )
        <a href="#" onclick="del_object('{url}', '#a_n_{id}')" title="удалить аналитику">
            x
        </a>
        <a href="#" onclick="edit_analytics_num_param('{id}', '#val_{id}_pn_{pn_id}')" title="изменить аналитику">
            <i class="fa fa-pencil-square" aria-hidden="true" title='изменить занчение аналитики'></i>
        </a>
    </div>
    '''
    del_path = '/projects/analytics/num/param/del_one/'
    an = p.get_analytics_num()
    type_abr = 'N'
    try:
        for a in an:
            url = del_path + str(a['id'])
            s += tpl.format(name=a['analyticnum__name'], val=a['num'], id=a['id'], url=url, pn_id=value, abr=type_abr)
    except IndexError as e:
        pass

    tpl = '''
        <div id="a_b_{id}_{pn_id}">
            ({abr}){name}(
            <val id="val_b_{id}_pn_{pn_id}">{val}</val>
            )
            <a href="#" onclick="del_object('{url}', '#a_b_{id}_{pn_id}')" title="удалить аналитику">
                x
            </a>
            <a href="#" onclick="change_param_bool_{remove}({pn_id}, '{id}', '#val_b_{id}_pn_{pn_id}')" title="изменить аналитику на противоположную">
                <i class="fa fa-pencil-square" aria-hidden="true" title='изменить занчение аналитики на противоположную'></i>
            </a>
        </div>
        '''

    del_path = '/projects/analytics/bool/param/del_one/{news_id}/{analytic_id}'
    an = p.get_analytics_bool()
    type_abr = 'B'

    for a in an:
        try:
            url = del_path.format(news_id=value, analytic_id=a['id'])
            p_name = '!Ошибка имени!'
            remove = ''
            if a.get('name_positive'):
                p_name = a.get('name_positive')
                remove = 'false'
            elif a.get('name_negative'):
                p_name = a.get('name_negative')
                remove = 'true'
            s += tpl.format(name=a['name'], val=p_name, id=a['id'], url=url, pn_id=value, abr=type_abr, remove=remove)
        except IndexError as e:
            pass


    tpl = '''
           <div id="a_l_{id}_{pn_id}">
               ({abr}){a_name}(
               <val id="val_l_{id}_pn_{pn_id}">{p_name}</val>
               )
               <a href="#" onclick="del_object('{url}', '#a_l_{id}_{pn_id}')" title="удалить аналитику">
                   x
               </a>

           </div>
           '''

#     < a
#     href = "#"
#     onclick = "change_param_list({pn_id}, '{id}', '#val_l_{id}_pn_{pn_id}')"
#     title = "изменить аналитику" >
#     < i
#
#     class ="fa fa-pencil-square" aria-hidden="true" title='изменить аналитику' > < / i >
#
# < / a >

    del_path = '/projects/analytics/list/param/del_one/{news_id}/{analytic_id}'
    an = p.get_analytics_list()
    type_abr = 'L'

    for a in an:
        try:
            url = del_path.format(news_id=value, analytic_id=a['p_id'])
            s += tpl.format(a_name=a['a_name'], p_name=a['p_name'], id=a['p_id'], url=url, pn_id=value, abr=type_abr)
        except IndexError as e:
            pass
    return s

