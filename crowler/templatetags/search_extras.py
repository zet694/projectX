from django import template

register = template.Library()


@register.simple_tag
def replace_highlight(value, highlights):
    if highlights:
        for i in highlights:
            value = value.replace(i, '<strong>{}</strong>'.format(i))
    return value


# @register.simple_tag(takes_context=True)
@register.filter
def get_dict_value(value, key):
    return value.get(key)