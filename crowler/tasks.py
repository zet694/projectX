# import os
from django.utils import timezone
from .spiders import comers_spider, eho_spider, radiovesti_spider, ria_spider, govoritmoskva_spider
from .sync import update_index
from smimonitor.celery import app
# from smimonitor.dev_celery import app


@app.task(soft_time_limit=3600)
def comers():
    d = timezone.timedelta(days=1)
    start = timezone.datetime.now() - d
    start = start.strftime('%Y-%m-%dT%H:%M:%S')
    comers_spider(start, 1, 2)


@app.task(soft_time_limit=3600)
def eho():
    d = timezone.timedelta(days=1)
    start = timezone.datetime.now() - d
    start = start.strftime('%Y-%m-%dT%H:%M:%S')
    eho_spider(start, 1, 3)


@app.task(soft_time_limit=3600)
def radiovesti():
    d = timezone.timedelta(days=1)
    start = timezone.datetime.now() - d
    start = start.strftime('%Y-%m-%dT%H:%M:%S')
    radiovesti_spider(start, 1, 4)


@app.task(soft_time_limit=3600)
def ria():
    d = timezone.timedelta(days=1)
    start = timezone.datetime.now() - d
    start = start.strftime('%Y-%m-%dT%H:%M:%S')
    ria_spider(start, 1, 0)


@app.task(soft_time_limit=3600)
def govoritmoskva():
    d = timezone.timedelta(days=1)
    start = timezone.datetime.now() - d
    start = start.strftime('%Y-%m-%dT%H:%M:%S')
    govoritmoskva_spider(from_date=start, date_range=1, source_code=6)