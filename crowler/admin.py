from django.contrib import admin
from .models import News, Source

# Register your models here.
admin.site.register(News)
admin.site.register(Source)


class MyAdminSite(admin.AdminSite):
    site_header = 'Админка Сборщика новостей'


admin_crowler = MyAdminSite(name='admin_crowler')


class NewsAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'url',
        'date_post',
        'date_add',
        'source',
    )
    search_fields = (
        'id',
        'url',
        'title',
        'text',
    )
    list_editable = ()


admin_crowler.register(News, NewsAdmin)


class SourceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'url',
        'code',
    )
    search_fields = (
        'name',
        'url',
        'code',
    )
    list_editable = (
        'name',
        'url',
        'code',
    )


admin_crowler.register(Source, SourceAdmin)




