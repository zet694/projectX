/**
 * Created by gad on 23.08.17.
 */
function cols_prefenses_render(project_id) {

    var prefs;

    if (!localStorage.getItem('prefs_' + project_id)) {
        prefs = {
            'id': {
                'val': '',
                'name': 'id',
                'column_id': 1
            },
            'date_post': {

                'val': 'checked',
                'name': 'Дата публикации',
                'column_id': 2
            },
            'time_post': {
                'val': 'checked',
                'name': 'Время публикации',
                'column_id': 3
            },
            'theme': {
                'val': 'checked',
                'name': 'Тема',
                'column_id': 4
            },
            'rubric': {
                'val': 'checked',
                'name': 'Рубрика',
                'column_id': 5
            },
            'title': {
                'val': 'checked',
                'name': 'Заголовок',
                'column_id': 6
            },
            'analytics_num': {
                'val': '',
                'name': 'Аналитики числовые',
                'column_id': 7
            },
            'analytics_bool': {
                'val': '',
                'name': 'Аналитики булевы',
                'column_id': 8
            },
            'analytics_list': {
                'val': '',
                'name': 'Аналитики списочные',
                'column_id': 9
            },
            'source': {
                'val': 'checked',
                'name': 'Источник',
                'column_id': 10
            },
            'source_town': {
                'val': 'checked',
                'name': 'Источник город',
                'column_id': 11
            },
            'source_type': {
                'val': '',
                'name': 'Источник тип',
                'column_id': 12
            },
            'source_authority': {
                'val': '',
                'name': 'Источник влиятельность',
                'column_id': 13
            },
            'author': {
                'val': '',
                'name': 'Автор',
                'column_id': 14
            },
            'coverage': {
                'val': '',
                'name': 'Охват',
                'column_id': 15
            },
            'tonality': {
                'val': 'checked',
                'name': 'Тональность',
                'column_id': 16
            },
            'status': {
                'val': 'checked',
                'name': 'Статус',
                'column_id': 17
            },
            'comment': {
                'val': 'checked',
                'name': 'Примечание',
                'column_id': 18
            },
            'datetime_journal': {
                'val': '',
                'name': 'Дата, время загрузки в журнал',
                'column_id': 19
            },
            'datetime_base': {
                'val': '',
                'name': 'Дата, время загрузки в базу',
                'column_id': 20
            }
        };

        localStorage.setItem('prefs_' + project_id, JSON.stringify(prefs));

    } else {
        prefs = JSON.parse(localStorage.getItem('prefs_' + project_id))
    }

    function pref_render(pref, p_id) {
        var template = '<div class="col-md-4"><input id="' + p_id + '" ' + pref.val + ' type="checkbox" title="' + pref.name + '">' + pref.name + '&#9;</div>';
        $('#pref_columns_' + project_id).append(template);
    }

    var p;
    for (p in prefs) {
        pref_render(prefs[p], p);
    }
}

function save_cols_prefs(project_id) {
    var cur_prefs = $('#pref_columns_' + project_id + ' :input');
    var prefs = JSON.parse(localStorage.getItem('prefs_' + project_id));
    var p;
    for (p = 0; p < cur_prefs.length; p++) {
        if (cur_prefs[p].checked) {
            prefs[cur_prefs[p].id].val = 'checked'
        } else {
            prefs[cur_prefs[p].id].val = ''
        }
    }
    localStorage.setItem('prefs_' + project_id, JSON.stringify(prefs));
}

function drop_prefs(project_id) {
    localStorage.removeItem('prefs_' + project_id);
    $('#pref_columns_' + project_id).html('');
    cols_prefenses_render(project_id)
}

<!-- Выделение в с таблицах -->
function select_all_t(table) {
    var table = window[table];
    var news_ids = table.rows('.selected').ids().toArray();
    console.log(table);
    console.log(news_ids);
}

function remove_selection_all_t(table) {
    var table = window[table];
    table.rows.toggleClass('selected')

}

function delete_selected_t(table) {
    var table = window[table];
    var rows = table.rows('.selected');
    var news_ids = rows.ids().toArray();
    var d = {};
    d.news = news_ids;
    d.csrfmiddlewaretoken = readCookie('csrftoken');
    return $.ajax({
        dataType: "json",
        url: "/api/news/delete/",
        type: "delete",
        data: d,
        error: function (data) {
            alert('Ошибка удаления новости \n Ошибка: ' + data.responseJSON.detail)
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRFToken', d.csrfmiddlewaretoken)
        },
        success: function (data) {
            alert('Новости удалены');
            console.log(data);
            rows.remove().draw();
        }
    })

}

function load_modal_news_creation() {
    $.ajax({
            dataType: "html",
            url: "/projects/news/create/",
            type: "get",
            // data: d,
            error: function (data) {
                alert('Ошибка получения формы \n Ошибка: ' + data)
            },
            success: function (data) {
                var modal = $('#modal');
                var modal_body = $('#modal_body');
                modal_body.html(data);
                modal.modal('show');
            }
        }
    )
}

function load_modal_news_edit(n_id) {
    event.preventDefault();
    $.ajax({
            dataType: "html",
            url: "/projects/news/"+n_id+"/edit/",
            type: "get",
            // data: d,
            error: function (data) {
                alert('Ошибка получения формы \n Ошибка: ' + data)
            },
            success: function (data) {
                var modal = $('#modal');
                var modal_body = $('#modal_body');
                modal_body.html(data);
                modal.modal('show');
            }
        }
    )
}