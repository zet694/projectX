/**
 * Created by gad on 15.08.17.
 */
function save_annotation(annotation_tag, news_id) {
    function handler() {
        var varData = {};
        varData['annotation'] = $(annotation_tag).val();
        varData['n_id'] = news_id;
        return $.ajax({
            dataType: "json",
            url: '/projects/news/annotation/save',
            type: "post",
            data: varData
        });
    }

    handler().done(function (data) {
        if (data.status == 'OK') {
            alert('Аннотация сохранена');
        } else {
            alert('Возникла проблема при выполнении запроса')
        }
    });
}

function save_text(text_tag, news_id) {
    function handler() {
        var varData = {};
        varData['text'] = $(text_tag).val();
        varData['n_id'] = news_id;
        return $.ajax({
            dataType: "json",
            url: '/projects/news/text/save',
            type: "post",
            data: varData
        });
    }

    handler().done(function (data) {
        if (data.status == 'OK') {
            alert('Текст сохранен');
        } else {
            alert('Возникла проблема при выполнении запроса')
        }

    });
}

function num_param_render(param) {
    var p = param,
        deletion = '<a href="#" onclick="del_object(\'/projects/news/param/num/del/one/' + p.id + '\', \'#param_num_' + p.id + '\')" title="удалить аналитику">X</a>',
        template = '<tr id="param_num_' + p.id + '"><td>' + p.name + ' (' + p.value + ') ' + deletion + '</td></tr>';
    $('#table_num').append(template);

}

function bool_param_render(param) {
    var p = param,
        deletion = '<a href="#" onclick="del_object(\'/projects/news/param/list/del/one/' + p.news_id + '/' + p.id + '\', \'#param_bool_' + p.id + '\')" title="удалить аналитику">X</a>',
        template = '<tr id="param_bool_' + p.id + '"><td>' + p.name + ' (' + p.value + ') ' + deletion + '</td></tr>';
    $('#table_bool').append(template);
}

function list_param_render(param) {
    var p = param,
        deletion = '<a href="#" onclick="del_object(\'/projects/news/param/list/del/one/' + p.news_id + '/' + p.id + '\', \'#param_list_' + p.id + '\')" title="удалить аналитику">X</a>',
        template = '<tr id="param_list_' + p.id + '"><td>' + p.name + ' (' + p.value + ') ' + deletion + '</td></tr>';
    $('#table_list').append(template);
}
// function del_num_param_one(param_id) {
//     function n_p_handler(a_id, a_param) {
//
//         return $.ajax({
//             dataType: "json",
//             url: '/projects/news/param/num/dell/one',
//             type: "get",
//         });
//     }
//
//     if (na_id == null) {
//         alert('Нужно указать аналитику');
//     }
//     else {
//         n_p_handler(na_id, p).done(
//             function (data) {
//                 if (data.status == 'OK') {
//                     console.log(data.param);
//                     num_param_render(data.param);
//                     alert('Аналитики добавлены\n message: ' + data.msg);
//                 } else {
//                     alert('Возникла ошибка при добавлении числовых аналитик \n message: ' + data.msg)
//                 }
//             }
//         )
//     }
//     this.event.preventDefault();
// }

function add_num_param_one(analytyc, param, news_id) {
    var na_id = $(analytyc).val(),
        p = $(param).val();

    function n_p_handler(a_id, a_param, n_id) {
        var varData = {};
        varData['a_id'] = a_id;
        varData['n_id'] = n_id;
        varData['param'] = a_param;
        return $.ajax({
            dataType: "json",
            url: '/projects/news/param/num/add/one',
            type: "post",
            data: varData
        });
    }

    if (na_id == null) {
        alert('Нужно указать аналитику');
    }
    else {
        n_p_handler(na_id, p, news_id).done(
            function (data) {
                if (data.status == 'OK') {
                    num_param_render(data.param);
                    alert('Аналитики добавлены\n message: ' + data.msg);
                } else {
                    alert('Возникла ошибка при добавлении числовых аналитик \n message: ' + data.msg)
                }
            }
        )
    }
    this.event.preventDefault();
}

function add_bool_param_one(param_tag, analytic_tag, news_id) {
    var n_id = news_id,
        b_id = $(analytic_tag).val(),
        param = $(param_tag).val();
    console.log("#param_bool_" + b_id);
    $("#param_bool_" + b_id).hide();
    function b_p_handler(b_id, n_id, cur_param) {
        var varData = {};
        varData['n_id'] = n_id;
        varData['b_id'] = b_id;
        // varData['param'] = cur_param;
        if (cur_param === '0') {
            return $.ajax({
                dataType: "json",
                url: '/projects/news/param/bool/add/false',
                type: "post",
                data: varData
            });
        } else if (cur_param === '1') {
            return $.ajax({
                dataType: "json",
                url: '/projects/news/param/bool/add/true',
                type: "post",
                data: varData
            });
        } else {
            alert('Неверный параметр аналитики');
        }
    }

    if (b_id == null) {
        alert('Нужно указать аналитику');
    }
    else {
        b_p_handler(b_id, n_id, param).done(
            function (data) {
                if (data.status == 'OK') {
                    bool_param_render(data.param);
                    alert('Аналитики добавлены\n message: ' + data.msg);
                } else {
                    alert('Возникла ошибка при добавлении булевых аналитик \n message: ' + data.msg)
                }
            }
        )
    }
    this.event.preventDefault();
}


function add_list_param_one(analytic, param, news_id) {
    var la_id = $(analytic).val(),
        p = $(param).val();

    function n_p_handler(l_id, l_param, n_id) {
        var varData = {};
        varData['n_id'] = n_id;
        varData['l_id'] = l_id;
        varData['param'] = l_param;
        return $.ajax({
            dataType: "json",
            url: '/projects/news/param/list/add/one',
            type: "post",
            data: varData
        });
    }

    if (la_id == null) {
        alert('Нужно указать аналитику');
    }
    else {
        n_p_handler(la_id, p, news_id).done(
            function (data) {
                if (data.status == 'OK') {
                    list_param_render(data.param);
                    alert('Аналитики добавлены\n message: ' + data.msg);
                } else {
                    alert('Возникла ошибка при добавлении списочных аналитик \n message: ' + data.msg)
                }
            }
        )
    }
    this.event.preventDefault();

}