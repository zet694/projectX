/**
 * Created by gad on 09.07.17.
 */

<!-- Работа с прикреплением новостей к ПТР ++++++++++++++++++++++++++++++++ -->

<!-- Загрузка Проектов -->
function load_projects(p_id) {
    function get_proj_list() {
        return $.ajax({
            url: "/projects/projects_ids/",
            type: "get"
        })
    }

    get_proj_list().done(function (proj_list) {
        if (proj_list !== 'FAIL') {
            var list = $('#project_s_' + p_id),
                items = "<option value=>---</option>";
            $.each(proj_list, function (i, o) {
                items += "<option  value=" + o.id + ">" + o.name + "</option>"
            });
            list.append(items)
        }
    })
}

<!-- Загрузка тем -->

function load_themes(pr_id) {
    function get_them_list(proj_id) {
        return $.ajax({
            url: "/projects/" + proj_id + "/themes_ids/",
            type: "get"
        })
    }

    var p_id = $('#project_s_' + pr_id).val();
    if (p_id !== '---' && p_id) {
        $("#themes_l_" + pr_id).show();
        get_them_list(p_id).done(function (theme_list) {

            if (theme_list !== 'FAIL') {
                var list = $("#theme_s_" + pr_id),
                    items = "<option>---</option>";
                $.each(theme_list, function (i, o) {
                    items += "<option value=" + o.id + ">" + o.name + "</option>"
                });
                list.html(items)
            }
        })
    } else {
        $("#themes_l_" + pr_id).hide();
        $("#theme_s_" + pr_id).val('---').change();

        $("#rubrics_l_" + pr_id).hide();
        $("#rubric_s_" + pr_id).val('---').change();
    }

}

<!-- Загрузка рубрик -->

function load_rubrics(pr_id) {

    function get_rubric_list(theme_id) {
        return $.ajax({
            url: "/projects/themes/" + theme_id + "/rubrics_ids/",
            type: "get"
        })
    }

    var r_id = $('#theme_s_' + pr_id).val();
    if (r_id !== '---') {
        $("#rubrics_l_" + pr_id).show();
        get_rubric_list(r_id).done(function (rubric_list) {
            if (rubric_list !== 'FAIL') {
                var list = $('#rubric_s_' + pr_id),
                    items = "<option>---</option>";
                $.each(rubric_list, function (i, o) {
                    items += "<option value=" + o.id + ">" + o.name + "</option>"
                });
                list.html(items)
            }
        })
    } else {
        $("#rubrics_l_" + pr_id).hide();
        $("#rubric_s_" + pr_id).val('---').change();
    }

}

<!-- Прикрепление новостей -->
function add_news(p_id, container) {
    var news_ids = $(container + ' input:checked').map(function () {
            return this.value;
        }).get(),
        project_id = $('#project_s_' + p_id).val(),
        theme_id = $('#theme_s_' + p_id).val(),
        rubric_id = $('#rubric_s_' + p_id).val();
    if (project_id == null) {
        alert('Нужно указать хотябы проект')
    }
    else {
        if (news_ids.length == 0) {
            alert('Нужно отметить хотябы одну новость')
        }
        else {
            var a;
            if (rubric_id !== null && rubric_id !== "---") {
                a = add_to_rubric(rubric_id, news_ids)
            } else {

                if (theme_id !== null && theme_id !== "---") {
                    a = add_to_theme(theme_id, news_ids)
                } else {
                    a = add_to_project(project_id, news_ids)
                }
            }
        }
    }
    a.done(function (data) {
        if (data == 'OK') {
            alert('Новости добавлены')
        } else {
            alert('Возникла ошибка при добавлении новостей в проект/тему/рубрику')
        }
    });
}

<!-- Прикрепление новостей  к проекту -->
function add_to_project(p_id, n_ids) {
    var varData = {};
    varData['n_ids'] = n_ids;
    return $.ajax({
        url: '/projects/' + p_id + '/add_news',
        type: "post",
        data: varData
    });
}

<!-- Прикрепление новостей к теме -->
function add_to_theme(t_id, n_ids) {
    var varData = {};
    varData['n_ids'] = n_ids;
    return $.ajax({
        url: '/projects/themes/' + t_id + '/add_news',
        type: "post",
        data: varData
    });
}

<!-- Прикрепление новостей к рубрике -->
function add_to_rubric(r_id, n_ids) {
    var varData = {};
    varData['n_ids'] = n_ids;
    return $.ajax({
        url: '/projects/rubrics/' + r_id + '/add_news',
        type: "post",
        data: varData
    });
}

<!-- Работа с прикреплением новостей к ПТР -------------------------------- -->

<!-- Загрузка списка числовых аналитик -->
function load_num_analytics(p_id, tag) {
    function get_num_analytics_list() {
        return $.ajax({
            url: "/projects/" + p_id + "/analytics/num/ids/",
            type: "get"
        })
    }

    get_num_analytics_list().done(function (proj_list) {
        if (proj_list !== 'FAIL') {
            var list = $(tag),
                items = "<option value=>---</option>";
            $.each(proj_list, function (i, o) {
                items += "<option  value=" + o.id + ">" + o.name + "</option>"
            });
            list.append(items)
        }
    })
}

<!-- Загрузка списка булевых аналитик -->
function load_bool_analytics(p_id, tag) {
    function get_bool_analytics_list() {
        return $.ajax({
            url: "/projects/analytics/bool/" + p_id + "/ids/",
            type: "get"
        })
    }

    get_bool_analytics_list().done(function (proj_list) {
        if (proj_list !== 'FAIL') {
            var list = $(tag),
                items = "<option value=>---</option>";
            $.each(proj_list, function (i, o) {
                items += "<option  value=" + o.id + ">" + o.name + "</option>"
            });
            list.append(items)
        }
    })
}

<!-- Загрузка параметров булевых аналитик -->
function load_bool_params(b_tag, tag) {
    function get_bool_analytics_list(bid) {
        return $.ajax({
            url: "/projects/analytics/bool/params/" + bid + "/ids/",
            type: "get"
        })
    }

    var b_id = $(b_tag).val();
    get_bool_analytics_list(b_id).done(function (proj_list) {
        if (proj_list !== 'FAIL') {
            var list = $(tag),
                items = "<option value=>---</option>";
            console.log(proj_list);
            items += "<option  value=" + proj_list[0].id + ">" + proj_list[0].name + "</option>"
            items += "<option  value=" + proj_list[1].id + ">" + proj_list[1].name + "</option>"
            list.html(items)
        }
    })
}

<!-- Загрузка списка списочных аналитик -->
function load_list_analytics(p_id, tag) {
    function get_list_analytics_list() {
        return $.ajax({
            url: "/projects/analytics/list/" + p_id + "/ids/",
            type: "get"
        })
    }

    get_list_analytics_list().done(function (proj_list) {
        if (proj_list !== 'FAIL') {
            var list = $(tag),
                items = "<option value=>---</option>";
            $.each(proj_list, function (i, o) {
                items += "<option  value=" + o.id + ">" + o.name + "</option>"
            });
            list.html(items)
        }
    })
}

<!-- Загрузка списка параметров списочных аналитик -->
function load_list_params(l_tag, tag) {
    function get_bool_analytics_list(lid) {
        return $.ajax({
            url: "/projects/analytics/list/params/" + lid + "/ids/",
            type: "get"
        })
    }

    var b_id = $(l_tag).val();
    get_bool_analytics_list(b_id).done(function (proj_list) {
        if (proj_list !== 'FAIL') {
            var list = $(tag),
                items = "<option value=>---</option>";
            $.each(proj_list, function (i, o) {
                items += "<option  value=" + o.id + ">" + o.name + "</option>"
            });
            list.html(items)
        }
    })
}
<!-- Выделение в с таблицах -->
function select_all(table) {
    table.$('input[type=checkbox]').prop('checked', true)
}

function remove_selection_all(table) {
    table.$('input[type=checkbox]').prop('checked', false)

}

<!-- Загрузка журнала проекта -->
function getJournal(p_id, journal_tag) {
    function get_j(pr_id) {

        return $.ajax({
            url: "/projects/" + pr_id + "/journal/",
            type: "get"
        });
    }

    $(journal_tag).html('Загрузка...');
    get_j(p_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {

            $(journal_tag).html(data);

        }
    });
}

<!-- Загрузка объектов ++++++++++++++++++++++++++++++++ -->

<!-- Загрузка всех проектов -->
function getProjects(tag) {
    function get_pr() {
        return $.ajax({
            url: '/projects/get/',
            type: "get",
        });
    }

    get_pr().done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).html(data);
        }
    });
}

<!-- Загрузка тем в проекте -->
function getThemes(p_id, tag) {
    function get_t(pr_id) {
        return $.ajax({
            url: "/projects/" + pr_id + "/themes/get/",
            type: "get"
        });
    }

    get_t(p_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).html(data);
        }
    });
}

<!-- Загрузка рубрик в темах -->
function getRubrics(th_id, tag) {
    function get_r(t_id) {
        return $.ajax({
            url: "/projects/themes/" + t_id + "/rubrics/get/",
            type: "get",
        });
    }

    get_r(th_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).html(data);
        }
    });
}

<!-- Загрузка числовых аналитик -->

function getNumAnalytics(p_id, tag) {
    function get_n_a(pr_id) {
        return $.ajax({
            url: "/projects/" + pr_id + "/analytics/num/get/",
            type: "get",
        });
    }

    get_n_a(p_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).html(data);
        }
    });
}

<!-- Загрузка Булевых аналитик -->

function getBoolAnalytics(p_id, tag) {
    function get_n_a(pr_id) {
        return $.ajax({
            url: "/projects/" + pr_id + "/analytics/bool/get/",
            type: "get",
        });
    }

    get_n_a(p_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).html(data);
        }
    });
}

<!-- Загрузка Списочных аналитик -->

function getListAnalytics(p_id, tag) {
    function get_n_a(pr_id) {
        return $.ajax({
            url: "/projects/" + pr_id + "/analytics/list/get/",
            type: "get"
        });
    }

    get_n_a(p_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).html(data);
        }
    });
}
<!-- Загрузка объектов -------------------------------- -->

<!-- Создание объектов ++++++++++++++++++++++++++++++++ -->
<!-- Создание проектов -->
function addProject(tag) {
    function create_p() {
        var name = prompt("Пожалуйста введите название проекта: ", "Название");
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            return $.ajax({
                url: "/projects/add/",
                type: "post",
                data: varData
            });
        }
    }

    create_p().done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).append(data);
        }
    });
    this.event.preventDefault();
}

<!-- Создание Числовой аналитики -->
function addNumAnalytic(tag, p_id) {
    function create_na() {
        var name = prompt("Пожалуйста введите название аналитики: ", "Название");
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['p_id'] = p_id;
            return $.ajax({
                url: "/projects/analytics/num/add/",
                type: "post",
                data: varData
            });
        }
    }

    create_na().done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).append(data);
        }
    });
    this.event.preventDefault();
}

<!-- Создание Булевой аналитики -->
function addBoolAnalytic(tag, p_id) {
    function create_ba() {
        var name = prompt("Пожалуйста введите название аналитики: ", "Название");
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['p_id'] = p_id;
            return $.ajax({
                url: "/projects/analytics/bool/add/",
                type: "post",
                data: varData
            });
        }
    }

    create_ba().done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).append(data);
        }
    });
    this.event.preventDefault();
}

<!-- Создание Булевой аналитики -->
function addListAnalytic(tag, p_id) {
    function create_la() {
        var name = prompt("Пожалуйста введите название аналитики: ", "Название");
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['p_id'] = p_id;
            return $.ajax({
                url: "/projects/analytics/list/add/",
                type: "post",
                data: varData
            });
        }
    }

    create_la().done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(tag).append(data);
        }
    });
    this.event.preventDefault();
}

<!-- Создание параметра списка -->
function addListParam(a_tag, al_id) {
    function create_la() {
        var name = prompt("Пожалуйста введите название параметра: ", "Название");
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['al_id'] = al_id;
            return $.ajax({
                url: "/projects/analytics/list/param/add/",
                type: "post",
                data: varData
            });
        }
    }

    create_la().done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            $(a_tag).append(data);
        }
    });
    this.event.preventDefault();
}


<!-- Добавление булевых параметров к новостям -->
function add_bool_param(a_tag, param_tag, container) {
    var news_ids = $(container + ' input:checked').map(function () {
            return this.value;
        }).get(),
        b_id = $(a_tag).val(),
        param = $(param_tag).val();

    function n_p_handler(b_id, ids, cur_param) {
        var varData = {};
        varData['n_ids'] = ids;
        varData['b_id'] = b_id;
        // varData['param'] = cur_param;
        if (cur_param === '0') {
            return $.ajax({
                url: '/projects/analytics/bool/param/add/false',
                type: "post",
                data: varData
            });
        } else if (cur_param === '1') {
            return $.ajax({
                url: '/projects/analytics/bool/param/add/true',
                type: "post",
                data: varData
            });
        } else {
            alert('Неверный параметр аналитики');
        }
    }

    if (b_id == null) {
        alert('Нужно указать аналитику');
    }
    else {
        n_p_handler(b_id, news_ids, param).done(
            function (data) {
                if (data == 'OK') {
                    alert('Аналитики добавлены');
                } else {
                    alert('Возникла ошибка при добавлении булевых аналитик')
                }
            }
        )
    }
    this.event.preventDefault();
}

<!-- Добавление параметров числовых аналитик к новости -->
function add_num_param(p_id, param, container) {
    var news_ids = $(container + ' input:checked').map(function () {
            return this.value;
        }).get(),
        na_id = $('#num_analytic_s_' + p_id).val(),

        param = $(param).val();

    function n_p_handler(a_id, ids) {
        var varData = {};
        varData['n_ids'] = ids;
        varData['param'] = param;
        return $.ajax({
            url: '/projects/analytics/num/' + a_id + '/add_param/',
            type: "post",
            data: varData
        });
    }

    if (na_id == null) {
        alert('Нужно указать аналитику');
    }
    else {
        n_p_handler(na_id, news_ids).done(
            function (data) {
                if (data == 'OK') {
                    alert('Аналитики добавлены');
                } else {
                    alert('Возникла ошибка при добавлении числовых аналитик')
                }
            }
        )
    }
    this.event.preventDefault();
}

<!-- Добавление списочных параметров к новостям -->
function add_list_param(a_tag, param_tag, pr_id) {
    var news_ids = window['jt_' + pr_id].rows('.selected').ids(),
        param = $(param_tag).val();
    console.log(news_ids);
    function n_p_handler(ids, cur_param) {
        var varData = {};
        varData['n_ids'] = ids;
        // varData['b_id'] = b_id;
        varData['param'] = cur_param;
        return $.ajax({
            url: '/projects/analytics/list/param/news/add',
            type: "post",
            data: varData
        });
    }

    if (param == null) {
        alert('Нужно указать параметр');
    }
    else {
        n_p_handler(news_ids, param).done(
            function (data) {
                if (data == 'OK') {
                    alert('Аналитики добавлены');
                } else {
                    alert('Возникла ошибка при добавлении булевых аналитик')
                }
            }
        )
    }
    this.event.preventDefault();
}


<!-- Создание объектов -------------------------------- -->

<!-- Изменение объектов ++++++++++++++++++++++++++++++++ -->
<!-- Переименование проектов -->
function renameProject(id, tag_list) {
    var p_name = prompt("Пожалуйста введите новое название проекта: ");

    function rename(name, p_id) {
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['p_id'] = p_id;
            return $.ajax({
                url: "/projects/rename/",
                type: "post",
                data: varData
            });
        }
    }

    rename(p_name, id).done(function (data) {
        // Updates the UI based the ajax result
        if (data !== 'FAIL') {
            var i = 0;
            for (; i < tag_list.length; i++) {
                $(tag_list[i]).text(p_name);
            }
        }
    });
}

<!-- Переименование числовых аналитик -->
function renameNumAnalytic(an_id, tag) {
    var an_name = prompt("Пожалуйста введите новое название аналитики: ");

    function rename(name, a_id) {
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['a_id'] = a_id;
            return $.ajax({
                url: "/projects/num_analytics/rename/",
                type: "post",
                data: varData
            });
        }
    }

    rename(an_name, an_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data && data !== 'FAIL') {
            $(tag).text(an_name);
        }
    });
    this.event.preventDefault();
}

<!-- Изменение параметра числовой аналитики -->
function edit_analytics_num_param(p_id, tag) {
    var p = prompt("Пожалуйста введите новое название аналитики: ");
    var new_param = parseFloat(p)


    function change(param, a_pr_id) {
        if (param && param !== null) {
            var varData = {};
            varData['param'] = param;
            varData['a_pr_id'] = a_pr_id;
            return $.ajax({
                url: "/projects/analytics/num/param/edit/",
                type: "post",
                data: varData
            });
        }
    }

    change(new_param, p_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data && data !== 'FAIL') {
            $(tag).text(new_param);
        }
    });
    this.event.preventDefault();
}

<!-- Переименование булевых аналитик -->
function renameBoolAnalytic(an_id, tag) {
    var an_name = prompt("Пожалуйста введите новое название аналитики: ");

    function rename(name, a_id) {
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['a_id'] = a_id;
            return $.ajax({
                url: "/projects/analytics/bool/rename/",
                type: "post",
                data: varData
            });
        }
    }

    rename(an_name, an_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data && data !== 'FAIL') {
            $(tag).text(an_name);
        }
    });
    this.event.preventDefault();
}

<!-- Переименование булево позитив -->
function renameBoolPositive(an_id, tag) {
    var an_name = prompt("Пожалуйста введите новое значение: ");

    function rename(name, a_id) {
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['a_id'] = a_id;
            return $.ajax({
                url: "/projects/analytics/bool/positive/rename/",
                type: "post",
                data: varData
            });
        }
    }

    rename(an_name, an_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data && data !== 'FAIL') {
            $(tag).text(an_name);
        }
    });
    this.event.preventDefault();
}

<!-- Переименование булево позитив -->
function renameBoolNegative(an_id, tag) {
    var an_name = prompt("Пожалуйста введите новое значение(максимум 50 знаков): ");

    function rename(name, a_id) {
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['a_id'] = a_id;
            return $.ajax({
                url: "/projects/analytics/bool/negative/rename/",
                type: "post",
                data: varData
            });
        }
    }

    rename(an_name, an_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data && data !== 'FAIL') {
            $(tag).text(an_name);
        }
    });
    this.event.preventDefault();
}

<!-- Изменение параметра булевой аналитики на пазитивный-->
function change_param_bool_true(n_id, ba_id, tag) {
    function change(news_id, bool_id) {
        return $.ajax({
            dataType: "json",
            url: "analytics/bool/param/change/true/" + news_id + '/' + bool_id,
            type: "get"
        });
    }

    change(n_id, ba_id).done(function (data) {
        // Updates the UI based the ajax result
        console.log(data);
        console.log(data.status);
        if (data && data.status !== 'FAIL') {
            $(tag).text(data.new_name);
        }
    });
    this.event.preventDefault();
}

<!-- Изменение параметра булевой аналитики на негативный-->
function change_param_bool_false(n_id, ba_id, tag) {
    function change(news_id, bool_id) {
        return $.ajax({
            dataType: "json",
            url: "analytics/bool/param/change/false/" + news_id + '/' + bool_id,
            type: "get"
        });
    }

    change(n_id, ba_id).done(function (data) {
        // Updates the UI based the ajax result
        console.log(data);
        console.log(data.status);
        if (data && data.status !== 'FAIL') {
            $(tag).text(data.new_name);
        }
    });
    this.event.preventDefault();
}

<!-- Переименование списочных аналитик -->
function renameListAnalytic(an_id, tag) {
    var an_name = prompt("Пожалуйста введите новое название аналитики: ");

    function rename(name, a_id) {
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['a_id'] = a_id;
            return $.ajax({
                url: "/projects/analytics/list/rename/",
                type: "post",
                data: varData
            });
        }
    }

    rename(an_name, an_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data && data !== 'FAIL') {
            $(tag).text(an_name);
        }
    });
    this.event.preventDefault();
}

<!-- Переименование списочных аналитик -->
function change_param_list(p_id, tag) {
    var p_name = prompt("Пожалуйста введите новое название: ");

    function rename(name, pr_id) {
        if (name !== null) {
            var varData = {};
            varData['name'] = name;
            varData['p_id'] = pr_id;
            return $.ajax({
                url: "/projects/analytics/list/param/rename/",
                type: "post",
                data: varData
            });
        }
    }

    rename(p_name, p_id).done(function (data) {
        // Updates the UI based the ajax result
        if (data && data !== 'FAIL') {
            $(tag).text(p_name);
        }
    });
    this.event.preventDefault();
}

<!-- Изменение объектов -------------------------------- -->


<!-- Удаление объектов ++++++++++++++++++++++++++++++++ -->
function del_object(url, row_id) {
    if (confirm('Подтвердите удаление! \n После удаления объекта восстановить его НЕВОЗМОЖНО!\n После удаления объекта восстановить его НЕВОЗМОЖНО! ')) {
        $.get(url)
            .done(function () {
                $(row_id).hide();
            })
            .fail(function () {
                alert('Ошибка удаления, обновите страницу!');
            });
        this.event.preventDefault();
    }
}

<!-- Удаление объектов -------------------------------- -->

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

