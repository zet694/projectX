/**
 * Created by gad on 23.08.17.
 */
function cols_href_render(project_id) {
    var prefs = JSON.parse(localStorage.getItem('prefs_' + project_id));

    function pref_render(pref, pr_id) {
        var template = '<a  id="' + pr_id + '" class="toggle-cols-' + project_id + '" column="' + pref.column_id + '">' + pref.name + '</a>/';
        $('#cols_toggle_' + project_id).append(template);
    }

    var p;
    for (p in prefs) {
        pref_render(prefs[p], p);
    }

}

function show_hidden_row(project_id, news_id) {
    $('#hidden_' + project_id + '_' + news_id).show()

}

function num_analytics(paramnum_set) {
    var a = '',
        i = 0;
    for (i; i < paramnum_set.length; i++) {
        a = a + paramnum_set[i].analyticnum_set[0].name + ' (' + paramnum_set[i].num + ')<br>';
    }
    return a

}

function bool_analytics(get_bool) {
    var a = '',
        i = 0;
    for (i; i < get_bool.true.length; i++) {
        a = a + get_bool.true[i].name + ' (' + get_bool.true[i].name_positive + ')<br>';
    }
    i = 0;
    for (i; i < get_bool.false.length; i++) {
        a = a + get_bool.false[i].name + ' (' + get_bool.false[i].name_negative + ')<br>';
    }
    return a
}

function list_analytics(paramlist_set) {
    var a = '',
        i = 0;
    for (i; i < paramlist_set.length; i++) {
        a = a + paramlist_set[i].analyticlist_set[0].name + ' (' + paramlist_set[i].name + ')<br>';
    }
    return a
}

function title_render(data) {
    var tpl = '<a href="#" onclick=load_modal_news_edit(' + data.id + ')>' + data.title + '</a>';
    return tpl
}