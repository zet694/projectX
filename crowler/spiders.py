from django.utils import timezone
from bs4 import BeautifulSoup
import requests
from .models import News, Source
import locale
from django.db.utils import IntegrityError
from smimonitor.celery import app
# from smimonitor.dev_celery import app


def selector(source):
    if source.code == 0:
        return ria_spider.delay
    # if source.code == 1:
    #     return cbr_spider.delay
    if source.code == 2:
        return comers_spider.delay
    if source.code == 3:
        return eho_spider.delay
    if source.code == 4:
        return radiovesti_spider.delay
    if source.code == 6:
        return govoritmoskva_spider.delay


def get_soup(current_url):
    source_code = requests.get(current_url)
    plain_text = source_code.text
    current_soup = BeautifulSoup(plain_text)
    return current_soup


@app.task(options={'queue': 'writers'})
def news_writer(news, source_code):
    l = []
    source = Source.objects.get(code__exact=source_code)
    for i in news:
        if i['news_title'] or i['news_text']:
            try:
                n = News(url=i['news_href'],
                         text=i['news_text'],
                         date_post=i['news_date'],
                         title=i['news_title'],
                         source=source
                         )
                l.append(n)
            except Exception as e:
                print(e)
    print(timezone.datetime.now())
    for i in l:
        try:
            i.save()
        except IntegrityError as e:
            print(e)
    print(timezone.datetime.now())


@app.task()
def ria_spider(from_date, date_range, source_code):
    source = Source.objects.get(code=source_code)
    url = source.url
    collected_urls = set()
    all_news = []
    day = timezone.timedelta(days=1)
    from_date = timezone.datetime.strptime(from_date, '%Y-%m-%dT%H:%M:%S')

    def prepare_url(s_url, date):
        return s_url + '/archive/{}/'.format(timezone.datetime.strftime(date, '%Y%m%d'))

    def check_date(c_soup, c_date):
        dates_soup = c_soup.find_all('div', {'class': 'b-list__item-date'})
        for i in dates_soup:
            n_date = timezone.datetime.strptime(i.text, '%d.%m.%Y')
            if n_date.date() < c_date.date():
                return False
        return True

    def get_urls(gu_soup):
        c_urls = []
        for i in gu_soup:
            d_url = url + i.contents[0].attrs.get('href')
            if d_url.endswith('html'):
                c_urls.append(d_url)
        return c_urls

    def urls_getter(c_soup):
        g_urls = set()
        while True:
            divs_soup = c_soup.find_all('div', {'class': 'b-list__item'})
            g_urls.update(get_urls(divs_soup))
            if not check_date(c_soup, from_date):
                break
            s = c_soup.find('a', {'class': 'b-pager__button m-button-more'})
            try:
                current_url = url + s.attrs.get('data-ajax')
            except AttributeError as e:
                print(e)
                break
            c_soup = get_soup(current_url)

        return g_urls

    def check_urls(c_urls, c_source):
        all_urls = News.objects.filter(source=c_source).values_list('url')
        set_all_urls = set()

        for i in all_urls:
            set_all_urls.add(i)

        c_urls -= set_all_urls
        return c_urls

    def get_news_title(current_soup):
        try:
            header = current_soup.find('h1', {'class': 'b-article__title'})
            if header:
                t = ''
                t += header.text. \
                    strip()
                return t
        except AttributeError as e:
            print(e)
            return None

    def get_news_text(current_soup):
        n = ''
        try:
            for text in current_soup.findAll('div', {'class': 'b-article__body'}):
                for t in text.findAll('p'):
                    n += t.text
        except AttributeError as e:
            print(e, 'Нет текста по ссылке')
        return n

    def get_news_date(current_soup):
        g = current_soup.find('div', {'class', 'b-article__info-date'})
        try:
            dt = timezone.datetime.strptime(g.attrs.get('datetime') + '+0300', '%Y-%m-%dT%H:%M%z')
        except Exception as e:
            print(e)
            return None
        return dt

    def get_news(g_url):
        current_soup = get_soup(g_url)
        news_text = get_news_text(current_soup)
        news_date = get_news_date(current_soup)
        news_title = get_news_title(current_soup)
        return {'news_text': news_text, 'news_date': news_date, 'news_title': news_title, 'news_href': g_url}

    for i in range(date_range + 1):
        cur_url = prepare_url(url, from_date)
        cur_soup = get_soup(cur_url)
        collected_urls.update(urls_getter(cur_soup))
        collected_urls = check_urls(collected_urls, source)

        for i in collected_urls:
            all_news.append(get_news(i))

        news_writer.delay(all_news, source_code)

        from_date += day
        collected_urls = set()
        all_news = []


@app.task()
def comers_spider(from_date, date_range, source_code):
    source = Source.objects.get(code=source_code)
    url = source.url
    collected_urls = set()
    all_news = []
    day = timezone.timedelta(days=1)
    from_date = timezone.datetime.strptime(from_date, '%Y-%m-%dT%H:%M:%S')

    def prepare_url(s_url, date):
        return s_url + '/archive/news/{}'.format(timezone.datetime.strftime(date, '%Y-%m-%d'))

    def urls_getter(c_soup):
        g_urls = set()
        a_tags = c_soup.find_all('a', {'class': 'item'})

        for i in a_tags:
            g_urls.add(url + i.attrs.get('href'))

        return g_urls

    def check_urls(c_urls, c_source):
        all_urls = News.objects.filter(source=c_source).values_list('url')
        set_all_urls = set()

        for i in all_urls:
            set_all_urls.add(i)

        c_urls -= set_all_urls
        return c_urls

    def get_news_title(current_soup):
        try:
            header = current_soup.find('article', {'class': 'b-article'})
            header = header.find('header')
            header = header.find('div', {'class': 'text'})
            if header:
                t = ''
                t += header.text. \
                    strip(). \
                    replace('\t', ' '). \
                    replace('\xa0', ' '). \
                    replace('   ', ' '). \
                    replace('  ', ' ')
                return t
        except AttributeError as e:
            print(e)
        return None

    def get_news_text(current_soup):
        n = ''
        text = current_soup.find_all('p', {'class', 'b-article__text'})
        if text:
            for t in text:
                n += t.text. \
                    strip(). \
                    replace('\t', ' '). \
                    replace('\xa0', ' '). \
                    replace('   ', ' '). \
                    replace('  ', ' ')
        return n

    def get_news_date(current_soup):

        g = current_soup.find('time', {'class', 'title__cake'})
        try:
            d = g.attrs.get('datetime')
            dt = timezone.datetime.strptime(d[:-3] + d[-2:], '%Y-%m-%dT%H:%M:%S%z')
        except Exception as e:
            print(e)
            g = current_soup.find('time', {'class', 'b-article__publish_date'})
            try:
                d = g.attrs.get('datetime')
                dt = timezone.datetime.strptime(d[:-3] + d[-2:], '%Y-%m-%dT%H:%M:%S%z')
            except Exception as e:
                print(e)
                return None
        return dt

    def get_news(g_url):
        current_soup = get_soup(g_url)
        news_text = get_news_text(current_soup)
        news_date = get_news_date(current_soup)
        news_title = get_news_title(current_soup)
        return {'news_text': news_text, 'news_date': news_date, 'news_title': news_title, 'news_href': g_url}

    for i in range(date_range + 1):
        cur_url = prepare_url(url, from_date)
        cur_soup = get_soup(cur_url)
        collected_urls.update(urls_getter(cur_soup))
        collected_urls = check_urls(collected_urls, source)

        for i in collected_urls:
            all_news.append(get_news(i))

        news_writer.delay(all_news, source_code)

        from_date += day
        collected_urls = set()
        all_news = []


@app.task()
def eho_spider(from_date, date_range, source_code):
    source = Source.objects.get(code=source_code)
    url = source.url
    collected_urls = set()
    all_news = []
    day = timezone.timedelta(days=1)
    from_date = timezone.datetime.strptime(from_date, '%Y-%m-%dT%H:%M:%S')
    locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')

    def prepare_url(s_url, date):
        return s_url + '/news/{}.html?'.format(timezone.datetime.strftime(date, '%Y/%m/%d'))

    def urls_getter(c_soup):
        g_urls = set()
        n_block = c_soup.find('div', {'class': 'newslist'})
        try:
            cur_news = n_block.find_all('div', {'class': 'preview'})

            for n in cur_news:
                h = n.find('h3')
                a = h.find('a')
                g_urls.add(url + a.attrs.get('href'))
        except AttributeError as e:
            print(e, 'From eho_spider in urls_getter')

        return g_urls

    def check_urls(c_urls, c_source):
        all_urls = News.objects.filter(source=c_source).values_list('url')
        set_all_urls = set()

        for i in all_urls:
            set_all_urls.add(i)

        c_urls -= set_all_urls
        return c_urls

    def get_news_title(current_soup):

        try:
            header = current_soup.find('div', {'class': 'conthead news'})
            h = header.find('h1')
            if h:
                t = ''
                t += h.text. \
                    strip(). \
                    replace('\t', ' '). \
                    replace('\xa0', ' '). \
                    replace('   ', ' '). \
                    replace('  ', ' ')
                return t
        except AttributeError as e:
            print(e)
        return None

    def get_news_text(current_soup):
        n = ''
        text = current_soup.find_all('div', {'class', 'typical'})
        if text:
            for t in text:
                n += t.text. \
                    strip(). \
                    replace('\t', ' '). \
                    replace('\xa0', ' '). \
                    replace('   ', ' '). \
                    replace('  ', ' ')
        return n

    def get_news_date(current_soup):
        g = current_soup.find('div', {'class': 'conthead news'})
        try:
            t = g.find('div', {'class': 'date'})
            d = t.text.replace('\n', ' '). \
                replace('\t', ' '). \
                replace('\xa0', ' '). \
                replace('   ', ' '). \
                replace('   ', ' '). \
                replace('  ', ' '). \
                strip()
            dt = timezone.datetime.strptime(d[:13] + d[-5:] + ' +0300', '%H:%M, %d %b %Y %z')
            return dt
        except Exception as e:
            # print(e)
            try:
                t = g.find('div', {'class': 'date'})
                d = t.text.replace('\n', ' '). \
                    replace('\t', ' '). \
                    replace('\xa0', ' '). \
                    replace('   ', ' '). \
                    replace('   ', ' '). \
                    replace('  ', ' '). \
                    strip()
                dt = timezone.datetime.strptime(d[:10] + 'май' + d[-5:] + ' +0300', '%H:%M, %d %b %Y %z')
                return dt
            except Exception as e:
                print(e)
                print('Не майская проблема')
                return None

    def get_news(g_url):
        print(g_url)
        current_soup = get_soup(g_url)
        news_text = get_news_text(current_soup)
        news_date = get_news_date(current_soup)
        news_title = get_news_title(current_soup)
        return {'news_text': news_text, 'news_date': news_date, 'news_title': news_title, 'news_href': g_url}

    for i in range(date_range + 1):
        cur_url = prepare_url(url, from_date)
        cur_soup = get_soup(cur_url)
        collected_urls.update(urls_getter(cur_soup))
        collected_urls = check_urls(collected_urls, source)

        for i in collected_urls:
            all_news.append(get_news(i))

        news_writer.delay(all_news, source_code)

        from_date += day
        collected_urls = set()
        all_news = []


@app.task()
def radiovesti_spider(from_date, date_range, source_code):
    source = Source.objects.get(code=source_code)
    url = source.url
    collected_urls = set()
    all_news = []
    day = timezone.timedelta(days=1)
    from_date = timezone.datetime.strptime(from_date, '%Y-%m-%dT%H:%M:%S')

    def prepare_url(s_url, date):
        return s_url + '/news/category/all/{}/'.format(timezone.datetime.strftime(date, '%d-%m-%Y'))

    def urls_getter(start_url):
        soup = get_soup(start_url)
        pages = soup.find_all('a', {'class': 'pager__link'})
        g_urls = set()

        def get_urls(c_soup):
            c_urls = []
            url_soup = c_soup.find_all('a', {'class': 'news__item-title'})
            for j in url_soup:
                c_urls.append(url + j['href'])
            return c_urls

        if len(pages) > 1:
            for i in pages:
                cur_soup = get_soup(url + i['href'])
                g_urls.update(get_urls(cur_soup))
        else:
            g_urls.update(get_urls(soup))

        return g_urls

    def check_urls(c_urls, c_source):
        all_urls = News.objects.filter(source=c_source).values_list('url')
        set_all_urls = set()

        for i in all_urls:
            set_all_urls.add(i)

        c_urls -= set_all_urls
        return c_urls

    def get_news(g_url):
        current_soup = get_soup(g_url)
        news_text = get_news_text(current_soup)
        news_date = get_news_date(current_soup)
        news_title = get_news_title(current_soup)
        return {'news_text': news_text, 'news_date': news_date, 'news_title': news_title, 'news_href': g_url}

    def get_news_title(current_soup):
        try:
            header = current_soup.find('h1', {'class': 'h1-title'})
            if header:
                t = ''
                t += header.text. \
                    replace('“', '"'). \
                    replace('”', '"'). \
                    replace('…', ' '). \
                    replace('–', '-'). \
                    replace('—', '-'). \
                    replace('‍', ' '). \
                    encode('ISO-8859-1'). \
                    decode('utf-8', errors='replace'). \
                    replace('\t', ' '). \
                    replace('�', ' '). \
                    replace('\r', ' '). \
                    replace('\u201c', ' '). \
                    replace('    ', ' '). \
                    replace('   ', ' '). \
                    replace('  ', ' '). \
                    replace('  ', ' '). \
                    strip()
                return t
        except AttributeError as e:
            print(e, 'Нет заголовка по ссылке. Ошибка парсера')
        return None

    def get_news_text(current_soup):
        t = ''
        try:
            art = current_soup.find('div', {'class': 'insides-page__news__text insides-page__news__text_with-popular'})
            t = art.text. \
                replace('“', '"'). \
                replace('”', '"'). \
                replace('…', ' '). \
                replace('–', '-'). \
                replace('—', '-'). \
                replace('‍', ' '). \
                replace('\u20ac', 'EURO'). \
                encode('ISO-8859-1'). \
                decode('utf-8', errors='replace'). \
                replace('\t', ' '). \
                replace('�', ' '). \
                replace('\r', ' '). \
                replace('\u201c', ' '). \
                replace('    ', ' '). \
                replace('   ', ' '). \
                replace('  ', ' '). \
                replace('  ', ' '). \
                strip()
        except AttributeError as e:
            print(e, 'Нет текста по ссылке. Ошибка парсера')
        return t

    def get_news_date(current_soup):
        try:
            locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
            date = current_soup.find('div', {'class': 'news__date'})
            d = date.text. \
                replace('\n', ' '). \
                replace('\t', ' '). \
                replace('\xa0', ' '). \
                replace('    ', ' '). \
                replace('   ', ' '). \
                replace('  ', ' '). \
                replace('  ', ' '). \
                strip().encode('ISO-8859-1').decode('utf-8')
            dt = timezone.datetime.strptime(d[:6] + d[-12:] + ' +0300', '%d %b %Y, %H:%M %z')
            return dt
        except AttributeError as e:
            print(e, 'Нет даты по ссылке. Ошибка парсера')
        return None

    for i in range(date_range + 1):
        cur_url = prepare_url(url, from_date)
        collected_urls.update(urls_getter(cur_url))
        collected_urls = check_urls(collected_urls, source)

        for i in collected_urls:
            all_news.append(get_news(i))

        news_writer.delay(all_news, source_code)

        from_date += day
        collected_urls = set()
        all_news = []


@app.task()
def govoritmoskva_spider(from_date='', date_range=0, source_code=0, get_param=False, n_urls=[]):
    # todo Должок по очередям при вызове из очереди таск

    def prepare_url(s_url, date):
        return s_url + '/news/?sort-select=date&rubric-select=&date-select={}'.format(timezone.datetime.strftime(date, '%d.%m.%Y'))

    def urls_getter(start_url):

        g_urls = []

        def get_urls(c_url):
            c_soup = get_soup(c_url)
            c_urls = []
            span_soup = c_soup.find_all('span', {'class': 'share'})
            for j in span_soup:
                c_urls.append(url + j['data-link'])
            g_urls.extend(c_urls)
            next_soup = c_soup.find('a', {'class': 'after'})
            if next_soup:
                get_urls(url + next_soup['href'])

        get_urls(start_url)

        return g_urls

    def check_urls(c_urls, c_source):
        min_date = timezone.datetime.strptime(from_date + ' +0300', '%Y-%m-%dT%H:%M:%S %z')
        max_date = min_date + day * (date_range + 1)
        all_urls = News.objects.filter(source=c_source, date_post__gte=min_date).exclude(date_post__gt=max_date).values_list('url')
        set_all_urls = set(all_urls)
        c_urls -= set_all_urls
        return c_urls

    def get_news(g_url, date):
        current_soup = get_soup(g_url)
        news_title = get_news_title(current_soup)
        news_date = get_news_date(current_soup, date)
        news_text = get_news_text(current_soup)
        return {'news_text': news_text, 'news_date': news_date, 'news_title': news_title, 'news_href': g_url}

    def get_news_title(current_soup):
        try:
            header = current_soup.find('h1')
            if header:
                t = ''
                t += header.text. \
                    replace('“', '"'). \
                    replace('”', '"'). \
                    replace('…', ' '). \
                    replace('–', '-'). \
                    replace('—', '-'). \
                    replace('‍', ' '). \
                    replace('\t', ' '). \
                    replace('�', ' '). \
                    replace('\r', ' '). \
                    replace('\u201c', ' '). \
                    replace('    ', ' '). \
                    replace('   ', ' '). \
                    replace('  ', ' '). \
                    replace('  ', ' '). \
                    strip()
                return t
        except AttributeError as e:
            print(e, 'Нет заголовка по ссылке. Ошибка парсера')
        return None

    def get_news_text(current_soup):
        t = ''
        try:
            art = current_soup.find('div', {'class': 'textContent'})
            t = art.text. \
                replace('“', '"'). \
                replace('”', '"'). \
                replace('…', ' '). \
                replace('–', '-'). \
                replace('—', '-'). \
                replace('‍', ' '). \
                replace('\u20ac', 'EURO'). \
                replace('\t', ' '). \
                replace('�', ' '). \
                replace('\r', ' '). \
                replace('\u201c', ' '). \
                replace('    ', ' '). \
                replace('   ', ' '). \
                replace('  ', ' '). \
                replace('  ', ' '). \
                strip()
        except AttributeError as e:
            print(e, 'Нет текста по ссылке. Ошибка парсера')
        return t

    def get_news_date(current_soup, c_date):
        try:
            locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
            date = current_soup.find('p', {'class': 'noteInformation'})
            d = date.text. \
                replace('\n', ' '). \
                replace('\t', ' '). \
                replace('\xa0', ' '). \
                replace('.', ''). \
                replace(',', ''). \
                replace('    ', ' '). \
                replace('   ', ' '). \
                replace('  ', ' '). \
                replace('  ', ' '). \
                strip()
            try:
                if d.split(' ')[1] == 'сегодня':
                    d = d.replace('сегодня', c_date.strftime('%b %d %Y'))
                    d = d.split(' ')
                    d = ' '.join(d[0:4]) + ' +0300'
                    dt = timezone.datetime.strptime(d.replace('сегодня', c_date.strftime('%b %d %Y')), '%H:%M %b %d %Y %z')
                    return dt
                elif d.split(' ')[1] == 'вчера':
                    d = d.replace('вчера', c_date.strftime('%b %d %Y'))
                    d = d.split(' ')
                    d = ' '.join(d[0:4]) + ' +0300'
                    dt = timezone.datetime.strptime(d.replace('вчера', c_date.strftime('%b %d %Y')), '%H:%M %b %d %Y %z')
                    return dt
            except ValueError as e:
                print(e)
            try:
                d = d.split(' ')
                d[1] = d[1].lower()[0:3]
                d = ' '.join(d[0:4]) + ' +0300'
                dt = timezone.datetime.strptime(d, '%H:%M %b %d %Y %z')
                return dt
            except ValueError as e:
                print(e)

        except AttributeError as e:
            print(e, 'Нет даты по ссылке. Ошибка парсера')
        return None

    if get_param and n_urls and from_date and source_code:
        all_news = []
        cur_from_date = timezone.datetime.strptime(from_date, '%Y-%m-%dT%H:%M:%S')
        for i in n_urls:
            all_news.append(get_news(i, cur_from_date))
        news_writer.delay(all_news, source_code)

    else:

        source = Source.objects.get(code=source_code)
        url = source.url
        collected_urls = set()
        day = timezone.timedelta(days=1)
        cur_from_date = timezone.datetime.strptime(from_date, '%Y-%m-%dT%H:%M:%S')
        for i in range(date_range + 1):
            cur_url = prepare_url(url, cur_from_date)
            collected_urls.update(urls_getter(cur_url))
            collected_urls = check_urls(collected_urls, source)
            if collected_urls:
                govoritmoskva_spider.delay(get_param=True, n_urls=list(collected_urls), from_date=cur_from_date.strftime('%Y-%m-%dT%H:%M:%S'), source_code=source_code)
            cur_from_date += day
            collected_urls = set()