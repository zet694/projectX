from .models import News
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from django.conf import settings
from django.utils import timezone
from smimonitor.celery import app
# from smimonitor.dev_celery import app


def news_preparator(news_objects_list):
    n_l = [{
               'id': i.id,
               'post_date': i.date_post,
               'source': i.source.name,
               'url': i.url,
               'title': i.title,
               'text': i.text
           } for i in news_objects_list]
    return n_l


@app.task(options={'queue': 'syncs'})
def update_index(news=False, host=settings.ES_SERVER, index=settings.ES_DEFAULT_INDEX,
                 doc_type=settings.ES_DEFAULT_DOC_TYPE, user=settings.NGINX_USER, passwd=settings.NGINX_PASS):
    es = Elasticsearch([host, ],
                       http_auth=(user, passwd),
                       port=9200
                       )

    def bulk_preparator(news_list, b_index=settings.ES_DEFAULT_INDEX,
                        b_doc_type=settings.ES_DEFAULT_DOC_TYPE):

        b = [[{
            'index': {
                "_index": b_index,
                "_type": b_doc_type,
                # "id": i['id']
            }
        }, i] for i in news_list]
        bu = sum(b, [])
        return bu

    def write_bulk_elastic(prepared_bulk):
        for i in range(0, len(prepared_bulk)+1, 300):
            es.bulk(index=index, body=prepared_bulk[i:i+300])

    # print(es.count(index=index, doc_type=doc_type)['count'])
    # print(News.objects.all().count())

    if news:
        write_bulk_elastic(bulk_preparator(news))

    elif es.count(index=index, doc_type=doc_type)['count'] != News.objects.all().count():
        s = Search(using=es, index='news', doc_type='news').source(fields=['id', ])
        s = s.params(size=8000)
        n = set(News.objects.all().values_list('id', flat=True))
        es_ids = set(list(map(lambda x: x.id, s.scan())))
        ids = n - es_ids
        obj_news = News.objects.all().filter(id__in=ids)
        write_bulk_elastic(bulk_preparator(news_preparator(obj_news)))


@app.task(options={'queue': 'syncs'})
def news_saver(ids):
    for j in News.objects.filter(id__in=ids):
        j.save()


@app.task(options={'queue': 'syncs'})
def update_index_v2():
    quantity = 10000
    l = News.objects.all().values_list('id', flat=True)
    for i in range(0, len(l) + 1, quantity):
        news_saver.delay(l[i:i+quantity])
