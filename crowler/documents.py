from crowler.models import News as N
from django_elasticsearch_dsl import DocType, Index, fields
from django.conf import settings

news = Index(settings.ES_DEFAULT_INDEX)


@news.doc_type
class News(DocType):
    post_date = fields.StringField(attr="date_post")
    source = fields.StringField(attr='source.name')

    class Meta:
        model = N
        fields = [
            'id',
            'url',
            'title',
            'text'
        ]