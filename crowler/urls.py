from django.conf.urls import url, include
from django.contrib.auth.views import logout
from django.contrib.admin.views.decorators import staff_member_required
from . import views
from django.conf import settings
from django.conf.urls.static import static
from .admin import admin_crowler



urlpatterns = [
    url(r'^$', staff_member_required(views.crowler), name='crowler'),
    url(r'^news_stats$', staff_member_required(views.news_stats), name='news_stats'),
    url(r'^xml/news_not_unloaded$', staff_member_required(views.xml_news_not_unloaded), name='xml_news_not_unloaded'),
    url(r'^xml/news_today$', staff_member_required(views.xml_news_today), name='xml_news_today'),
    url(r'^xml/news_yesterday$', staff_member_required(views.xml_news_yesterday), name='xml_news_yesterday'),
    url(r'^xml/news_all$', staff_member_required(views.xml_news_all), name='xml_news_all'),
    url(r'^xml/manual$', staff_member_required(views.xml_manual), name='xml_manual'),

    url(r'^clean_up_base$', staff_member_required(views.clean_up_base), name='clean_up_base'),

    url(r'^crowler/hand_scan$', staff_member_required(views.hand_scan), name='hand_scan'),


    url(r'^crowler/admin/', admin_crowler.urls),
    # url(r'^analyzer/$', staff_member_required(views.analyzer), name='analyzer'),
    # url(r'^charts/$', staff_member_required(views.charts), name='charts'),
    # url(r'^exchange_cbrf/$', staff_member_required(views.exchange_cbrf), name='exchange_cbrf'),

    # url(r'^get_news/$', staff_member_required(views.get_news), name='get_news'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)