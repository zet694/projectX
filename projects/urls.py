from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.admin.views.decorators import staff_member_required
from projects.views import *

urlpatterns = [

                  url(r'^$', staff_member_required(projects), name='projects'),
                  # Проекты
                  url(r'^add/$', staff_member_required(add_project), name='add'),
                  url(r'^get/$', staff_member_required(get_projects), name='get'),
                  # url(r'^get_one/(?P<pk>[0-9]+)$', staff_member_required(get_project), name='get_one'),
                  url(r'^del_one/(?P<pk>[0-9]+)$', staff_member_required(del_project)),
                  url(r'^(?P<pk>[0-9]+)/journal/$', staff_member_required(get_journal)),
                  url(r'^rename/$', staff_member_required(rename_project), name='rename_project'),

                  # Темы
                  url(r'^themes/add/$', staff_member_required(add_theme), name='add_theme'),
                  url(r'^themes/rename/$', staff_member_required(rename_theme), name='rename_theme'),
                  url(r'^(?P<pk>[0-9]+)/themes/get/$', staff_member_required(get_themes)),
                  url(r'^(?P<pk>[0-9]+)/themes_ids/$', staff_member_required(themes_ids)),
                  # url(r'^themes/get_one/(?P<pk>[0-9]+)$', staff_member_required(get_theme), name='get_one'),
                  url(r'^themes/del_one/(?P<pk>[0-9]+)$', staff_member_required(del_theme)),

                  # Рубрики
                  url(r'^rubric/add/$', staff_member_required(add_rubric), name='add_rubric'),
                  url(r'^rubric/rename/$', staff_member_required(rename_rubric), name='rename_rubric'),
                  url(r'^projects_ids/$', staff_member_required(projects_ids), name='projects_ids'),
                  url(r'^themes/(?P<pk>[0-9]+)/rubrics/get/$', staff_member_required(get_rubrics)),
                  url(r'^themes/(?P<pk>[0-9]+)/rubrics_ids/$', staff_member_required(rubrics_ids)),
                  url(r'^rubrics/del_one/(?P<pk>[0-9]+)$', staff_member_required(del_rubric)),

                  # Аналитики
                  # Числовая
                  url(r'^analytics/num/add/$', staff_member_required(add_num_analytics), name='add_num_analytics'),
                  url(r'^(?P<pk>[0-9]+)/analytics/num/get/$', staff_member_required(get_num_analytics),
                      name='get_num_analytics'),
                  url(r'^(?P<pk>[0-9]+)/analytics/num/ids/$', staff_member_required(num_analytics_ids),
                      name='num_analytics_ids'),
                  url(r'^num_analytics/del_one/(?P<pk>[0-9]+)$', staff_member_required(del_num_analytic)),
                  url(r'^num_analytics/rename/$', staff_member_required(rename_num_analytics),
                      name='rename_num_analytics'),
                  url(r'^num_analytics/rename/$', staff_member_required(rename_num_analytics),
                      name='rename_num_analytics'),
                  url(r'^analytics/num/(?P<pk>[0-9]+)/add_param/$', staff_member_required(add_param_num_analytics),
                      name='add_param_num_analytics'),
                  # todo заменить на ту что из новостей
                  url(r'^analytics/num/param/del_one/(?P<pk>[0-9]+)$', staff_member_required(del_param_num_analytics)),
                  url(r'^analytics/num/param/edit/$', staff_member_required(edit_param_num_analytics)),
                  url(r'^analytics/num/param/journal/$', staff_member_required(edit_param_num_analytics)),

                  # Булевы
                  url(r'^analytics/bool/(?P<pk>[0-9]+)/ids/$', staff_member_required(bool_analytics_ids)),
                  url(r'^analytics/bool/params/(?P<pk>[0-9]+)/ids/$', staff_member_required(bool_params_ids)),
                  url(r'^analytics/bool/add/$', staff_member_required(add_bool_analytics), name='add_bool_analytics'),
                  url(r'^(?P<pk>[0-9]+)/analytics/bool/get/$', staff_member_required(get_bool_analytics),
                      name='bool_params_ids'),
                  url(r'^analytics/bool/rename/$', staff_member_required(rename_bool_analytics),
                      name='rename_bool_analytics'),
                  url(r'^analytics/bool/positive/rename/$', staff_member_required(rename_bool_positive),
                      name='rename_bool_positive'),
                  url(r'^analytics/bool/negative/rename/$', staff_member_required(rename_bool_negative),
                      name='rename_bool_negative'),
                  url(r'^analytics/bool/del_one/(?P<pk>[0-9]+)$', staff_member_required(del_bool_analytic)),
                  url(r'^analytics/bool/param/add/true$', staff_member_required(add_bool_analytic_param_true)),
                  url(r'^analytics/bool/param/add/false$', staff_member_required(add_bool_analytic_param_false)),
                  url(r'^analytics/bool/param/change/true/(?P<n_id>[0-9]+)/(?P<b_id>[0-9]+)$',
                      staff_member_required(change_bool_analytic_param_true)),
                  url(r'^analytics/bool/param/change/false/(?P<n_id>[0-9]+)/(?P<b_id>[0-9]+)$',
                      staff_member_required(change_bool_analytic_param_false)),
                  url(r'^analytics/bool/param/del_one/(?P<n_id>[0-9]+)/(?P<b_id>[0-9]+)$',
                      staff_member_required(del_bool_analytic_param)),

                  # Списочные
                  url(r'^(?P<pk>[0-9]+)/analytics/list/get/$', staff_member_required(get_list_analytics)),
                  url(r'^analytics/list/(?P<pk>[0-9]+)/ids/$', staff_member_required(list_analytics_ids)),
                  url(r'^analytics/list/params/(?P<pk>[0-9]+)/ids/$', staff_member_required(list_params_ids)),
                  url(r'^analytics/list/add/$', staff_member_required(add_list_analytics)),
                  url(r'^analytics/list/del_one/(?P<pk>[0-9]+)$', staff_member_required(del_list_analytic)),
                  url(r'^analytics/list/rename/$', staff_member_required(rename_list_analytics)),
                  url(r'^analytics/list/param/add/$', staff_member_required(add_list_param)),
                  url(r'^analytics/list/param/del_one/(?P<pk>[0-9]+)$', staff_member_required(del_list_param)),
                  url(r'^analytics/list/param/rename/$', staff_member_required(rename_list_param)),
                  url(r'^analytics/list/param/news/add$', staff_member_required(add_list_analytic_param)),
                  url(r'^analytics/list/param/del_one/(?P<n_id>[0-9]+)/(?P<p_id>[0-9]+)$',
                      staff_member_required(del_list_analytic_param)),

                  # Журнал

                  # Новости
                  # создать
                  url(r'^news/create/$', staff_member_required(project_news_create)),
                  # get news
                  url(r'^news/(?P<pk>[0-9]+)/edit', staff_member_required(get_project_news_edit)),
                  url(r'^news/(?P<pk>[0-9]+)/get/detail/row$', staff_member_required(get_news_project_row)),
                  url(r'^news/annotation/save$', staff_member_required(news_annotation_save)),
                  url(r'^news/text/save$', staff_member_required(news_text_save)),
                  url(r'^news/param/num/add/one$', staff_member_required(news_num_add_one)),
                  url(r'^news/param/num/del/one/(?P<pk>[0-9]+)$', staff_member_required(del_param_num_analytics)),
                  url(r'^news/param/bool/add/true$', staff_member_required(news_bool_add_true)),
                  url(r'^news/param/bool/add/false$', staff_member_required(news_bool_add_false)),
                  url(r'^news/param/bool/del/one/(?P<n_id>[0-9]+)/(?P<b_id>[0-9]+)$',
                      staff_member_required(del_bool_analytic_param)),
                  url(r'^news/param/list/add/one$', staff_member_required(news_list_add_one)),
                  url(r'^news/param/list/del/one/(?P<n_id>[0-9]+)/(?P<p_id>[0-9]+)$',
                      staff_member_required(del_list_analytic_param)),

                  # add news
                  url(r'^(?P<pk>[0-9]+)/add_news$', staff_member_required(add_news_project)),
                  url(r'^themes/(?P<pk>[0-9]+)/add_news$', staff_member_required(add_news_themes)),
                  url(r'^rubrics/(?P<pk>[0-9]+)/add_news$', staff_member_required(add_news_rubrics)),

                  url(r'^themes/(?P<pk>[0-9]+)/rubrics_ids$', staff_member_required(rubrics_ids), name=''),

                  # ListViews
                  # url(r'^sourcefull_list/$', staff_member_required(SourceFullListView.as_view()),
                  #     name='sourcefull_list'),
                  #   Формы
                  url(r'^status/create/(?P<pr_id>[0-9]+)$', staff_member_required(get_status_form)),

                  url(r'^make_xml/$', xml_news),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
