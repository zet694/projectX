from django import forms
from projects.models import *


class StatusForm(forms.Form):
    name = forms.CharField(max_length=50)
    color = forms.CharField(max_length=20)
    bold = forms.BooleanField(initial=False, required=False)
    editable = forms.BooleanField(initial=False, required=False)
    project = forms.IntegerField(widget=forms.HiddenInput())