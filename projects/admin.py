from django.contrib import admin
from projects.models import Project, Theme
from projects.models import *
# Register your models here.

# admin.site.register(Annotation)
admin.site.register(ParamList)
admin.site.register(AnalyticList)
admin.site.register(AnalyticBool)
admin.site.register(AnalyticNum)
admin.site.register(ParamNum)
admin.site.register(ProjectNews)
admin.site.register(Rubric)
admin.site.register(Theme)
admin.site.register(Project)