import json

from django.shortcuts import render
from django.views.generic.edit import CreateView
from django.views.decorators.csrf import csrf_exempt
from projects.models import Project, Theme, Rubric, ProjectNews, AnalyticNum, ParamNum, AnalyticBool, AnalyticList, \
    ParamList
from crowler.models import News
from catalog.models import SourceFull
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from projects.forms import *
from django.views.generic.edit import FormView
from django.http import Http404
from django.views.generic import CreateView, UpdateView
from crowler.xml_handlers import make_xml_v2_project_news
# Create your views here.


def projects(request):
    return render(request, 'projects/projects_index.html')


def get_news_project_analytics(news):
    np = ParamNum.objects.filter(p_news=news)
    np = list(np.values('id', 'num', 'analyticnum__name'))

    ba_f = list(AnalyticBool.objects.filter(p_news_false=news).values('id', 'name', 'name_negative'))
    ba_t = list(AnalyticBool.objects.filter(p_news_true=news).values('id', 'name', 'name_positive'))
    bp = []

    for p in ba_f:
        p.update({'val': 0})
    bp.extend(ba_f)

    for p in ba_t:
        p.update({'val': 1})
    bp.extend(ba_t)

    lp = ParamList.objects.filter(p_news=news)
    lp = lp.values('id', 'name', 'analyticlist__name')

    r = {'nums': np, 'bools': bp, 'lists': lp}

    return r


def get_project_news_edit(request, pk):
    if request.method == 'GET':
        n = ProjectNews.objects.get(id=pk)
        p = request.user.project_set.all()
        s = SourceFull.objects.all()
        return render(request, 'projects/project_news_edit.html', {'news': n, 'projects': p, 'sources': s})


def get_news_project_row(request, pk):
    if request.method == 'GET':
        n = ProjectNews.objects.get(id=pk)
        p = n.project_set.first()
        return render(request, 'projects/project_news_row.html', {'news': n, 'project': p})


@csrf_exempt
def news_annotation_save(request):
    if request.method == 'POST':
        n_id = request.POST.get('n_id')
        annotation = request.POST.get('annotation')
        news = ProjectNews.objects.get(id=n_id)
        news.annotation = annotation
        news.save()
        r = {'status': 'OK'}
        return JsonResponse(r, safe=True)


@csrf_exempt
def news_text_save(request):
    if request.method == 'POST':
        n_id = request.POST.get('n_id')
        text = request.POST.get('text')
        news = ProjectNews.objects.get(id=n_id)
        news.text = text
        news.save()
        r = {'status': 'OK'}
        return JsonResponse(r, safe=True)


@csrf_exempt
def news_num_add_one(request):
    r = dict()
    msg = 'OK'
    if request.method == 'POST':
        name = request.user
        a_id = request.POST.get('a_id')
        n_id = request.POST.get('n_id')
        param = request.POST.get('param')
        print(param)
        if name:
            a = AnalyticNum.objects.get(id=a_id)
            if a.owner == name:
                try:
                    news = ProjectNews.objects.get(id=n_id)
                    if a in news.num_analytics.all():
                        msg = 'Новось уже имеет помечена этой аналитикой, удалите эту аналитику из новости или измените ее'
                    else:
                        p = ParamNum.objects.create(num=float(param), p_news=news)
                        a.param.add(p)
                        a.save()
                        news.num_analytics.add(a)
                        news.save()
                        r.update([('status', 'OK'),
                                  ('param',
                                   {'id': p.id, 'name': p.analyticnum_set.first().name, 'value': p.num}
                                   )
                                  ]
                                 )
                    r.update([('msg', msg)])
                except Exception as e:
                    print(e)
                    r.update([('status', 'FAIL'), ('msg', msg)])
    return JsonResponse(r, safe=True)


@csrf_exempt
def news_bool_add_true(request):
    msg = 'OK'
    r = dict()
    r.update([('status', 'FAIL'), ('msg', msg)])

    if request.method == 'POST':
        name = request.user
        n_id = request.POST.get('n_id')
        b_id = request.POST.get('b_id')
        if name:
            a = AnalyticBool.objects.get(id=b_id)
            if a.owner == name:
                news = ProjectNews.objects.get(id=n_id)
                a.p_news_false.remove(news)
                a.p_news_true.add(news)
                news.bool_analytics.add(a)
                r.update([('status', 'OK'),
                          ('param',
                           {'id': a.id, 'name': a.name, 'value': a.name_positive, 'news_id': news.id}
                           )
                          ]
                         )
                r.update([('msg', msg)])
    print(r)
    return JsonResponse(r, safe=True)


@csrf_exempt
def news_bool_add_false(request):
    msg = 'OK'
    r = dict()
    r.update([('status', 'FAIL'), ('msg', msg)])

    if request.method == 'POST':
        name = request.user
        n_id = request.POST.get('n_id')
        b_id = request.POST.get('b_id')
        if name:
            a = AnalyticBool.objects.get(id=b_id)
            if a.owner == name:
                news = ProjectNews.objects.get(id=n_id)
                a.p_news_false.add(news)
                a.p_news_true.remove(news)
                news.bool_analytics.add(a)
                r.update([('status', 'OK'),
                          ('param',
                           {'id': a.id, 'name': a.name, 'value': a.name_negative}
                           )
                          ]
                         )
                r.update([('msg', msg)])
    print(r)

    return JsonResponse(r, safe=True)


@csrf_exempt
def news_list_add_one(request):
    r = dict()
    msg = 'OK'
    if request.method == 'POST':
        name = request.user
        l_id = request.POST.get('l_id')
        n_id = request.POST.get('n_id')
        param = request.POST.get('param')
        print(param)
        if name:
            a = AnalyticList.objects.get(id=l_id)
            if a.owner == name:
                try:
                    news = ProjectNews.objects.get(id=n_id)
                    p = ParamList.objects.get(id=param)
                    if news in p.p_news.all():
                        msg = 'Новось уже помечена параметром этой аналитики'
                    else:
                        p.p_news.add(news)
                        news.list_analytics.add(a)
                        r.update([('status', 'OK'),
                                  ('param',
                                   {'id': p.id, 'name': p.analyticlist_set.first().name, 'value': p.name, 'news_id': news.id}
                                   )
                                  ]
                                 )
                    r.update([('msg', msg)])
                except Exception as e:
                    print(e)
                    r.update([('status', 'FAIL'), ('msg', msg)])
    return JsonResponse(r, safe=True)


def get_projects(request):
    if request.method == 'GET':
        all_proj = Project.objects.filter(owner=request.user)
        return render(request, 'projects/projects_detail.html', {'projects': all_proj})
    return HttpResponse('FAIL')


def get_project(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        if p.owner == request.user:
            return render(request, 'projects/projects_detail.html', {'project': p})
    return HttpResponse('FAIL')


def get_themes(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        t = p.themes.all()
        return render(request, 'projects/themes_detail.html', {'themes': t})
    return HttpResponse('FAIL')


def get_rubrics(request, pk):
    if request.method == 'GET':
        t = Theme.objects.get(id=pk)
        r = t.rubrics.all()
        return render(request, 'projects/rubrics_detail.html', {'rubrics': r})
    return HttpResponse('FAIL')


def get_num_analytics(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        an = p.num_analytics.all()
        return render(request, 'projects/num_analytics_detail.html', {'analytics': an})
    return HttpResponse('FAIL')


def get_bool_analytics(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        an = p.bool_analytics.all()
        return render(request, 'projects/bool_analytics_detail.html', {'analytics': an})
    return HttpResponse('FAIL')


def get_list_analytics(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        an = p.list_analytics.all()
        return render(request, 'projects/list_analytics_detail.html', {'analytics': an})
    return HttpResponse('FAIL')


@csrf_exempt
def add_project(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        if name:
            p = Project(name=name, owner=request.user)
            p.save()
            p = Project.objects.all().filter(id=p.id)
            return render(request, 'projects/projects_detail.html', {'projects': p})
    return HttpResponse('FAIL')


@csrf_exempt
def add_theme(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        p_id = request.POST.get('p_id')
        if name:
            t = Theme(name=name, owner=request.user)
            t.save()
            p = Project.objects.get(id=p_id)
            p.themes.add(t)
            t = Theme.objects.all().filter(id=t.id)
            return render(request, 'projects/themes_detail.html', {'themes': t})
    return HttpResponse('FAIL')


@csrf_exempt
def add_rubric(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        t_id = request.POST.get('t_id')
        if name:
            r = Rubric(name=name, owner=request.user)
            r.save()
            t = Theme.objects.get(id=t_id)
            t.rubrics.add(r)
            r = Rubric.objects.all().filter(id=r.id)
            return render(request, 'projects/rubrics_detail.html', {'rubrics': r})
    return HttpResponse('FAIL')


@csrf_exempt
def add_num_analytics(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        p_id = request.POST.get('p_id')
        if name:
            p = Project.objects.get(id=p_id, owner=request.user)
            a = AnalyticNum.objects.create(owner=request.user, name=name)
            p.num_analytics.add(a)

            a = [a, ]
            return render(request, 'projects/num_analytics_detail.html', {'analytics': a})
    return HttpResponse('FAIL')


@csrf_exempt
def add_bool_analytics(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        p_id = request.POST.get('p_id')
        if name:
            p = Project.objects.get(id=p_id, owner=request.user)
            a = AnalyticBool.objects.create(owner=request.user, name=name)
            p.bool_analytics.add(a)
            a = [a, ]
            return render(request, 'projects/bool_analytics_detail.html', {'analytics': a})
    return HttpResponse('FAIL')


@csrf_exempt
def add_list_analytics(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        p_id = request.POST.get('p_id')
        if name:
            p = Project.objects.get(id=p_id, owner=request.user)
            a = AnalyticList.objects.create(owner=request.user, name=name)
            p.list_analytics.add(a)
            a = [a, ]
            return render(request, 'projects/list_analytics_detail.html', {'analytics': a})
    return HttpResponse('FAIL')


@csrf_exempt
def add_list_param(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        al_id = request.POST.get('al_id')
        if name:
            al = AnalyticList.objects.get(id=al_id, owner=request.user)
            p = ParamList.objects.create(name=name)
            al.params.add(p)
            p = [p, ]
            return render(request, 'projects/list_param.html', {'params': p, 'an': al})
    return HttpResponse('FAIL')


def projects_ids(request):
    if request.method == 'GET':
        all_proj = Project.objects. \
            filter(owner=request.user)
        p = list(map(lambda x: dict(name=x.name, id=x.id), all_proj))
        return JsonResponse(p, safe=False)
    return HttpResponse('FAIL')


def themes_ids(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        themes = p.themes.all()
        t = list(map(lambda x: dict(name=x.name, id=x.id), themes))
        return JsonResponse(t, safe=False)
    return HttpResponse('FAIL')


def rubrics_ids(request, pk):
    if request.method == 'GET':
        t = Theme.objects.get(id=pk)
        rubrics = t.rubrics.all()
        r = list(map(lambda x: dict(name=x.name, id=x.id), rubrics))
        return JsonResponse(r, safe=False)
    return HttpResponse('FAIL')


def num_analytics_ids(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        na = p.num_analytics.all()
        na = list(map(lambda x: dict(name=x.name, id=x.id), na))
        return JsonResponse(na, safe=False)
    return HttpResponse('FAIL')


def bool_analytics_ids(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        na = list(p.bool_analytics.all().values('id', 'name'))
        return JsonResponse(na, safe=False)
    return HttpResponse('FAIL')


def list_analytics_ids(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        a = list(p.list_analytics.all().values('id', 'name'))
        return JsonResponse(a, safe=False)
    return HttpResponse('FAIL')


def bool_params_ids(request, pk):
    if request.method == 'GET':
        b = AnalyticBool.objects.get(id=pk)
        na = []
        na.append(dict(name=b.name_positive, id=1))
        na.append(dict(name=b.name_negative, id=0))
        return JsonResponse(na, safe=False)
    return HttpResponse('FAIL')


def list_params_ids(request, pk):
    if request.method == 'GET':
        l = AnalyticList.objects.get(id=pk)
        p = list(l.params.all().values('id', 'name'))
        print(p)
        return JsonResponse(p, safe=False)
    return HttpResponse('FAIL')


def del_project(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        if p.owner == request.user:
            try:
                p.delete()
                return HttpResponse('OK')
            except Exception as e:
                print(e)
    return HttpResponse('FAIL')


def del_theme(request, pk):
    if request.method == 'GET':
        t = Theme.objects.get(id=pk)
        if t.owner == request.user:
            try:
                t.delete()
                return HttpResponse('OK')
            except Exception as e:
                print(e)
    return HttpResponse('FAIL')


def del_rubric(request, pk):
    if request.method == 'GET':
        r = Rubric.objects.get(id=pk)
        if r.owner == request.user:
            try:
                r.delete()
                return HttpResponse('OK')
            except Exception as e:
                print(e)
    return HttpResponse('FAIL')


def del_num_analytic(request, pk):
    if request.method == 'GET':
        na = AnalyticNum.objects.get(id=pk)
        if na.owner == request.user:
            try:
                na.delete()
                return HttpResponse('OK')
            except Exception as e:
                print(e)
    return HttpResponse('FAIL')


def del_param_num_analytics(request, pk):
    if request.method == 'GET':
        pn = ParamNum.objects.get(id=pk)
        # todo owner
        try:
            a = pn.analyticnum_set.first()
            pn.p_news.num_analytics.remove(a)
            pn.delete()

            return HttpResponse('OK')
        except Exception as e:
            print(e)
    return HttpResponse('FAIL')


def del_bool_analytic_param(request, n_id, b_id):
    if request.method == 'GET':
        # todo owner
        try:
            news = ProjectNews.objects.get(id=n_id)
            bool_an = AnalyticBool.objects.get(id=b_id)
            news.bool_analytics.remove(bool_an)
            bool_an.p_news_false.remove(news)
            bool_an.p_news_true.remove(news)
            return HttpResponse('OK')
        except Exception as e:
            print(e)
    return HttpResponse('FAIL')


def del_bool_analytic(request, pk):
    if request.method == 'GET':
        ba = AnalyticBool.objects.get(id=pk)
        if ba.owner == request.user:
            try:
                ba.delete()
                return HttpResponse('OK')
            except Exception as e:
                print(e)
    return HttpResponse('FAIL')


def del_list_analytic(request, pk):
    if request.method == 'GET':
        la = AnalyticList.objects.get(id=pk)
        if la.owner == request.user:
            try:
                la.params.all().delete()
                la.delete()
                return HttpResponse('OK')
            except Exception as e:
                print(e)
    return HttpResponse('FAIL')


def del_list_param(request, pk):
    if request.method == 'GET':
        p = ParamList.objects.get(id=pk)
        try:
            p.delete()
            return HttpResponse('OK')
        except Exception as e:
            print(e)
    return HttpResponse('FAIL')


def del_list_analytic_param(request, n_id, p_id):
    if request.method == 'GET':
        # todo owner
        try:
            news = ProjectNews.objects.get(id=n_id)
            list_p = ParamList.objects.get(id=p_id)
            list_p.p_news.remove(news)
            return HttpResponse('OK')
        except Exception as e:
            print(e)
    return HttpResponse('FAIL')


# Редактирование объектов
@csrf_exempt
def edit_param_num_analytics(request):
    if request.method == 'POST':
        try:
            p_id = request.POST.get('a_pr_id')
            param = float(request.POST.get('param'))
            na = ParamNum.objects.get(id=p_id)
            na.num = param
            na.save()
            return HttpResponse('OK')
        except Exception as e:
            print(e)
    return HttpResponse('FAIL')


def change_bool_analytic_param_true(request, n_id, b_id):
    if request.method == 'GET':
        # todo owner
        try:
            news = ProjectNews.objects.get(id=n_id)
            bool_an = AnalyticBool.objects.get(id=b_id)
            bool_an.p_news_false.remove(news)
            bool_an.p_news_true.add(news)
            return JsonResponse({'status': 'OK', 'new_name': bool_an.name_positive})
        except Exception as e:
            print(e)
    return JsonResponse({'status': 'FAIL'})


def change_bool_analytic_param_false(request, n_id, b_id):
    if request.method == 'GET':
        # todo owner
        try:
            news = ProjectNews.objects.get(id=n_id)
            bool_an = AnalyticBool.objects.get(id=b_id)
            bool_an.p_news_true.remove(news)
            bool_an.p_news_false.add(news)
            return JsonResponse({'status': 'OK', 'new_name': bool_an.name_negative})
        except Exception as e:
            print(e)
    return JsonResponse({'status': 'FAIL'})


# Добавление новостей в ПТР
def make_p_news(n):
    ne = list(map(lambda x: ProjectNews.objects.create(
        origin=x,
        url=x.url,
        title=x.title,
        text=x.text,
        date_post=x.date_post,
        date_add=x.date_add,
        source=SourceFull.objects.get(code=x.source.code)), n))
    return ne


@csrf_exempt
def add_news_project(request, pk):
    if request.method == 'POST':
        name = request.user
        n_ids = request.POST.getlist('n_ids[]')
        if name:
            p = Project.objects.get(id=pk)
            if p.owner == name:
                try:
                    news = News.objects.filter(id__in=n_ids)
                    p_news = make_p_news(news)
                except SourceFull.DoesNotExist as e:
                    p_n = ProjectNews.objects.filter(id__in=n_ids)
                    n_ids = list(p_n.values_list('origin_id', flat=True))
                    news = News.objects.filter(id__in=n_ids)
                    p_news = make_p_news(news)
                p.news.add(*p_news)
                return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def add_news_themes(request, pk):
    if request.method == 'POST':
        name = request.user
        n_ids = request.POST.getlist('n_ids[]')
        if name:
            t = Theme.objects.get(id=pk)
            if t.owner == name:
                try:
                    news = News.objects.filter(id__in=n_ids)
                    p_news = make_p_news(news)
                except SourceFull.DoesNotExist as e:
                    p_n = ProjectNews.objects.filter(id__in=n_ids)
                    n_ids = list(p_n.values_list('origin_id', flat=True))
                    news = News.objects.filter(id__in=n_ids)
                    p_news = make_p_news(news)
                t.news.add(*p_news)
                t.project_set.first().news.add(*p_news)
                return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def add_news_rubrics(request, pk):
    if request.method == 'POST':
        name = request.user
        n_ids = request.POST.getlist('n_ids[]')
        if name:
            r = Rubric.objects.get(id=pk)
            if r.owner == name:
                try:
                    news = News.objects.filter(id__in=n_ids)
                    p_news = make_p_news(news)
                except SourceFull.DoesNotExist as e:
                    p_n = ProjectNews.objects.filter(id__in=n_ids)
                    n_ids = list(p_n.values_list('origin_id', flat=True))
                    news = News.objects.filter(id__in=n_ids)
                    p_news = make_p_news(news)
                r.news.add(*p_news)
                r.theme_set.first().project_set.first().news.add(*p_news)
                return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def add_param_num_analytics(request, pk):
    if request.method == 'POST':
        name = request.user
        n_ids = request.POST.getlist('n_ids[]')
        param = request.POST.get('param')
        if name:
            a = AnalyticNum.objects.get(id=pk)
            if a.owner == name:
                news = ProjectNews.objects.filter(id__in=n_ids)
                for n in news:
                    n.num_analytics.add(a)
                    n.save()
                    p = ParamNum.objects.create(num=float(param), p_news=n)
                    a.param.add(p)
                    a.save()

            return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def add_bool_analytic_param_true(request):
    if request.method == 'POST':
        name = request.user
        n_ids = request.POST.getlist('n_ids[]')
        b_id = request.POST.get('b_id')
        if name:
            a = AnalyticBool.objects.get(id=b_id)
            if a.owner == name:
                news = ProjectNews.objects.filter(id__in=n_ids)
                a.p_news_false.remove(*list(news))
                a.p_news_true.add(*list(news))
                for n in news:
                    n.bool_analytics.add(a)
        return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def add_bool_analytic_param_false(request):
    if request.method == 'POST':
        name = request.user
        n_ids = request.POST.getlist('n_ids[]')
        b_id = request.POST.get('b_id')
        if name:
            a = AnalyticBool.objects.get(id=b_id)
            if a.owner == name:
                news = ProjectNews.objects.filter(id__in=n_ids)
                a.p_news_true.remove(*list(news))
                a.p_news_false.add(*list(news))
                for n in news:
                    n.bool_analytics.add(a)
        return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def add_list_analytic_param(request):
    if request.method == 'POST':
        name = request.user
        n_ids = request.POST.getlist('n_ids[]')
        param = request.POST.get('param')
        if name:
            p = ParamList.objects.get(id=param)
            news = ProjectNews.objects.filter(id__in=n_ids)
            p.p_news.add(*news)
            for i in news:
                i.list_analytics.add(p.analyticlist_set.first())
            return HttpResponse('OK')
    return HttpResponse('FAIL')


def get_journal(request, pk):
    if request.method == 'GET':
        p = Project.objects.get(id=pk)
        if p.owner == request.user:
            news = []

            n = p.news.all().values('id', 'url', 'title', 'source__name', 'date_post', 'source__location__name')
            n = [v for v in n]
            for ne in n:
                ne.update(project_name=p.name)
            news.extend(n)
            for t in p.themes.all():
                n = t.news.all().values('id', 'url', 'title', 'source__name', 'date_post', 'source__location__name')
                n = [v for v in n]
                for ne in n:
                    ne.update(project_name=p.name,
                              theme_name=t.name
                              )
                news.extend(n)
                for r in t.rubrics.all():
                    n = r.news.all().values('id', 'url', 'title', 'source__name', 'date_post', 'source__location__name', 'annotation')
                    n = [v for v in n]
                    for ne in n:
                        ne.update(project_name=p.name,
                                  theme_name=t.name,
                                  rubric_name=r.name
                                  )
                    news.extend(n)
            news.sort(key=lambda i: i['id'])
            return render(request, 'projects/journal_list.html', {'news': news, 'project': p})
    return HttpResponse('FAIL')


@csrf_exempt
def rename_project(request):
    if request.method == 'POST':
        u_name = request.user
        p_id = request.POST.get('p_id')
        new_name = request.POST.get('name')
        if u_name:
            p = Project.objects.get(id=p_id)
            if p.owner == u_name:
                p.name = new_name
                p.save()
                return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def rename_theme(request):
    if request.method == 'POST':
        u_name = request.user
        t_id = request.POST.get('t_id')
        new_name = request.POST.get('name')
        if u_name:
            t = Theme.objects.get(id=t_id)
            if t.owner == u_name:
                t.name = new_name
                t.save()
                return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def rename_rubric(request):
    if request.method == 'POST':
        u_name = request.user
        r_id = request.POST.get('r_id')
        new_name = request.POST.get('name')
        if u_name:
            r = Rubric.objects.get(id=r_id)
            if r.owner == u_name:
                r.name = new_name
                r.save()
                return HttpResponse('OK')
    return HttpResponse('FAIL')


@csrf_exempt
def rename_num_analytics(request):
    if request.method == 'POST':
        u_name = request.user
        a_id = request.POST.get('a_id')
        new_name = request.POST.get('name')
        if u_name:
            an = AnalyticNum.objects.get(id=a_id)
            if an.owner == u_name:
                try:
                    an.name = new_name
                    an.save()
                    return HttpResponse('OK')
                except Exception as e:
                    print(e)
    return HttpResponse('FAIL')


@csrf_exempt
def rename_bool_analytics(request):
    if request.method == 'POST':
        u_name = request.user
        a_id = request.POST.get('a_id')
        new_name = request.POST.get('name')
        if u_name:
            an = AnalyticBool.objects.get(id=a_id)
            if an.owner == u_name:
                try:
                    an.name = new_name
                    an.save()
                    return HttpResponse('OK')
                except Exception as e:
                    print(e)
    return HttpResponse('FAIL')


@csrf_exempt
def rename_bool_positive(request):
    if request.method == 'POST':
        u_name = request.user
        a_id = request.POST.get('a_id')
        new_name = request.POST.get('name')
        if u_name:
            an = AnalyticBool.objects.get(id=a_id)
            if an.owner == u_name:
                try:
                    an.name_positive = new_name
                    an.save()
                    return HttpResponse('OK')
                except Exception as e:
                    print(e)
    return HttpResponse('FAIL')


@csrf_exempt
def rename_bool_negative(request):
    if request.method == 'POST':
        u_name = request.user
        a_id = request.POST.get('a_id')
        new_name = request.POST.get('name')
        if u_name:
            an = AnalyticBool.objects.get(id=a_id)
            if an.owner == u_name:
                try:
                    an.name_negative = new_name
                    an.save()
                    return HttpResponse('OK')
                except Exception as e:
                    print(e)
    return HttpResponse('FAIL')


@csrf_exempt
def rename_list_analytics(request):
    if request.method == 'POST':
        u_name = request.user
        a_id = request.POST.get('a_id')
        new_name = request.POST.get('name')
        if u_name:
            an = AnalyticList.objects.get(id=a_id)
            if an.owner == u_name:
                try:
                    an.name = new_name
                    an.save()
                    return HttpResponse('OK')
                except Exception as e:
                    print(e)
    return HttpResponse('FAIL')


@csrf_exempt
def rename_list_param(request):
    if request.method == 'POST':
        p_id = request.POST.get('p_id')
        new_name = request.POST.get('name')
        p = ParamList.objects.get(id=p_id)
        try:
            p.name = new_name
            p.save()
            return HttpResponse('OK')
        except Exception as e:
            print(e)
    return HttpResponse('FAIL')


def get_status_form(request, pr_id):
    if request.method == 'GET':
        form = StatusForm(initial={'project': pr_id})
        return render(request, 'projects/form.html', {'form': form})
    raise Http404("Poll does not exist")


def project_news_create(request):
    if request.method == 'GET':
        user = request.user
        p = user.project_set.all()
        return render(request, 'projects/project_news_creation.html', {'projects': p})


def xml_news(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        n_ids = data['news']
        news = ProjectNews.objects.filter(id__in=n_ids)
        xml = make_xml_v2_project_news(news)
        # xml = ''
        response = HttpResponse(xml, content_type='text/xml')
        response['Content-Disposition'] = 'attachment; filename=report.xml'
        return response