from django.db import models
from crowler.models import News
from catalog.models import SourceFull
from django.contrib.auth.models import User
from django.shortcuts import render
from projects.forms import StatusForm
from django.http import Http404
from django.utils.timezone import datetime
# Create your models here.


def now(*args):
    if len(args) > 0:
        for i in args:
            print(i)
    print(datetime.now())


class ParamList(models.Model):
    name = models.CharField(verbose_name='Название', max_length=150)
    p_news = models.ManyToManyField(to='ProjectNews', blank=True, editable=False)

    def __str__(self):
        return self.name


class AnalyticList(models.Model):
    owner = models.ForeignKey(to=User, default=None, null=True)
    name = models.CharField(verbose_name='Название', max_length=150)
    params = models.ManyToManyField(ParamList, verbose_name='параметры', blank=True)

    def __str__(self):
        return self.name


class AnalyticBool(models.Model):
    owner = models.ForeignKey(to=User, default=None, null=True)
    name = models.CharField(verbose_name='Название', max_length=150)
    p_news_true = models.ManyToManyField(to='ProjectNews', related_name='p_news_true', blank=True, null=True)
    p_news_false = models.ManyToManyField(to='ProjectNews', related_name='p_news_false', blank=True, null=True)
    name_positive = models.CharField(max_length=50, default='Да')
    name_negative = models.CharField(max_length=50, default='Нет')


class ParamNum(models.Model):
    num = models.DecimalField(max_digits=10, decimal_places=4)
    p_news = models.ForeignKey(to='ProjectNews', null=True, blank=True)


class AnalyticNum(models.Model):
    owner = models.ForeignKey(to=User, default=None, null=True)
    name = models.CharField(max_length=150, default='Название числовой аналитики')
    param = models.ManyToManyField(to=ParamNum, blank=True)

    def __str__(self):
        return self.name

    def add_param(self, num, p_n):
        self.param.add(ParamNum(num=num, p_news_id=p_n))


class ProjectNews(models.Model):
    origin = models.ForeignKey(to=News, editable=False, blank=True, null=True, default=None)
    url = models.URLField(verbose_name='Ссылка на источник', blank=True, null=True, default=None)
    title = models.TextField(verbose_name='Заголовок', blank=True, null=True)
    text = models.TextField(verbose_name='Текст', blank=True, null=True)
    date_post = models.DateTimeField(verbose_name='Дата публикации', db_index=True, blank=True, null=True)
    date_add = models.DateTimeField(verbose_name='Дата добавления в базу', blank=True, null=True, auto_now_add=True)
    date_journal = models.DateTimeField(verbose_name='Дата добавления в журнал', auto_now_add=True, blank=True, null=True)
    source = models.ForeignKey(SourceFull, verbose_name='Источник', blank=True, null=True, default=None)
    tonality = models.ForeignKey(to='Tonality', blank=True, null=True, default=None)
    status = models.ForeignKey(to='Status', blank=True, null=True, default=None)
    note = models.TextField(blank=True, null=True, default=None)
    annotation = models.TextField(verbose_name='Аннотация', blank=True, null=True, default='')

    num_analytics = models.ManyToManyField(to=AnalyticNum, blank=True)
    bool_analytics = models.ManyToManyField(to=AnalyticBool, blank=True)
    list_analytics = models.ManyToManyField(to=AnalyticList, blank=True)

    def analytics_add(self, nums_list, lists_list, bools_list):
        now('start_add')
        if nums_list:
            for i in nums_list:
                try:
                    a_num = AnalyticNum.objects.get(id=i['id'])
                    param = ParamNum.objects.create(num=float(i['value']), p_news=self)
                    a_num.param.add(param)
                    self.num_analytics.add(a_num)
                except Exception as e:
                    print(e)
        now('lists')
        if lists_list:
            for i in lists_list:
                try:
                    a_list = AnalyticList.objects.get(id=i['id'])
                    p_list = ParamList.objects.get(id=i['value'])
                    self.list_analytics.add(a_list)
                    p_list.p_news.add(self)
                except Exception as e:
                    print(e)
        now('bools')
        if bools_list:
            print(bools_list)
            for i in bools_list:
                try:
                    a_bool = AnalyticBool.objects.get(id=i['id'])
                    if i['value'] == 'true':
                        a_bool.p_news_true.add(self)
                        self.bool_analytics.add(a_bool)
                    elif i['value'] == 'false':
                        a_bool.p_news_false.add(self)
                        self.bool_analytics.add(a_bool)
                except Exception as e:
                    print(e)
        now('save')
        self.save()
        now('end_add')

    def analytics_remove(self):
        now('remove nums')
        self.num_analytics.clear()
        self.paramnum_set.clear()

        now('remove bools')
        self.bool_analytics.clear()
        self.p_news_true.remove(*self.p_news_true.all())
        self.p_news_false.remove(*self.p_news_false.all())
        # t = self.p_news_true.all()
        # f = self.p_news_false.all()

        # for i in t:
        #     i.p_news_true.remove(self)
        # for i in f:
        #     i.p_news_false.remove(self)

        now('remove lists')
        self.list_analytics.clear()
        self.paramlist_set.remove(*self.paramlist_set.all())
        now('end remove')

    def get_theme(self):
        try:
            return self.theme_set.all().values()[0]
        except IndexError as e:
            return None

    def get_rubric(self):
        try:
            return self.rubric_set.all().values()[0]
        except IndexError as e:
            return None

    def get_analytics_num(self):
        l = []
        p = ParamNum.objects.filter(p_news=self).values('analyticnum__name', 'num', 'id')
        l.extend(p)
        return l

    def get_analytics_bool(self):
        l = []
        b_true = self.bool_analytics.all().filter(p_news_true=self).values('name', 'id', 'name_positive')
        for i in b_true:
            i.update({'val': 1})

        b_false = self.bool_analytics.all().filter(p_news_false=self).values('name', 'id', 'name_negative')
        for i in b_true:
            i.update({'val': 0})
        l.extend(b_true)
        l.extend(b_false)
        return l

    def get_analytics_list(self):
        l = []
        for p in self.paramlist_set.all():
            l.append(dict(
                a_name=p.analyticlist_set.first().name,
                p_name=p.name,
                p_id=p.id
            ))
        return l

    def get_bool(self):
        t = AnalyticBool.objects.filter(p_news_true=self).values('id', 'name', 'name_positive')
        f = AnalyticBool.objects.filter(p_news_false=self).values('id', 'name', 'name_negative')
        return dict(true=t, false=f)


class Rubric(models.Model):
    owner = models.ForeignKey(to=User, blank=True, null=True)
    name = models.CharField(verbose_name='Название', max_length=150)
    news = models.ManyToManyField(to=ProjectNews, blank=True, editable=False)

    def __str__(self):
        return self.name

    def news_count(self):
        return self.news.count()


class Theme(models.Model):
    owner = models.ForeignKey(to=User, blank=True, null=True)
    name = models.CharField(verbose_name='Название', max_length=150)
    rubrics = models.ManyToManyField(to=Rubric, blank=True)
    news = models.ManyToManyField(to=ProjectNews, blank=True, editable=False)

    def __str__(self):
        return self.name

    def news_count(self):
        return self.news.count()

    def rubrics_dict(self):
        return [{'id': i.id, 'name': i.name, 'news_count': i.news_count()} for i in self.rubrics.all()]


class Tonality(models.Model):
    name = models.CharField(verbose_name='Название', max_length=150)
    color = models.CharField(verbose_name='Цвет', max_length=20)

    def __str__(self):
        return self.name


class Status(models.Model):
    name = models.CharField(verbose_name='Название', max_length=150, default='Новый статус', blank=True)
    color = models.CharField(verbose_name='Цвет', max_length=20, default='#000000', blank=True)
    bold = models.BooleanField(default=False)
    editable = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Project(models.Model):
    owner = models.ForeignKey(to=User, blank=True, null=True)
    name = models.CharField(verbose_name='Название', max_length=150)
    description = models.TextField(verbose_name='Описание', blank=True)
    themes = models.ManyToManyField(to=Theme, blank=True)
    news = models.ManyToManyField(to=ProjectNews, blank=True, editable=False)

    num_analytics = models.ManyToManyField(to=AnalyticNum, blank=True)
    bool_analytics = models.ManyToManyField(to=AnalyticBool, blank=True)
    list_analytics = models.ManyToManyField(to=AnalyticList, blank=True)

    tonalities = models.ManyToManyField(to=Tonality)
    statuses = models.ManyToManyField(to=Status)

    def __str__(self):
        return self.name

    def news_count(self):
        return self.news.count()

    def themes_dict(self):
        return [{'id': i.id, 'name': i.name, 'news_count': i.news_count(), 'rubrics_dict': i.rubrics_dict()} for i in
                self.themes.all()]

    def make_p_news(self, news):
        ne = list(map(lambda x: ProjectNews.objects.create(
            origin=x,
            url=x.url,
            title=x.title,
            text=x.text,
            date_post=x.date_post,
            date_add=x.date_add,
            source=SourceFull.objects.get(code=x.source.code)), news))

        return ne