from django.apps import AppConfig


class PublicapiConfig(AppConfig):
    name = 'publicapi'
