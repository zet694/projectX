from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'smimonitor.dev_settings')
# os.environ['CELERY_CONFIG_MODULE'] = 'myapp.celeryconfig'
app = Celery('smimonitor')
# app.config_from_envvar('CELERY_CONFIG_MODULE')
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))