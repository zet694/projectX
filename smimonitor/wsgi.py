import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smimonitor.settings")
#os.environ["CELERY_LOADER"] = "django"

application = get_wsgi_application()