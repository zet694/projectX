time_zone = 'Europe/Moscow'
app = 'smimonitor'
# nodes = 'spider1 spider2 spider3 writer1 task1 xml1'
# options = ''
log_file = '/home/gad/log/celery/%n%I.log'
broker_host = 'localhost'
broker_port = '5672'
broker_user = '***'
broker_password = '***'
broker_vhost = 'smimonitor'
task_routes = (
    {'crowler.spiders.*': {'queue': 'spiders'}},
    {'crowler.sync.*': {'queue': 'sync'}},
    {'crowler.xml_handlers.*': {'queue': 'spiders'}},
    {'crowler.tasks.*': {'queue': 'tasks'}},

)
